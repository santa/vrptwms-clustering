#!/bin/bash

# This script executes the clustering heuristic followed by the routing
# algorithm for all instances in data/. The heuristic is executed for
# all penalty values from 1.0 to (including) 2.0 in 0.1 steps.

# Make sure you installed the most recent clustering_cli to a location in your
# path!

# trivial, by_distance, combined, parallel, sequential
clustering_heuristic='combined'
cluster_solution_dir=./out/${clustering_heuristic}/

green='\e[0;32m'
red='\e[0;31m'
NC='\e[0m'  # No Color


echo -e "${green}${clustering_heuristic}${NC}"
for penalty in `seq 0 0.1 0.4`; do
  echo -e "${green}==================== penalty ${penalty} ===========${NC}"
  outdir=${cluster_solution_dir}/${penalty}/
  mkdir -p ${outdir}
  outfile=${outdir}/routing_${clustering_heuristic}_${penalty}.out
  if [ -f ${outfile} ]; then  # remove outfile if it already exists
    rm ${outfile}
  fi
  for f in data/?200*; do
    echo "processing ${f}"
    prototype/cli.py --penalty ${penalty} --export-routing-problem --outdir ${outdir} --heuristic ${clustering_heuristic} ${f}
    clusters=${f:5}
    clusters=${clusters%\.json}
    clusters=`ls "${outdir}/"${clusters}*`
    clustering_cli ${f} --clusters "${clusters}" >> $outfile
  done
done
