"""
Provide a data structure for storing an managing clusters of clients.

To be used by the VRPTWMS.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

import os

from . import config as cfg
from .node import Node, Parking


class ClusterInfeasibleException(Exception):
    """
    Thrown if an infeasible cluster is created.
    """


class Cluster(object):
    """
    A cluster has one or more clients and a single parking.

    The cluster's coordinates denote the location of its parking.
    """

    def __init__(self, problem=None, parking=None, client=None):
        """
        Initialize a cluster or create an "empty cluster" for cloning.
        """
        if client is None:
            return  # for cloning
        self.parking = parking
        self.clients = [client]
        self.demand = client.demand  # aggregated demand
        self.min_workers = 1
        self.pb = problem
        if not self._update_times(self.clients):
            raise ClusterInfeasibleException("New cluster not feasible")
        self.parking.clusters.append(self)
        self.id = next(self.pb.cluster_id_generator)

    def __str__(self):
        clients = ''
        for c in self.clients:
            clients += '\t{}{}'.format(c, os.linesep)
        string = "Cluster {i}: {p} {d} {w}{n}{clients}".format(i=self.id,
                                                               p=self.parking,
                                                               d=self.demand,
                                                               w=self.min_workers,
                                                               n=os.linesep,
                                                               clients=clients)
        return string

    @property
    def x(self):
        return self.parking.x

    @property
    def y(self):
        return self.parking.y

    def as_dict(self):
        """
        Return a dict representation for json encoding.

        The contained information is enough for routing, but also includes a
        compact representation of the cluster internals.
        """
        return {'client_ids': [c.id for c in self.clients],
                'demand': self.demand,
                'ests': self.ests[self.min_workers:],
                'id': self.id,
                'lsts': self.lsts[self.min_workers:],
                'min_workers': self.min_workers,
                'parking_id': self.parking.id,
                'sts': self.sts[self.min_workers:],
                'x': self.x,
                'y': self.y}

    def encode(self):
        """
        Return string-encoded outline of the cluster.

        This string can be used for textually encoding solutions.
        An example for an encoded cluster is "18,28,25,24" where 18 is the id
        of the cluster's parking and 28,25,24 are the clients serviced in the
        cluster (the workers are departing for the clients in that order).
        """
        s = "{}".format(self.parking.id)
        for c in self.clients:
            s += ",{}".format(c.id)
        return s

    def evaluate_addition(self, client):
        """
        Return attractiveness of adding client to cluster.

        If adding the client is not feasible, return 0.

        The attractiveness is the inverse of the sum of additional st for 1 to
        numerous workers. If the addition requires additional workers,
        a) add the full service time of the original cluster w/ the now
        impossible number of service workers multiplied w/ a penalty constant
        to the delta (this works best!) or
        b) add the service time of the newly required worker as delta; this
        already incorporates a penalty, so we can drop the penalty constant;
        this extra service time is the mean of the possible service times for
        all feasible numbers of workers; however, since an additional worker
        is required and the now impossible number of workers is still part of
        the old total service time, delta_workers is incremented by one
        => this works considerably worse then approach a)

        Note: I also tried to weight the number of workers in the deltas (a
        delta for two workers would be counted twice, a delta for three workers
        would be counted three times). This also worked considerably worse
        than the current approach.
        """
        cluster = self._clone()
        if not cluster.add(client):  # addition not successful
            if cfg.DEBUG_CLUSTER or cfg.DEBUG_HEURISTICS:
                print("Addition to cluster {} not feasible".format(cluster.id))
            return 0.0
        old_sts = self.sts[self.min_workers:]
        new_sts = cluster.sts[cluster.min_workers:]
        old_total = new_total = 0
        for num_workers, st in enumerate(old_sts, self.min_workers):
            old_total += num_workers * st
        for num_workers, st in enumerate(new_sts, cluster.min_workers):
            new_total += num_workers * st
        delta = new_total - old_total
        delta_workers = cluster.min_workers - self.min_workers
        if delta_workers:
            sts = self.sts[self.min_workers:cluster.min_workers]
            sts = [st * cfg.ATTRACTIVITY_PENALTY for st in sts]
            for num_workers, st in enumerate(sts, self.min_workers):
                delta += num_workers * st
        return 1 / delta if delta > cfg.MIN_DELTA else cfg.MAX_ATTR

    def add(self, client):
        """
        Add given client to cluster and update all relevant values.

        The addition must be feasible as this method does not check for
        feasibility. Use `evaluate_addition` to check feasibility.

        Return True if the resulting cluster is feasible, otherwise False.
        """
        self.clients.append(client)
        self.demand += client.demand
        if self.demand > self.pb.capacity:
            return False
        return self._update_times(self.clients)

    def _is_feasible(self):
        """
        Return True if the cluster is feasible, otherwise false.

        Checks time windows (incl. depot) and total demand.
        """
        if self.demand > self.pb.capacity:
            return False
        elif not self.lsts[-1]:
            return False
        return True

    def _update_times(self, clients):
        """
        Update the ests, lsts and sts for each possible number of workers.

        Order the clients to the sequence they will be serviced in.
        Calculate all times. Update the cluster's customer order and
        times only if the result is feasible.

        Return True if a feasible ordering (concerning time windows) was
        obtained with this heuristic, otherwise False.
        """
        #import pdb
        #pdb.set_trace()
        # TODO: evaluate/ use alternative orderings
        # eg. sort descending by walking time + service time which should
        # be better for small clusters if the time windows aren't too different

        def assign_clients(clients):
            """
            Return assignments data structure (3D array).

            This nested method expects a sorted list of clients for which
            an assignment to the available workers is done.

            Creates one assignment matrix for each available number of workers.
            Index 0 is not used; starting at 1, the index corresponds to the
            number of workers available.
            """
            d = self.pb.d
            p = self.parking
            assignments = [[] for _ in range(cfg.MAX_WORKERS + 1)]
            assignments[1] = [[], list(clients)]  # single worker
            for num_workers in range(2, cfg.MAX_WORKERS + 1):
                fts = [est] * num_workers  # keep track of each worker's ft
                assignment = [[] for _ in range(num_workers)]  # 1 per worker
                for c in clients:
                    time = min(fts)  # time when next worker becomes available
                    worker = fts.index(time)  # decide who serves the customer
                    assignment[worker].append(c)  # assign customer to worker
                    time += d[p.id, c.id] * self.pb.walking_time  # walk to client
                    time = max(time, c.est)  # wait before starting if required
                    time += c.st + d[c.id, p.id] * self.pb.walking_time  # work and return
                    fts[worker] = time
                assignments[num_workers] = [[]] + assignment
            return assignments

        def __sort_and_assign(clients, key):
            clients = sorted(clients, key=key)
            return clients, assign_clients(clients)

        def sorted_by_latest_return(clients):
            """
            Return list of clients sorted by latest return to parking.

            Also return how the clients are assigned to any number of workers.
            """
            k = lambda c: (c.lst + c.st +
                           self.pb.d[c.id, self.parking.id] *
                           self.pb.walking_time)
            return __sort_and_assign(clients, key=k)

        def sorted_by_latest_start(clients):
            """
            Return list of clients sorted by latest start at parking.

            Also return how the clients are assigned to any number of workers.
            """
            k = lambda c: (c.lst -
                           self.pb.d[self.parking.id, c.id] * self.pb.walking_time)
            return __sort_and_assign(clients, key=k)

        def sorted_by_longest_total_st(clients):
            """
            Return list of clients sorted by longest total service time.

            The total service time includes the walking time from and to the
            parking. Also return how the clients are assigned to any number of
            workers.
            Issue: currently neglects earliest starting time => causes waiting
            """
            k = lambda c: (c.st + self.pb.d[self.parking.id, c.id] *
                           self.pb.walking_time * 2)
            return __sort_and_assign(clients, key=k, reverse=True)

        def calc_lsts(assigments):
            """
            Return the latest starting times.

            One value for each number of available workers is calculated.
            """
            lsts = [None] * self.min_workers
            for num_workers in range(self.min_workers, cfg.MAX_WORKERS + 1):
                lsts.append(calc_lst(assigments[num_workers]))
            return lsts

        def calc_lst(assignment):
            """
            Return the latest starting time for the given assignment.

            An assigment is a list of each worker's clients (=> list of lists).
            """
            lst = calc_lst_for_worker(assignment[1])  # lst for first worker
            for clients in assignment[1:]:  # every other worker
                try:
                    lst = min(lst, calc_lst_for_worker(clients))
                except TypeError:  # one of the assignments is not feasible
                    return None
            return lst

        def calc_lst_for_worker(clients):
            """
            Return the latest starting time for the given worker's assignment.

            An assignment is a list of the clients the worker needs to visit.
            If the worker has no clients, he doesn't ever need to start.
            If the assignment is not feasible, `None` is returned.
            """
            if not clients:
                return self.pb.depot.lst  # proxy for a high enough start time
            d = self.pb.d
            p = self.parking
            c = clients[-1]
            # start from when we're done
            lst = c.lst + c.st + d[c.id, p.id] * self.pb.walking_time
            # make sure it's still possible to return to the depot
            lst = min(lst, self.pb.depot.lst - d[p.id, self.pb.depot.id])
            for c in reversed(clients):
                lst -= d[c.id, p.id] * self.pb.walking_time  # walk back
                lst -= c.st
                if lst < c.est:
                    return None  # this worker's assignment is not feasible
                lst = min(lst, c.lst)  # start to work
                lst -= d[p.id, c.id] * self.pb.walking_time  # walk to customer
            return lst

        def calc_fts(assignments, start_times):
            """
            Return the finishing times for the given start times.

            One value for each number of available workers is calculated.
            """
            # only calculated for the min. number of workers or more
            fts = [None] * self.min_workers
            for num_workers in range(self.min_workers, cfg.MAX_WORKERS + 1):
                fts.append(calc_ft(assignments[num_workers],
                                   start_times[num_workers]))
            if cfg.DEBUG_CLUSTER:
                print("fts", fts)
            return fts

        def calc_ft(assignment, start):
            """
            Return the latest finishing time for the given assignment.

            An assigment is a list of each worker's clients (=> list of lists).
            Start is the time the workers start at the parking.
            """
            ft = calc_ft_for_worker(assignment[1], start)  # first worker
            for clients in assignment[1:]:  # every other worker
                ft = max(ft, calc_ft_for_worker(clients, start))
            return ft

        def calc_ft_for_worker(clients, start):
            """
            Return the finishing time for the given worker's assignment.

            An assigment is a list of the clients the worker needs to visit.
            Start is the time the worker starts at the parking.
            """
            d = self.pb.d
            p = self.parking

            time = start
            for c in clients:
                time += d[p.id, c.id] * self.pb.walking_time  # walk to client
                feasible = round(time, cfg.DIGITS) <= round(c.lst, cfg.DIGITS)
                assert feasible, ("Bug: arriving too late at client\n"
                                  "time: {}, c.lst: {}\n"
                                  "{}".format(time, c.lst, self))
                time = max(time, c.est)  # wait before starting if required
                time += c.st + d[c.id, p.id] * self.pb.walking_time  # work and return
            return time

        def update_with_depot(lsts):
            """
            Update the given lsts if infeasibilities occur.

            An infeasibility occurs when the cluster cannot be reached in time
            from the opening depot.
            Return False, if none of the lsts remains feasible; otherwise True.
            """
            depot = self.pb.depot
            for idx, lst in enumerate(lsts):
                if lst is None:
                    continue
                if lst < depot.est + self.pb.d[depot.id, self.parking.id]:
                    lsts[idx] = None
            if lsts[-1] is None:
                if cfg.DEBUG_CLUSTER:
                    print("infeasible by time (return to depot not possible)")
                return False
            if cfg.DEBUG_CLUSTER:
                print("lsts", lsts)
            return True

        # earliest start time at this cluster (depends on when the depot opens)
        est = self.pb.depot.est + self.pb.d[self.pb.depot.id][self.parking.id]
        clients, assigments = sorted_by_latest_return(clients)
        if cfg.DEBUG_CLUSTER:
            print([c.id for c in clients])
        lsts = calc_lsts(assigments)
        if lsts[-1] is None:  # none of the assignments was feasible
            clients, assigments = sorted_by_latest_start(clients)
            lsts = calc_lsts(assigments)
            if lsts[-1] is None:
                if cfg.DEBUG_CLUSTER:
                    print("infeasible by time")
                return False
        if not update_with_depot(lsts):
            return False
        # if this is reached, the cluster is feasible
        self.lsts = lsts
        self.clients = clients
        self.min_workers = lsts.count(None)
        lfts = calc_fts(assigments, lsts)
        efts = calc_fts(assigments, [est] * (cfg.MAX_WORKERS + 1))
        deltas = [None] * (self.min_workers)
        deltas += [l - e for l, e in zip(lfts[self.min_workers:],
                                         efts[self.min_workers:])]
        if cfg.DEBUG_CLUSTER:
            print("deltas", deltas)
        self.ests = [None] * (self.min_workers)  # earliest start times
        self.ests += [l - d for l, d in zip(lsts[self.min_workers:],
                                            deltas[self.min_workers:])]
        if cfg.DEBUG_CLUSTER:
            print("ests", self.ests)
        self.sts = [None] * (self.min_workers)  # cluster's service times
        self.sts += [eft - est for eft, est in zip(efts[self.min_workers:],
                                                   self.ests[self.min_workers:])]
        if cfg.DEBUG_CLUSTER:
            print("sts", self.sts)
        return True

    def _clone(self):
        """
        Return a copy of the current cluster.

        Reduces the bloat of a deep copy but ensures that the client list
        is independent.
        """
        clone = self.__class__()
        clone.clients = list(self.clients)
        clone.demand = self.demand
        clone.ests = list(self.ests)
        clone.lsts = list(self.lsts)
        clone.min_workers = self.min_workers
        clone.parking = self.parking
        clone.pb = self.pb
        clone.sts = list(self.sts)
        clone.id = self.id
        return clone


def evaluate_new_cluster(problem, parking, client):
    """
    Return the attractiveness of potential new cluster.

    The attractiveness is 1 / total service time where the total service time
    is the time to serve the cluster with 1 worker + 2 times the time to
    serve the cluster with two workers + 3 times the time to serve the cluster
    with three workers etc.
    If the cluster would be infeasible for the given problem, 0 is returned.
    """
    d = problem.d
    aest = d[problem.depot.id, parking.id]
    aest += d[parking.id, client.id] * problem.walking_time
    if aest > client.lst:  # client cannot be reached on time
        return 0
    time = max(aest, client.est)
    time += client.st + d[client.id, parking.id] * problem.walking_time
    time += d[parking.id, problem.depot.id]
    if time > problem.depot.lst:  # cannot return to depot on time
        return 0
    st = d[client.id, parking.id] * problem.walking_time
    st += client.st
    st += d[parking.id, client.id] * problem.walking_time
    #return 1 / (st * cfg.MAX_WORKERS)  # only one worker is needed
    total_st = 0
    # additional workers don't speed things up
    for num_workers in range(1, cfg.MAX_WORKERS + 1):
        total_st += st * num_workers
    return 1 / total_st
