"""
Provides classes for clients, the depot and parkings.

To be used by the VRPTWMS.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

import json


class Point(object):
    """
    Represent a point in a cartesian coordinate system.

    >>> print(Point('3', 5))
    (3.0, 5.0)
    """
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __str__(self):
        return "({s.x:.1f}, {s.y:.1f})".format(s=self)

    def as_dict(self):
        return {'x': self.x, 'y': self.y}


class Location(Point):
    """
    A location is a point with a unique id.
    """
    def __init__(self, id_, x, y):
        super().__init__(x, y)
        self.id = int(id_)

    def as_dict(self):
        d = super().as_dict()
        d['id'] = self.id
        return d


class Parking(Location):
    """
    Represent a parking location.

    A parking is later made aware of all clusters using it. This helps when
    selecting potential clusters a customer could/ should be added to.

    >>> print(Parking(5, 3, 5))
    Parking 5: (3.0, 5.0)
    """
    def __init__(self, id_, x, y):
        super().__init__(id_, x, y)
        self.clusters = []

    def __str__(self):
        return "Parking {}: {}".format(self.id, super().__str__())


class Depot(Location):
    def __init__(self, id_, x, y, est, lst):
        super().__init__(id_, x, y)
        self.clusters = []
        self.est = float(est)  # earliest_start
        self.lst = float(lst)  # latest_start

    def __str__(self):
        return "Depot {}: {}".format(self.id, super().__str__())

    def as_dict(self):
        d = super().as_dict()
        d['est'] = self.est
        d['lst'] = self.lst
        return d


# TODO: rename to Client
class Node(Location):
    """
    Represent a client.
    """
    def __init__(self, id_, x, y, demand, est, lst, st):
        super().__init__(id_, x, y)
        self.est = float(est)  # earliest_start
        self.lst = float(lst)  # latest_start
        self.demand = float(demand)
        self.st = float(st)  # servicetime

    def __str__(self):
        return "Node {s.id}: {} d={s.demand}".format(super().__str__(), s=self)

    def as_dict(self):
        d = super().as_dict()
        d['est'] = self.est
        d['lst'] = self.lst
        d['st'] = self.st
        d['demand'] = self.demand
        return d

Client = Node


def decode_node(class_, data):
    """
    Return an object of type class_ from the deserialize json dump.
    """
    data['id_'] = data.pop('id')
    return class_(**data)
