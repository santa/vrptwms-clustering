"""
Configuration file for clustering heuristics.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

## default heuristic to use for clustering
## choose from {trivial,by_distance,parallel}
HEURISTIC = "parallel"

## max. number of workers allowed per vehicle
MAX_WORKERS = 3

## every Nth input node is a parking unless an instance specifies the parkings
PARKINGS = 5

## relation of walking speed to driving speed
## 0.2 means that walking takes 5 times longer than driving
WALKING_SPEED = 0.2
WALKING_TIME = 1 / WALKING_SPEED

## default settings for cost parameters
COST_TRUCK = 1
COST_WORKER = 0.1
COST_DISTANCE = 0.0001

## penalty for cluster attractiveness when adding a customer requires more
## workers; should be >= 1
## TODO: globally rename to ATTRACTIVENESS_PENALTY
ATTRACTIVITY_PENALTY = 1.5

## the minimum difference in aggregated service time when evaluating
## the addition of a customer
## its purpose is to limit the maximum attractiveness of any addition
## (even if the addition does not cause any additional service time at the
## cluster)
MIN_DELTA = 0.01
## the maximum allowed attractiveness for adding a client to a cluster
## typical attractiveness values lie between 0.0 and 0.025
## limiting the max. attractiveness (which occurs if an addition does not cause
## any change in the cluster's service time) allows for stochastic
## metaheuristics to perform better by allowing selecting clusters that are
## not best from a myopic perspective
MAX_ATTR = 0.5

## the number of significant digits when asserting feasibility
## (all numbers are rounded to DIGITS after the decimal point)
DIGITS = 5

###############################################################################
## automatically update problems if new best results are encountered
WRITEBACK = True

## enable debug output for different parts of the program
DEBUG_CLI = False
DEBUG_CLUSTER = False
DEBUG_HEURISTICS = False
DEBUG_INSTANCE_GENERATOR = False
DEBUG_PROBLEM = False

###############################################################################
## configuration of visualization

FONTSIZE = 8
CLUSTER_COLOR = '#000000'  # for printing assignments of clients to parkings
COLOR = True  # optimize visualization for color (True) or gray scale (False)

###############################################################################
## configuration of input file layout
## don't change them unless you know exactly what you're doing
DEMAND_COL = 3
SKIPROWS = 9
HEADER_LINE = 5
SERVICETIME_COL = 6
TIMEWINDOW_COLS = (4, 5)
TRUCK_CAPACITY_COL = 1
TRUCK_NUMBER_COL = 0
COORD_COLS = (1, 2)

###############################################################################
## configuration of optimize-clusters
## very big number required for modelling
Q = 99999.0
