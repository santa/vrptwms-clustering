"""
Representation of a VRPTWMS problem.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

import json
import os
import sys

from itertools import count
from linecache import getline
from math import floor
from numpy import genfromtxt, int32
from os import linesep
from os.path import basename, splitext
from scipy.spatial.distance import pdist, squareform

from . import config as cfg
from . import heuristic
from .node import Depot, Node, Parking, Point, decode_node

#import config as cfg
#from node import Depot, Node, Parking, Point, decode_node


class InstanceError(Exception):
    """InstanceError covers infeasible instances, invalid input files etc."""


class Problem(object):
    """
    Represent a VRPTWMS problem.
    """
    def __init__(self, filename, assert_feasibility=True):
        """
        Read a VRPTW instance from the given file.

        All read columns also contain the depot, however the
        list of clients does not.

        The expected fileformat is the one used by Solomon.
        """
        self.cluster_id_generator = count(1)  # 1 avoids clash w/ depot id
        self.filename = basename(filename)
        self.found_new_global_best = False
        self.clients = []
        self.parkings = []
        name, ext = splitext(self.filename)
        ext = ext.lower()
        self.name = name
        if ext in [b'.txt', '.txt']:  # old solomon format
            node = Node(*getline(filename, cfg.SKIPROWS+1).split())
            self.depot = Depot(node.id, node.x, node.y, node.est, node.lst)
            self._create_clients_and_parkings(filename)
            coords = genfromtxt(filename, dtype=int32,
                                skiprows=cfg.SKIPROWS, usecols=cfg.COORD_COLS)
            self.d = squareform(pdist(coords, 'euclidean'))  # distance matrix
            header = getline(filename, cfg.HEADER_LINE).split()
            self.capacity = float(header[cfg.TRUCK_CAPACITY_COL])
            # set values that are set for JSON files but miss in the .txt files
            self.best_known_solution = ""  # string encoded best known solution
            self.best_known_distance = float(sys.maxsize)
            self.best_known_workers = sys.maxsize
            self.best_known_trucks = sys.maxsize
            self.best_known_cost = float(sys.maxsize)
            self.best_solution_time = ""
            self.description = ""
            self.optimum_known = False
            self.walking_speed = cfg.WALKING_SPEED
            self.walking_time = 1 / self.walking_speed
        elif ext in [b'.json', '.json']:
            try:
                self._parse_json(filename)
            except (KeyError, ValueError) as e:
                raise InstanceError("ERROR: {} is not a valid input file"
                    .format(filename))
        else:
            raise IOError("{} file format not recognized".format(ext))
        if assert_feasibility and not ids_are_consecutive(self.parkings +
                                                          self.clients):
            raise InstanceError("{} has non-consecutive ids".format(filename))
        # every client is made aware of its closest parkings
        for client in self.clients:
            client.parkings = self._closest_parkings(client)
        if assert_feasibility and not self._is_feasible():
            raise InstanceError("{} is not feasible".format(filename))

    def __str__(self):
        s = self.name + linesep
        s += str(self.depot) + linesep
        s += "Clients:" + linesep
        for n in self.clients:
            s += str(n) + linesep
        s += "Parkings:" + linesep
        for p in self.parkings:
            s += str(p) + linesep
        return s

    def _create_clients_and_parkings(self, filename):
        """
        Reads the input file and creates a list of customers. This list
        does not contain the depot!
        Arguments:
        - `self`:
        - `filename`: name of the input file
        """
        with open(filename, encoding="utf-8") as data:
            for _count in range(cfg.SKIPROWS):
                next(data)
            # the depot also serves as parking
            line = next(data)
            self.parkings.append(self.depot)
            for idx, line in enumerate(data):
                if not line.split():  # ignore empty lines
                    continue
                if idx % cfg.PARKINGS == 0:
                    self.parkings.append(Parking(*line.split()[:3]))
                else:
                    self.clients.append(Node(*line.split()))

    def _parse_json(self, filename):
        """
        Populate object's values with json encoded data.
        """
        with open(filename, encoding='utf-8') as f:
            data = json.load(f, encoding='utf-8')
        self.capacity = data['truckCapacity']
        self.depot = decode_node(Depot, data['depot'])
        # the depot also serves as parking
        self.parkings.append(self.depot)
        for raw_parking in data['parkings']:
            self.parkings.append(decode_node(Parking, raw_parking))
        for raw_client in data['clients']:
            self.clients.append(decode_node(Node, raw_client))
        locations = self.clients + self.parkings  # the depot is in parkings
        coords = [(p.x, p.y) for p in sorted(locations, key=lambda l: l.id)]
        self.d = squareform(pdist(coords, 'euclidean'))  # distance matrix
        # read optional values
        self.best_known_solution = data.get('bestKnownSolution', '')
        self.best_known_distance = data.get('bestKnownDistance', float('inf'))
        self.best_known_workers = data.get('bestKnownWorkers', sys.maxsize)
        self.best_known_trucks = data.get('bestKnownTrucks', sys.maxsize)
        self.best_known_cost = data.get('bestKnownCost', float('inf'))
        self.best_solution_time = data.get('bestSolutionTime', '')
        self.optimum_known = data.get("optimumKnown", False)
        self.walking_speed = data.get("walkingSpeed", cfg.WALKING_SPEED)
        self.walking_time = 1 / self.walking_speed
        self.name = data.get('name', '')
        self.description = data.get('description', '')

    def _is_feasible(self):
        """
        Return True if the parsed instance is feasible, otherwise False.

        Not being feasible means that
         - a customer has more demand than the truck's capacity
         - it is not possible to satisfy the customer's time window with
           or without using a truck
         - the ids are not consecutive starting at 0 (this is for convenience
           to allow using the ids as indices for the distance matrix)
        """
        depot = self.depot
        if not ids_are_consecutive(self.parkings + self.clients):
            print("instance doesn't have unique consecutive ids starting at 0",
                  file=sys.stderr)
            return False
        for client in self.clients:
            if client.demand > self.capacity:
                print("{} exceeds capacity: {}".format(client, self.capacity),
                      file=sys.stderr)
                return False
            closest_parking = client.parkings[0]
            time = depot.est + self.d[depot.id, closest_parking.id]
            time += self.d[closest_parking.id, client.id] * self.walking_time
            # walk directly from depot if that's faster than driving + walking
            time = min(time, depot.est +
                       self.d[depot.id, client.id] * self.walking_time)
            if time > client.lst:
                print("{} cannot be reached in time".format(client),
                      file=sys.stderr)
                print("{closest_parking}; earliest arrival at client: {time}"
                      .format(**locals()), file=sys.stderr)
                return False
            time += client.st
            time += min(self.d[client.id, closest_parking.id] *
                        self.walking_time +
                        self.d[closest_parking.id, depot.id],
                        self.d[client.id, depot.id] * self.walking_time)
            if time > depot.lst:
                print("cannot return from {} in time".format(client),
                      file=sys.stderr)
                print("{closest_parking}, time: {time}".format(**locals()),
                      file=sys.stderr)
                return False
        return True

    def as_dict(self):
        """
        Return a dict representation for json encoding.
        """
        parkings = [p.as_dict() for p in self.parkings if type(p) is Parking]
        d = {"name": self.name,
             "description": self.description,
             "truckCapacity": self.capacity,
             "costTruck": cfg.COST_TRUCK,
             "costWorker": cfg.COST_WORKER,
             "costDistance": cfg.COST_DISTANCE,
             "depot": self.depot.as_dict(),
             "parkings": parkings,
             "clients": [c.as_dict() for c in self.clients],
             "bestKnownDistance": self.best_known_distance,
             "bestKnownWorkers": self.best_known_workers,
             "bestKnownTrucks": self.best_known_trucks,
             "bestKnownCost": self.best_known_cost,
             "bestKnownSolution": self.best_known_solution,
             "bestSolutionTime": self.best_solution_time,
             "optimumKnown": self.optimum_known,
             "walkingSpeed": self.walking_speed,
        }
        return d

    @property
    def json(self):
        """
        Return a JSON string representing the problem.

        The JSON output will also include the settings used for the
        current problem that are defined in the JSON schema for VRPTWMS
        clustering instances.
        """
        return json.dumps(self.as_dict(), sort_keys=True, indent=2,
                          ensure_ascii=False, separators=(',', ': '))

    def export_routing_problem(self):
        """
        Save json representation of the routing problem to a file.

        The exported file's name is the name of the instance with "_routing"
        appended to it. Files will only be written if the target doesn't exist.

        The exported problem can then be solved by a dedicated routing solver.
        It is important to also store the maximum number workers used to create
        the clusters as the clustering solutions will vastly differ given this
        setting.
        """
        error_msg = ("ERROR: clustering must be solved"
                     "before exporting the routing problem")
        assert hasattr(self, 'clusters'), error_msg
        assert len(self.clusters), error_msg
        d = self.as_dict()
        del(d['clients'])
        del(d['parkings'])
        d["clusters"] = [c.as_dict() for c in self.clusters]
        d["max_workers"] = cfg.MAX_WORKERS
        filename = self.name + "_clusters" + "_" + cfg.HEURISTIC + ".json"
        if os.path.exists(filename):
            raise EnvironmentError('ERROR: ' + filename + ' already exists')
        json_string = json.dumps(d, sort_keys=True, indent=2,
                                 ensure_ascii=False, separators=(',', ': '))
        with open(os.path.join(cfg.outdir, filename),
                  'w', encoding='utf-8') as f:
            f.write(json_string)
            print("wrote {}".format(os.path.join(cfg.outdir, filename)))

    @property
    def nodes(self):
        """
        Return a list of all nodes (depot, parkings and clients) in the problem.

        The depot is included in the parkings.
        """
        return self.parkings + self.clients

    def get_parking_by_id(self, id_):
        """
        Return parking corresponding to the given id.

        Note: a node can also serve as a parking.
        """
        id_ = int(id_)
        for parking in self.nodes:
            if parking.id == id_:
                return parking
        raise LookupError('Parking {} does not exist'.format(id_))

    def get_client_by_id(self, id_):
        """
        Return client corresponding to the given id.
        """
        id_ = int(id_)
        for client in self.clients:
            if client.id == id_:
                return client
        raise LookupError('Client {} does not exist'.format(id_))

    def create_clusters(self):
        """
        Create a list of clusters using the configured heuristic.

        TODO: test heuristic (needs to return list of cluster objects)
        """
        if cfg.DEBUG_HEURISTICS:
            print("using", cfg.HEURISTIC, "heuristic")
        self.clusters = heuristic.heuristic[cfg.HEURISTIC](self)

    def visualize(self, clf=True):
        """
        Visualize the problem instance.
        """
        from matplotlib import pyplot as plt
        if clf:
            plt.clf()
        x_min, x_max, y_min, y_max = get_limits(self.nodes, 0.1)
        if cfg.COLOR:
            self.visualize_color(plt)
        else:  # gray scale output
            self.visualize_gray_scale(plt)
        plt.ylim(y_min, y_max)
        plt.xlim(x_min, x_max)
        plt.subplots_adjust(left=0.03, bottom=0.05, right=0.97, top=0.95)
        plt.legend(numpoints=1, loc='lower right')
        plt.show()
        #plt.savefig(cfg.IMAGEDIR + os.sep + self.name + ".svg",
        #transparent=True)

    def visualize_color(self, plt):
        plt.text(self.depot.x, self.depot.y,
                 self.depot.id,
                 backgroundcolor="black", color="white",
                 fontsize=cfg.FONTSIZE, verticalalignment='center',
                 horizontalalignment='center')
        for client in self.clients:
            plt.text(client.x, client.y,
                     client.id,
                     backgroundcolor="blue", color="white",
                     fontsize=cfg.FONTSIZE, verticalalignment='center',
                     horizontalalignment='center')
        for parking in self.parkings:
            if isinstance(parking, Depot):
                continue
            plt.text(parking.x, parking.y,
                     parking.id,
                     backgroundcolor="red", color="white",
                     fontsize=cfg.FONTSIZE, verticalalignment='center',
                     horizontalalignment='center')

    def visualize_gray_scale(self, plt):
        plt.plot(self.depot.x, self.depot.y, 'ms', label='depot')
        plt.plot(self.clients[0].x, self.clients[0].y, 'co', label='customer')
        for client in self.clients[1:]:
            plt.plot(client.x, client.y, 'co')
        label = True
        for parking in self.parkings[1:]:
            if isinstance(parking, Depot):
                continue
            if label:
                plt.plot(parking.x, parking.y, 'kx', label='parking')
                label = False
            else:
                plt.plot(parking.x, parking.y, 'kx')

    def visualize_solution(self, sol=None):
        """
        Visualize the given problem and its solution.

        sol: string-encoded solution
        """
        if sol is None:
            print("no solution given for visualization", sys.stderr)
            return
        from matplotlib import pyplot as plt
        x_min, x_max, y_min, y_max = get_limits(self.nodes, 0.1)
        routes, clusters = decode(sol)
        for route in routes:
            for i, parking_index in enumerate(route[2:], 2):
                origin = self.get_parking_by_id(route[i - 1])
                sink = self.get_parking_by_id(parking_index)
                plt.plot([origin.x, sink.x], [origin.y, sink.y], 'k-')
                plt.arrow(origin.x, origin.y,
                          (sink.x - origin.x) / 4,
                          (sink.y - origin.y) / 4,
                          color="black",
                          width=0.05)
                if i == 2:  # indicate direction
                    plt.text((sink.x - origin.x) / 8,
                             (sink.y - origin.y) / 8,
                             route[0], fontsize=cfg.FONTSIZE,
                             color="black", backgroundcolor="white",
                             verticalalignment='center',
                             horizontalalignment='center')

        for cluster in clusters:
            cluster = list(cluster)  # in case cluster is a map object
            parking = self.get_parking_by_id(cluster.pop(0))
            for i, client in enumerate(cluster, 1):
                client = self.get_client_by_id(client)
                plt.plot([parking.x, client.x], [parking.y, client.y],
                         color=cfg.CLUSTER_COLOR,  linewidth=1.5)
                plt.text((parking.x + client.x) / 2,
                         (parking.y + client.y) / 2,
                         i, backgroundcolor="white", color="green",
                         fontsize=cfg.FONTSIZE, verticalalignment='center',
                         horizontalalignment='center')
        self.visualize(clf=False)

    def closest_parking(self, client):
        """
        Return the closest parking to the given client.

        TODO: improve function to return closest feasible parking
        see comments in heuristic.parallel_insertion(.)
        """
        min_distance = float("inf")
        for parking in self.parkings:
            if self.d[client.id][parking.id] < min_distance:
                closest_parking = parking
                min_distance = self.d[client.id][parking.id]
        return closest_parking

    def _closest_parkings(self, client):
        """
        Return all parkings sorted by their distance to the given client.

        The closest parking is first in the returned list. Does not affect
        the sorting of self.parkings.
        """
        return sorted(self.parkings, key=lambda p: self.d[client.id][p.id])


def ids_are_consecutive(node_list):
    """
    Return true if `nodelist` has unique consecutive ids starting at 0.
    """
    ids = sorted([n.id for n in node_list])
    return ids == list(range(len(node_list)))


def get_limits(points, padding):
    """
    Return x and y limits for the plot (x_min, x_max, y_min, y_max).

    Allows adding a padding in % of the max. range.
    >>> get_limits([Point(-2, -1), Point(3, 1), Point(2, 2), ], 0)
    (-2, 3, -1, 2)
    """
    x_min, x_max, y_min, y_max = ((int(points[0].x),) * 2 +
                                  (int(points[0].y),) * 2)
    for point in points[1:]:
        if point.x < x_min:
            x_min = int(point.x)
        elif point.x > x_max:
            x_max = int(point.x)
        if point.y < y_min:
            y_min = int(point.y)
        elif point.y > y_max:
            y_max = int(point.y)
    x_range = x_max - x_min
    y_range = y_max - y_min
    x_min -= int(floor(padding * x_range))
    y_min -= int(floor(padding * y_range))
    x_max += int(floor(padding * x_range))
    y_max += int(floor(padding * y_range))
    return x_min, x_max, y_min, y_max


def decode(sol):
    """
    Return list of simple routes and list of simple clusters.

    A simple route is a list of elements of a route. The first element
    is the number of workers required. The other are the ids of the
    parking locations.

    A simple cluster is a ordered list of elements of a cluster. The
    first element is the id of the cluster's parking, the rest are
    ids of the nodes.

    >>> routes, clusters = decode("3;0;1,6,10;2,5;0 2;0;18,28,25,24;0")
    >>> routes, list(map(list, clusters))
    ([[3, 0, 1, 2, 0], [2, 0, 18, 0]], [[1, 6, 10], [2, 5], [18, 28, 25, 24]])
    """
    routes_ = sol.split()
    clusters = []
    routes = []
    for route_ in routes_:
        # strip number of workers and depots
        clusters_ = [(c.split(',')) for c in route_.split(';')[2:-1]]
        clusters.extend([map(int, c) for c in clusters_])
        route_ = [e.split(',')[0] for e in route_.split(';')]
        routes.append([int(e) for e in route_])
    return routes, clusters
