"""
LP-Formulation for the clustering problem

.. codeauthor:: Dagmar Tschabrun <dagmar.tschabrun@edu.uni-graz.at>
.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

import os.path

import pulp

from .problem import Problem
from . import config as cfg
from .config import Q
from . import node


CUSTOMER_INDEX = 'n'
CLUSTER_INDEX = 'c'
PARKING_INDEX = 'p'
WORKER_INDEX = 'l'


LEGEND = ("\nLegend:\n" +
          "custmer index: {}\n".format(CUSTOMER_INDEX) +
          "cluster index: {}\n".format(CLUSTER_INDEX) +
          "parking index: {}\n".format(PARKING_INDEX) +
          "worker index: {}\n".format(WORKER_INDEX))


def brief_str(function):
    """
    Shortens the string representations of Location-based classes.
    """
    def _brief_str(*args, **kwargs):
        node__str__ = node.Node.__str__
        parking__str__ = node.Parking.__str__
        node.Node.__str__ = lambda self: "{}{}".format(CUSTOMER_INDEX, self.id)
        node.Parking.__str__ = lambda self: "{}{}".format(PARKING_INDEX,
                                                          self.id)
        result = function(*args, **kwargs)
        node.Node.__str__ = node__str__
        node.Parking.__str__ = parking__str__
        return result
    return _brief_str


@brief_str
def optimize_clusters(pb):
    for p in pb.parkings:  # model requires parkings to have dummy data
        p.est = pb.depot.est
        p.lst = pb.depot.lst
        p.st = 0.0
    N = pb.clients  # list of customers
    P0 = pb.parkings  # list of parking sites
    M = N + P0

    walking_time = {i: {j: pb.d[i.id][j.id] * pb.walking_time for j in M}
                    for i in M}
    # return time has to be index [n][m] to return from customer n to parking m
    return_time = walking_time
    # alpha is a matrix indexed by dictionary keys (hash table of hash tables)
    # each value denotes the earliest start towards a customer from a parking
    alpha = {p: {c: c.est - walking_time[p][c] for c in pb.clients}
             for p in pb.parkings}
    # each value denotes the latest start towards a customer from a parking
    beta = {p: {c: c.lst - walking_time[p][c] for c in pb.clients}
            for p in pb.parkings}
    # combined_time includes the walking time from p to c and the service time
    # note that the walking time back to p is not included
    combined_time = {p: {c: c.st + walking_time[p][c] for c in M}
                     for p in pb.parkings}

    C = ["{}{}".format(CLUSTER_INDEX, c) for c in range(len(N))]
    L = ["{}{}".format(WORKER_INDEX, c) for c in range(1, cfg.MAX_WORKERS + 1)]

    '''*** Decision variables****************************************'''
    # cluster c uses parking p
    x = pulp.LpVariable.dicts("cluster", (C, P0), cat=pulp.LpBinary)
    # customer n is in cluster c (using parking p)
    y = pulp.LpVariable.dicts("customer", (C, P0, N), cat=pulp.LpBinary)
    # worker l walks from customer or parking n to n' in cluster c
    # n' uses the variable name np (for n prime) in the model
    v = pulp.LpVariable.dicts("worker", (L, M, M, C), cat=pulp.LpBinary)
    # actual starting time from a parking to a customer
    delta = pulp.LpVariable.dicts("delta", (M), lowBound=0.0,
                                  cat=pulp.LpContinuous)
    # actual start time at cluster (truck arrives)
    xi_under = pulp.LpVariable.dicts("xi_u", (C), lowBound=0.0,
                                     cat=pulp.LpContinuous)
    # actual finish time at cluster (truck leaves)
    xi_over = pulp.LpVariable.dicts("xi_o", (C), lowBound=0.0,
                                    cat=pulp.LpContinuous)

    '''*** MILP *****************************************************'''
    prob = pulp.LpProblem("CCWRPTWMS", pulp.LpMinimize)
    prob += pulp.lpSum([(xi_over[c] - xi_under[c]) for c in C])

    ##subject to:

    # constraint (2.2)
    # only positive values for the actual service time are possible
    for c in C:
        prob += xi_over[c] - xi_under[c] >= 0.0

    # constraint (2.3)
    # a cluster has to have no or 1 parking
    for c in C:
        prob += sum([x[c][p] for p in P0]) <= 1

    # constraint (2.4)
    # ensure that no cluster's total demand exceeds the truck capacity
    for c in C:
        for p in P0:
            prob += (sum([y[c][p][n] * n.demand for n in N]) <=
                     pb.capacity * x[c][p])

    # constraint (2.5)
    # each customer must be assigned to exactly one cluster
    for n in N:
        prob += sum([y[c][p][n] for c in C for p in P0]) == 1

    # constraint (2.6)
    # only assign customers to existing clusters (clusters with a parking)
    for n in N:
        for p in P0:
            for c in C:
                prob += y[c][p][n] <= x[c][p]

    # constraint (2.7)
    # only build cluster when at least one customer is assigned to it
    for p in P0:
        for c in C:
            prob += x[c][p] <= sum([y[c][p][n] for n in N])

    # constraint (2.8)
    # a worker can only walk from n to n' in c if n is in c
    for c in C:
        for n in N:
            for l in L:
                for np in M:
                    if np is n:
                        continue
                    prob += v[l][n][np][c] <= sum([y[c][p][n] for p in P0])

    # constraint (2.9)
    # if worker l visits customer n in cluster c, the worker has to leave again
    for n in N:
        for c in C:
            for l in L:
                prob += (sum([v[l][ndp][n][c] for ndp in M]) ==
                         sum([v[l][n][np][c] for np in M]))

    # constraint (2.10)
    # each customer must be visited by exactly one worker
    # the worker must then leave towards another customer or parking
    for n in N:
        prob += sum([v[l][n][np][c] for c in C for l in L
                     for np in M if np is not n]) == 1

    # constraint (2.11)
    # only start working at each customer n when n is ready
    # if n is not assigned to c with p, this constraint is always fulfilled
    for n in N:
        for p in P0:
            for c in C:
                prob += delta[n] >= (alpha[p][n] * y[c][p][n] -
                                     Q * (1 - y[c][p][n]))  # cond. ignore

    # costraint (2.12)
    # work must start before the latest possible start time at the customer
    for n in N:
        for p in P0:
            for c in C:
                prob += delta[n] <= (beta[p][n] * y[c][p][n] +
                                     Q * (1 - y[c][p][n]))  # cond. ignore

    # constraint (2.13)
    # walking back from customer n is only possible when the time from the customer
    # before (np), the walking time and the service time is considered
    for n in N:
        for p in P0:
            for l in L:
                for c in C:
                    for np in M:
                        prob += delta[n] >= (delta[np] + combined_time[p][np] +
                                             return_time[np][p] -
                                             Q * (1-v[l][np][n][c]) -
                                             Q * (1-y[c][p][n]))

    # constraint (2.14)
    # Truck must arrive before departing for the first customer in cluster.
    for c in C:
        for n in N:
            for p in P0:
                prob += xi_under[c] <= delta[n] + Q * (1 - y[c][p][n])

    # constraint (2.15)
    # Truck cannot leave before last worker returns.
    for c in C:
        for n in N:
            for p in P0:
                prob += xi_over[c] >= delta[n] + (combined_time[p][n] + return_time[n][p]) * y[c][p][n] - Q * (1-y[c][p][n])

    import datetime as dt
    lp_filename = (dt.datetime.strftime(dt.datetime.now(), '%Y%m%d%H%M') +
                   '_' + os.path.splitext(pb.filename)[0] + '.lp')
    prob.writeLP(lp_filename)
    print('created', lp_filename)
    #prob.solve(pulp.COIN(msg=1, maxSeconds=172800))  # two days
    #prob.solve(pulp.GLPK())
    #prob.solve(pulp.GUROBI(timeLimit=60*60*24))  # one day

    #print(LEGEND)
    #print("status", pulp.LpStatus[prob.status])
    #print("value objective", pulp.value(prob.objective))

    #print solution of each decision veriable
    #for z in prob.variables():
        #if z.varValue != 0:
            #print("{}: {:.5f}".format(z.name, z.varValue))
