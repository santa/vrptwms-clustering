#!/usr/bin/env python3
"""
Commandline interface to VRPTWMS clustering heuristics.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""
from datetime import datetime
from os.path import abspath, basename, dirname, splitext
import argparse
import sys
import os

if __name__ == "__main__" and __package__ is None:
    # The following assumes the script is in the top level of the package
    # directory.  We use dirname() to help get the parent directory to add to
    # sys.path, so that we can import the current package.  This is necessary
    # since when invoked directly, the 'current' package is not automatically
    # imported.
    parent_dir = dirname(dirname(abspath(__file__)))
    sys.path.insert(0, parent_dir)
    import prototype
    __package__ = str("prototype")

from .lp import optimize_clusters
from .problem import InstanceError, Problem
from . import config as cfg
from . import heuristic


def writeback(filename, problem):
    """
    Write new globally best results to given file.

    Ensures that .json extensions are used.
    """
    filename = splitext(filename)[0] + '.json'
    json_string = problem.json  # avoid overwriting `filename` on failure
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(json_string)
        print("updated {}".format(filename))


def main(argv=None):
    """
    Read one or more problem files, solve them and print their solution stats.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs='+',
                        help='one or more input files')
    #parser.add_argument('--export-routing-problem', action='store_true',
                        #help='solve clustering and export the routing problem')
    parser.add_argument('--generate', action='store_true',
                        help='generate new instance and exit')
    parser.add_argument('--heuristic',
                        choices=sorted(heuristic.heuristic.keys()),
                        help='select the clustering heuristic to use')
    parser.add_argument('--no-color', action='store_true',
                        help='optimize visualization for gray scale printouts')
    parser.add_argument('--no-writeback', dest="no_writeback",
                        action='store_true',
                        help='disable updating problems with new best results')
    parser.add_argument('--optimize-clusters', dest="optimize_clusters",
                        action='store_true',
                        help='optimize the internal order of the clusters')
    parser.add_argument('--outdir', default=os.curdir+os.sep,
                        metavar='dir',
                        help=('write all output files in this directory; '
                              'the given directory must exist and be writable '
                              '(default: {})'.format(os.curdir+os.sep)))
    parser.add_argument('--parkings', type=int, default=cfg.PARKINGS,
                        metavar='n',
                        help=('every Nth input line is a parking '
                              '(only relevant for non-JSON input) '
                              '(default: {})'.format(cfg.PARKINGS)))
    parser.add_argument('-p', '--penalty', type=float,
                        default=cfg.ATTRACTIVITY_PENALTY,
                        metavar='value',
                        help=('penalty for cluster attractiveness when adding '
                              'a customer requires more workers; '
                              'should be >= 1 '
                              '(default: {})'.format(cfg.ATTRACTIVITY_PENALTY)))
    parser.add_argument('--visualize', action='store_true',
                        help='visualize instances and exit')
    parser.add_argument('--visualize-best', dest="visualize_best",
                        action='store_true',
                        help='visualize best known solutions and exit')
    args = parser.parse_args()
    cfg.PARKINGS = args.parkings
    cfg.ATTRACTIVITY_PENALTY = args.penalty
    cfg.outdir = args.outdir
    if args.generate:
        from . import instance_generator
        try:
            for infile in args.infile:
                instance_generator.generate(infile)
        except KeyboardInterrupt:
            print("Creation of new instance canceled by user")
        return 0
    if args.heuristic:
        cfg.HEURISTIC = args.heuristic
    if args.no_writeback:
        cfg.WRITEBACK = False
    if args.no_color:
        cfg.COLOR = False
    for infile in args.infile:
        try:
            pb = Problem(infile, not args.visualize)
        except InstanceError as e:
            print(e)
            continue
        except FileNotFoundError as e:
            print("WARNING: skipping {} (file not found)".format(infile),
                  file=sys.stderr)
            continue
        if args.visualize:
            pb.visualize()
        if args.visualize_best:
            pb.visualize_solution(pb.best_known_solution)
        if args.visualize or args.visualize_best:
            continue
        if args.optimize_clusters:
            optimize_clusters(pb)
            return 0  # TODO: remove once this does what it should
        else:
            pb.create_clusters()
        #if args.export_routing_problem:
        pb.export_routing_problem()
        if cfg.DEBUG_CLI:
            for c in pb.clusters:
                print(c)

        if cfg.WRITEBACK and pb.found_new_global_best:
            writeback(infile, pb)

    return 0

if __name__ == '__main__':
    sys.exit(main())
