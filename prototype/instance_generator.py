
"""
.. module:: instance_generator
   :synopsis: This module generates benchmark instances for VRPTWMS problems.

.. moduleauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. copyright:: GNU General Public License version 3
"""

import math
import os.path
import random
import sys

from scipy.spatial.distance import pdist, squareform

from .node import Client, Depot, Parking
from . import config as cfg
from . import problem


_GRID = {'x-min': -50,
         'x-max': 50,
         'y-min': -50,
         'y-max': 50}
_CAPACITY = 200  # default truck capacity
_DEPOT_START = 0
_DEPOT_END = 480  # in minutes (8 hours)
_MAX_DEPOT_DECENTRALITY = 0.1  # max. distance from center (fraction of grid)
_DEPOT_ID = 0
_DEMAND_FRACTION = 0.025  # max. client demand (faction of the truck capacity)
_MIN_ST = 3  # min. service time at single client
_MAX_ST = 15
# rectangular areas for clustering; x1/y1: left top, x2/y2: right bottom
_REGIONS = [{'x1': -50, 'y1': 48, 'x2': -35, 'y2': 28},
            {'x1': -14, 'y1': 41, 'x2': 12, 'y2': 16},
            {'x1': 23, 'y1': 30, 'x2': 34, 'y2': 7},
            {'x1': 42, 'y1': 43, 'x2': 50, 'y2': 22},
            {'x1': -30, 'y1': 4, 'x2': -12, 'y2': -17},
            {'x1': 15, 'y1': -12, 'x2': 38, 'y2': -27},
            {'x1': -47, 'y1': -28, 'x2': -23, 'y2': -50},
            {'x1': 3, 'y1': -31, 'x2': 14, 'y2': -41},
            {'x1': 31, 'y1': -30, 'x2': 43, 'y2': -44},
            {'x1': 43, 'y1': 14, 'x2': 50, 'y2': -3},
            {'x1': 30, 'y1': 50, 'x2': 38, 'y2': 42}]
_TW_MAX = {'tight': 15, 'normal': 60, 'wide': 120}
_TW_MAX['t'] = _TW_MAX['tight']
_TW_MAX['n'] = _TW_MAX['normal']
_TW_MAX['w'] = _TW_MAX['wide']
_TW_MIN = {'tight': 5, 'normal': 15, 'wide': 30}
_TW_MIN['t'] = _TW_MIN['tight']
_TW_MIN['n'] = _TW_MIN['normal']
_TW_MIN['w'] = _TW_MIN['wide']


class CancelledError(Exception):
    pass


class InfeasibilityError(Exception):
    pass


class RangeError(Exception):
    pass


def generate(infile):
    """
    Generate a new VRPTWMS benchmark instances.

    The generated instances can be entirely new or based on modifying the
    time windows of the passed input file.
    The new instances can be randomly distributed or clustered.
    """
    while True:
        filename = _get_string("Enter the new instance's filename", "filename",
                               "new_instance.json")
        if os.path.splitext(filename)[-1] != '.json':
            filename += '.json'
        if os.path.exists(filename):
            print("{} exists; choose another filename".format(filename))
        else:
            break
    pb = problem.Problem(infile)
    pb.found_new_global_best = False
    pb.name = os.path.splitext(os.path.basename(filename))[0]
    pb.walking_speed = _get_float(
        'Enter the walking speed (fraction of the driving speed)',
        name='walking speed', default=cfg.WALKING_SPEED, minimum=0.01,
        maximum=2.0, allow_zero=False)
    pb.walking_time = 1 / pb.walking_speed
    if _get_string("Base new instance on {} [y/N]".format(infile),
                   default='n').lower() not in {"y", "yes"}:
        _generate_new(pb)
    else:
        _generate_derived(pb)
    pb.best_known_solution = ""  # string encoded best known solution
    pb.best_known_distance = float(sys.maxsize)
    pb.best_known_workers = sys.maxsize
    pb.best_known_trucks = sys.maxsize
    pb.best_known_cost = float(sys.maxsize)
    pb.optimum_known = False
    pb.best_solution_time = ""
    pb.description = _get_string("Enter short description of the instance",
                                 name="description", default="")
    # _is_feasible requires that every client is aware of its closest parkings
    for client in pb.clients:
        client.parkings = pb._closest_parkings(client)
    assert pb._is_feasible(), "ERROR: generated problem is not feasible"
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(pb.json)
        print("wrote new instance to {}".format(filename))


def _ask_time_data(pb):
    """
    Return the minimum and maximum service time and the time window width.
    """
    max_range = max(_GRID['x-max'] - _GRID['x-min'],
                    _GRID['y-max'] - _GRID['y-min'])
    min_st = _get_integer("Enter the minimum service time for customers",
                          name='minimum service time', default=_MIN_ST,
                          minimum=1,
                          maximum=pb.depot.lst - 2 * max_range,
                          allow_zero=False)
    max_st = _get_integer("Enter the maximum service time for customers",
                          name='maximum service time', default=_MAX_ST,
                          minimum=min_st,
                          maximum=pb.depot.lst - 2 * max_range,
                          allow_zero=False)
    while True:
        tw_width = _get_string("How tight should time windows be"
                               "([t]ight, [n]ormal, [w]ide) [n]",
                               default='n').lower()
        if tw_width not in {'t', 'n', 'w'}:
            print("invalid choice: must be any of 't', 'n', 'w'")
        else:
            break
    return min_st, max_st, tw_width


def _generate_derived(pb):
    """
    Generate a new VRPTWMS benchmark instance derieved from the given problem.

    The given problem will only be changed in terms of the time windows.
    """
    if cfg.DEBUG_INSTANCE_GENERATOR:
        print("creating derived instance")
    min_st, max_st, tw_width = _ask_time_data(pb)
    for client in pb.clients:
        while True:
            try:
                est, lst, st = _create_times(pb, client.x, client.y,
                                             min_st, max_st, tw_width)
                client.est = float(est)
                client.lst = float(lst)
                client.st = float(st)
                break
            except InfeasibilityError:
                pass


def _create_depot():
    """
    Return a depot at a random location.
    """
    x = random.randint(_GRID['x-min'], _GRID['x-max'])
    x *= _MAX_DEPOT_DECENTRALITY
    x = int(x)
    y = random.randint(_GRID['y-min'], _GRID['y-max'])
    y *= _MAX_DEPOT_DECENTRALITY
    y = int(y)
    return Depot(_DEPOT_ID, x, y, _DEPOT_START, _DEPOT_END)


def _get_location(used_locations, clustered=False):
    """
    Return a location inside the grid that is not in the used locations.
    """
    overlapping = True
    while overlapping:
        overlapping = False
        if clustered:
            region = _REGIONS[random.randrange(len(_REGIONS))]
            x = random.randint(region['x1'], region['x2'])
            y = random.randint(region['y2'], region['y1'])
        else:
            x = random.randint(_GRID['x-min'], _GRID['x-max'])
            y = random.randint(_GRID['y-min'], _GRID['y-max'])
        for l in used_locations:
            if l.x == x and l.y == y:
                overlapping = True
                break
    return x, y


def _create_parkings(pb, num_parkings, next_id, clustered=False):
    """
    Add parkings to the given problem.

    Parkings cannot overlap with existing parkings or the depot. Overlapping
    with clients is ok.

    TODO: ensure that each clustering region has at least one parking
    """
    for unused_ in range(num_parkings):
        x, y = _get_location([pb.depot] + pb.parkings, clustered)
        pb.parkings.append(Parking(next_id, x, y))
        next_id += 1


def _get_closest_node(x, y, nodes):
    """
    Return the element of nodes closest to (x/y).
    """
    min_distance = math.hypot(nodes[0].x - x, nodes[0].y - y)
    closest = nodes[0]
    for n in nodes[1:]:
        distance = math.hypot(n.x - x, n.y - y)
        if distance < min_distance:
            min_distance = distance
            closest = n
    return closest


def _create_times(pb, x, y, min_st, max_st, tw_width):
    """
    Return feasible time windows for new client.

    :arg tw_width: Textual description of average/ maximum time window width.
    :type tw_width: str.
    :arg min_st: Minimum duration of the service time.
    :type min_st: int.
    :arg max_st: Maximum duration of the service time.
    :type max_st: int.
    :returns:  (int, int, int) -- (earliest start, latest start, service time).
    :raises: InfeasibilityError

    The created times must leave the problem feasible:
    aest = max(depot.est + driving time + walking to client, client.est)
    aest <= client.lst
    aest + st + walking to parking + driving to depot <= depot.lst

    Not considering the client's est and lst, the timespan between the earliest
    arrival at the customer and between the latest start to return to the depot
    in time marks a feasible period. To ensure feasibility, it must be
    guaranteed that at the clients est->lst timespan overlaps with the
    earliest arrival->latest start to return to depot timespan. This is done
    by randomly picking a moment in the latter period and extending that
    moment in both directions to obtain feasible values for client est and lst.
    """
    closest_parking = _get_closest_node(x, y, pb.parkings)
    driving_time = math.hypot(pb.depot.x - closest_parking.x,
                              pb.depot.y - closest_parking.y)
    walking_time = math.hypot(closest_parking.x - x,
                              closest_parking.y - y) * pb.walking_time
    service_time = random.randint(min_st, max_st)
    aest = math.ceil(pb.depot.est + driving_time + walking_time)
    alst = math.floor(pb.depot.lst - driving_time - walking_time -
                      service_time)
    if aest > alst:
        raise InfeasibilityError
    valid_moment = random.randint(aest, alst)
    time_window = random.randint(_TW_MIN[tw_width], _TW_MAX[tw_width])
    est = valid_moment - time_window // 2
    lst = valid_moment + time_window // 2
    return est, lst, service_time


def _create_clients(pb, num_clients, next_id, clustered=False):
    """
    Add parkings to the given problem.

    The clients cannot overlap with existing clients or the depot. Overlapping
    with parkings is ok.
    """
    pb.clients = []
    min_st, max_st, tw_width = _ask_time_data(pb)
    for unused_ in range(num_clients):
        while True:
            try:
                x, y = _get_location([pb.depot] + pb.clients, clustered)
                demand = random.randint(1, int(_DEMAND_FRACTION * pb.capacity))
                est, lst, st = _create_times(pb, x, y,
                                             min_st, max_st, tw_width)
                pb.clients.append(Client(next_id, x, y, demand, est, lst, st))
                next_id += 1
                break
            except InfeasibilityError:
                pass


def _generate_new(pb):
    """
    Generate an entirely new instance.
    """
    if cfg.DEBUG_INSTANCE_GENERATOR:
        print("creating new instance from scratch")
    pb.depot = _create_depot()
    pb.capacity = _get_integer("Enter the truck capacity",
                               name='capacity', default=_CAPACITY,
                               minimum=10, maximum=1000, allow_zero=False)
    clustered = False
    if _get_string("Cluster locations [y/N]",
                   default='n').lower() in {"y", "yes"}:
        clustered = True
    num_parkings = _get_integer("Enter the number of parkings",
                                'number of parkings', default=100,
                                minimum=0, maximum=1000, allow_zero=True)
    pb.parkings = [pb.depot]
    _create_parkings(pb, num_parkings, pb.depot.id + 1, clustered)
    num_clients = _get_integer("Enter the number of clients",
                               name='number of clients', default=200,
                               minimum=1, maximum=1000, allow_zero=False)
    _create_clients(pb, num_clients, pb.depot.id + num_parkings + 1, clustered)
    # the distance matrix is required to check if the instance is feasible
    locations = pb.clients + pb.parkings  # the depot is in parkings
    coords = [(p.x, p.y) for p in sorted(locations, key=lambda l: l.id)]
    pb.d = squareform(pdist(coords, 'euclidean'))


def _get_float(message, name="float", default=None, minimum=0,
               maximum=1.0, allow_zero=True):
    message += ": " if default is None else " [{0}]: ".format(default)
    while True:
        try:
            line = input(message)
            if not line and default is not None:
                return default
            f = float(line)
            if f == 0.0:
                if allow_zero:
                    return f
                else:
                    raise RangeError("{0} may not be 0.0".format(name))
            if not (minimum <= f <= maximum):
                raise RangeError("{name} must be between {minimum} "
                                 "and {maximum} inclusive{0}".format(
                                     " (or 0)" if allow_zero else "",
                                     **locals()))
            return f
        except RangeError as err:
            print("ERROR", err)
        except ValueError as err:
            print("ERROR {0} must be a float".format(name))


def _get_integer(message, name="integer", default=None, minimum=0,
                 maximum=100, allow_zero=True):
    message += ": " if default is None else " [{0}]: ".format(default)
    while True:
        try:
            line = input(message)
            if not line and default is not None:
                return default
            i = int(line)
            if i == 0:
                if allow_zero:
                    return i
                else:
                    raise RangeError("{0} may not be 0".format(name))
            if not (minimum <= i <= maximum):
                raise RangeError("{name} must be between {minimum} "
                                 "and {maximum} inclusive{0}".format(
                                     " (or 0)" if allow_zero else "",
                                     **locals()))
            return i
        except RangeError as err:
            print("ERROR", err)
        except ValueError as err:
            print("ERROR {0} must be an integer".format(name))


def _get_string(message, name="string", default=None):
    message += ": "
    while True:
        try:
            line = input(message)
            if not line:
                if default is not None:
                    return default
                else:
                    raise ValueError("{0} may not be empty".format(
                                     name))
            return line
        except ValueError as err:
            print("ERROR", err)




