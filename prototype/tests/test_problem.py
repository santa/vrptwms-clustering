"""
Test the Problem class.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
"""
from os.path import dirname, join
from nose.tools import *


from .. import config as cfg
from ..node import Node, Parking
from ..problem import Problem

TEST_INSTANCE = "presentation_scheduling_three_trucks.json"
INSTANCE_PATH = join(dirname(dirname(dirname(__file__))), "data", "tests",
                     TEST_INSTANCE)

TXT_INSTANCE = "test.txt"
TXT_PATH = join(dirname(dirname(dirname(__file__))), "data", "tests",
                TXT_INSTANCE)


class TestProblem():
    """
    Tests reading and writing regular JSON problem files.
    """
    def setUp(self):
        self.pb = Problem(INSTANCE_PATH)

    def tearDown(self):
        pass

    def test_json_output(self):
        """JSON representation must be the same as the input file."""
        with open(INSTANCE_PATH, encoding="utf-8") as f:
            content = f.read()
        eq_(self.pb.json, content)

    def test_closest_parkings(self):
        """
        Test if each customer's list of closest parkings is correct.
        """
        c14 = [c for c in self.pb.clients if c.id == 14][0]
        c26 = [c for c in self.pb.clients if c.id == 26][0]
        c47 = [c for c in self.pb.clients if c.id == 47][0]
        c14_parking_ids = [p.id for p in c14.parkings]
        c26_parking_ids = [p.id for p in c26.parkings]
        c47_parking_ids = [p.id for p in c47.parkings]
        eq_(c14_parking_ids[:3], [3, 2, 4])
        eq_(c26_parking_ids[:4], [16, 18, 15, 19])
        eq_(c47_parking_ids[:5], [31, 33, 19, 15, 36])
        eq_(c26_parking_ids[-1], 1)

    def test_num_clients(self):
        """Test that the correct number of clients is read."""
        eq_(len(self.pb.clients), 31)

    def test_num_parkings(self):
        """Test that the correct number of parkings is read."""
        eq_(len(self.pb.parkings), 17 + 1)  # includes the depot parking

    def test_depot(self):
        """Ensure that the depot is read correctly."""
        eq_(self.pb.depot.id, 0)
        eq_(self.pb.depot.x, 0.0)
        eq_(self.pb.depot.y, 0.0)
        eq_(self.pb.depot.est, 0.0)
        eq_(self.pb.depot.lst, 480.0)

    def test_distance_matrix(self):
        """Test if the distance matrix' values are as expected."""
        d = self.pb.d
        eq_(d[0][0], 0.0)  # depot -> depot
        assert_almost_equals(d[0][4], 31.144823004794873)  # depot -> parking
        eq_(d[4][0], d[0][4])  # depot <- parking
        assert_almost_equals(d[0][48], 25.942243542145693)  # depot -> client
        eq_(d[48][0], d[0][48])  # depot <- client
        assert_almost_equals(d[15][31], 25.179356624028344)  # parking -> parking
        eq_(d[31][15], d[15][31])  # parking <- parking
        assert_almost_equals(d[30][42], 24.413111231467404)  # client -> client
        eq_(d[42][30], d[30][42])  # client <- client
        assert_almost_equals(d[47][3], 54.3323108288245)  # parking -> client
        eq_(d[3][47], d[47][3])  # parking <- client


cfg.WALKING_TIME = 5  # 5 time units per distance unit
cfg.PARKINGS = 5  # Ensure there is only a single parking


class TestTxtProblem():
    """
    Tests reading old style TXT input files.
    """
    def setUp(self):
        self.pb = Problem(TXT_PATH)

    def test_closest_parkings(self):
        """
        Test if each customer's list of closest parkings is correct.
        """
        c2 = [c for c in self.pb.clients if c.id == 2][0]
        c3 = [c for c in self.pb.clients if c.id == 3][0]
        c9 = [c for c in self.pb.clients if c.id == 9][0]
        c2_parking_ids = [p.id for p in c2.parkings]
        c3_parking_ids = [p.id for p in c3.parkings]
        c9_parking_ids = [p.id for p in c9.parkings]
        assert (c2_parking_ids == [0, 1, 6, 16, 11] or
                c2_parking_ids == [1, 0, 6, 16, 11])
        assert (c3_parking_ids == [6, 1, 0, 16, 11] or
                c3_parking_ids == [6, 0, 1, 16, 11])
        assert (c9_parking_ids == [0, 1, 6, 16, 11] or
                c9_parking_ids == [1, 0, 6, 16, 11])

    def test_num_clients(self):
        """Test that the correct number of clients is read."""
        eq_(len(self.pb.clients), 12)

    def test_num_parkings(self):
        """Test that the correct number of parkings is read."""
        eq_(len(self.pb.parkings), 4 + 1)  # includes the depot parking

    def test_depot(self):
        """Ensure that the depot is read correctly."""
        eq_(self.pb.depot.id, 0)
        eq_(self.pb.depot.x, 0.0)
        eq_(self.pb.depot.y, 0.0)
        eq_(self.pb.depot.est, 0.0)
        eq_(self.pb.depot.lst, 1236.0)

    def test_distance_matrix(self):
        """Test if the distance matrix' values are as expected."""
        d = self.pb.d
        eq_(d[0][0], 0.0)  # depot -> depot
        assert_almost_equals(d[0][11], 3.1622776601683795)  # depot -> parking
        eq_(d[11][0], d[0][11])  # depot <- parking
        assert_almost_equals(d[0][5], 3.1622776601683795)  # depot -> client
        eq_(d[5][0], d[0][5])  # depot <- client
        assert_almost_equals(d[1][6], 1.0)  # parking -> parking
        eq_(d[6][1], d[1][6])  # parking <- parking
        assert_almost_equals(d[10][14], 0.0)  # client -> client
        eq_(d[14][10], d[10][14])  # client <- client
        assert_almost_equals(d[1][3], 2.0)  # parking -> client
        eq_(d[3][1], d[1][3])  # parking <- client

