"""
Test the Problem class.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
"""

from os.path import dirname, join
import unittest

from .. import config as cfg
from .. import instance_generator as ig
from ..node import Client, Parking
from ..problem import Problem


TEST_INSTANCE = "presentation_scheduling_three_trucks.json"
INSTANCE_PATH = join(dirname(dirname(dirname(__file__))), "data", "tests",
                     TEST_INSTANCE)


class TestInstanceGenerator(unittest.TestCase):
    """
    Tests generation of valid instances.
    """
    def setUp(self):
        self.pb = Problem(INSTANCE_PATH)

    def test_get_closest_node(self):
        for client in self.pb.clients[:2]:
            self.assertEqual(client.parkings[0].id,
                             ig._get_closest_node(client.x, client.y,
                                                  self.pb.parkings).id)

