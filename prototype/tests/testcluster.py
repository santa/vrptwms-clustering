"""
Test the Cluster data structure and its methods.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
"""

from math import *
from nose.tools import *
from os.path import dirname, join
import unittest

from .. import config as cfg
from ..node import Node, Parking
from ..problem import Problem

from ..cluster import Cluster

cfg.WALKING_TIME = 5  # 5 time units per distance unit
cfg.PARKINGS = 5  # Ensure there is only a single parking

TEST_INSTANCE = "unit_tests.json"
INSTANCE_PATH = join(dirname(dirname(dirname(__file__))), "data", "tests",
                     TEST_INSTANCE)


class TestCluster(unittest.TestCase):
    def setUp(self):
        self.pb = Problem(INSTANCE_PATH)

    def tearDown(self):
        pass

    def test__update_times_st_single_overlapping(self):
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(2, x=0, y=0, demand=10, est=0, lst=10, st=10))
        eq_(cluster.sts[0], None,
            "The first element of the st list must be empty")
        eq_(cluster.sts[1], 10,
            "The st of the cluster must equal the node's if no walking occurs")
        eq_(cluster.sts[2], 10,
            "A second worker cannot speed up the work at a single client")

    def test__update_times_st_single(self):
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        eq_(cluster.sts[0], None,
            "The first element of the st list must be empty")
        eq_(cluster.sts[1], 10 + 2 * cfg.WALKING_TIME * sqrt(2 ** 2 + 0 ** 2),
            "The st of the cluster must equal the node's if no walking occurs")
        eq_(cluster.sts[2], 10 + 2 * cfg.WALKING_TIME * sqrt(2 ** 2 + 0 ** 2),
            "A second worker cannot speed up the work at a single client")

    def test__update_times_st_double(self):
        """
        Test service time for two nodes with one and two and three workers.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        cluster.clients.append(Node(4, x=0, y=1, demand=10, est=0, lst=50,
                                    st=10))
        cluster._update_times(cluster.clients)
        st_single = (10 + sqrt(2 ** 2 + 0 ** 2) * cfg.WALKING_TIME * 2 +
                     10 + sqrt(1 ** 2 + 0 ** 2) * cfg.WALKING_TIME * 2)
        st_double = max(10 + sqrt(2 ** 2 + 0 ** 2) * cfg.WALKING_TIME * 2,
                        10 + sqrt(1 ** 2 + 0 ** 2) * cfg.WALKING_TIME * 2)
        st_triple = st_double  # the third worker cannot help here
        eq_(cluster.sts[0], None,
            "The first element of the st list must be empty")
        eq_(cluster.sts[1], st_single,
            "Incorrect service time for a single worker.")
        eq_(cluster.sts[2], st_double,
            "Failure with two workers. {} != {}".format(cluster.sts[2],
                                                        st_double))
        eq_(cluster.sts[3], st_triple,
            "A third worker cannot speed up the work if there are two nodes.")

    def test__update_times_st_triple(self):
        """
        Test service time for three nodes with up to three workers.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        cluster.clients.append(Node(4, x=0, y=1, demand=10, est=0, lst=50,
                                    st=10))
        cluster.clients.append(Node(5, x=-3, y=1, demand=10, est=0, lst=75,
                                    st=10))
        cluster._update_times(cluster.clients)
        st_single = (10 + hypot(2, 0) * cfg.WALKING_TIME * 2 +
                     10 + hypot(1, 0 ** 2) * cfg.WALKING_TIME * 2 +
                     10 + hypot(3, 1 ** 2) * cfg.WALKING_TIME * 2)
        st_double = max(10 + hypot(2, 0) * cfg.WALKING_TIME * 2 +
                        10 + hypot(1, 0) * cfg.WALKING_TIME * 2,
                        10 + hypot(3, 1) * cfg.WALKING_TIME * 2)
        st_double_alt = max(10 + hypot(2, 0) * cfg.WALKING_TIME * 2,
                            10 + hypot(1, 0) * cfg.WALKING_TIME * 2 +
                            10 + hypot(3, 1) * cfg.WALKING_TIME * 2)
        st_triple = max(10 + hypot(2, 0) * cfg.WALKING_TIME * 2,
                        10 + hypot(1, 0) * cfg.WALKING_TIME * 2,
                        10 + hypot(3, 1) * cfg.WALKING_TIME * 2)
        import sys
        print("is {}, should be {}".format(cluster.sts[1], st_single))
        print("is {}, should be {}".format(cluster.sts[2], st_double))
        print("is {}, should be {}".format(cluster.sts[3], st_triple))
        eq_(cluster.sts[0], None,
            "The first element of the st list must be empty")
        eq_(cluster.sts[1], st_single,
            "Incorrect st for one worker.")
        # don't test the ordering as it differs by heuristic
        if (cluster.sts[2] != st_double) and (cluster.sts[2] != st_double_alt):
            ok_(False, "A second worker speeds up the work.")
        eq_(cluster.sts[3], st_triple,
            "A third worker speeds up the work.")

    def test__update_times_st_waiting(self):
        """
        Test if required waiting is done correctly.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(6, x=1, y=0, demand=10, est=0, lst=10, st=10))
        cluster.clients.append(Node(7, x=0, y=1, demand=10, est=40, lst=50,
                                    st=10))
        cluster._update_times(cluster.clients)
        st_single = 5 + 10 + 5 + 5 + 10 + 10 + 5  # includes 10 for waiting
        st_double = st_single  # the second worker just waits
        eq_(cluster.sts[1], st_single,
            "The st must include waiting times")
        eq_(cluster.sts[2], st_double,
            "The second worker just waits.")

    def test__update_times_infeasible(self):
        """
        Test if an infeasible cluster is recognized.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(6, x=1, y=0, demand=10, est=0, lst=10, st=10))
        assert not cluster._update_times(cluster.clients +
                                         [Node(8, x=0, y=3, demand=10,
                                               est=0, lst=10, st=10)])

    def test__update_times_min_two_workers(self):
        """
        Test a cluster that requires at least two workers.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(6, x=1, y=0, demand=10, est=0, lst=10, st=10))
        cluster.clients.append(Node(9, x=0, y=1, demand=10, est=0, lst=10,
                                    st=10))
        cluster._update_times(cluster.clients)
        eq_(cluster.sts[1], None,
            "Not feasible with one worker.")
        ok_(cluster.sts[2], "Feasible with two workers.")
        eq_(cluster.min_workers, 2, "Cluster requires at least two workers")

    def test_add_client_demand(self):
        """
        Test if the aggregate demand is correct.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        client = Node(4, x=0, y=1, demand=10, est=0, lst=50, st=10)
        cluster.add(client)
        val_calc = cluster.demand
        val_true = 20

        eq_(val_calc, val_true, "Demand must be aggregate")

    def test_add_client_sts(self):
        """
        Test if the service times are updated correctly.
        """
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        client = Node(4, x=0, y=1, demand=10, est=0, lst=50, st=10)
        cluster.add(client)
        val_calc = cluster.sts
        val_true = [None, 50.0, 30.0, 30.0]
        eq_(val_calc, val_true)
        ok_(cluster.sts[1], "Feasible with one worker.")
        eq_(cluster.min_workers, 1, "Cluster requires one worker.")

    def test_encode(self):
        cluster = Cluster(self.pb, Parking(1, x=0, y=0),
                          Node(3, x=2, y=0, demand=10, est=0, lst=10, st=10))
        client = Node(6, x=0, y=1, demand=10, est=0, lst=50, st=10)
        cluster.add(client)
        self.assertEqual(cluster.encode(), "1,3,6")
        client = Node(2, x=1, y=1, demand=10, est=60, lst=150, st=10)
        cluster.add(client)
        self.assertEqual(cluster.encode(), "1,3,6,2")
        client = Node(5, x=-1, y=-1, demand=10, est=50, lst=60, st=10)
        cluster.add(client)
        self.assertEqual(cluster.encode(), "1,3,6,5,2")

    def test_issue_4(self):
        """
        TC for Issue #4.
        """
        pb = Problem(join(dirname(dirname(dirname(__file__))),
                          "data/tests/presentation_scheduling_one_truck.json"))
        n8 = pb.get_client_by_id(8)
        n11 = pb.get_client_by_id(11)
        n12 = pb.get_client_by_id(12)
        n14 = pb.get_client_by_id(14)
        p3 = pb.get_parking_by_id(3)
        cluster = Cluster(pb, p3, n8)
        cluster.add(n11)
        cluster.add(n12)
        cluster.add(n14)
