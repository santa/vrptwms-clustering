"""
Test the heuristics.

.. codeauthor:: Dagmar Tschabrun <dagmar.tschabrun@edu.uni-graz.at>
.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
"""
from nose.tools import *
from os.path import dirname, join

from .. import config as cfg
from ..node import Node, Parking
from ..problem import Problem

from .. import heuristic as h1

TEST_INSTANCE = "scheduling_tests.json"
INSTANCE_PATH = join(dirname(dirname(dirname(__file__))), "data", "tests",
                     TEST_INSTANCE)


class TestHeuristics():
    def setUp(self):
        self.pb = Problem(INSTANCE_PATH)

    def tearDown(self):
        pass

    def test_each_customer_in_own_cluster_correct_number_cluster(self):
        """
        There must be as many clusters as there are clients.
        """
        clusters = h1.each_customer_in_own_cluster(self.pb)
        val_calc = len(clusters)
        val_true = 8
        eq_(val_calc, val_true)

    def test_each_customer_in_own_cluster_correct_parking_and_client(self):
        """
        Test if selected client is assigned to the correct parking
        """
        clusters = h1.each_customer_in_own_cluster(self.pb)
        client_id = clusters[1].clients[0].id
        val_calc = [(clusters[1].clients[0].id), (clusters[1].parking.id)]
        val_true = [6, 4]
        eq_(val_calc, val_true)
