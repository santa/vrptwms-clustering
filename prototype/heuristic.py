"""
Provides different heuristics for creating customer clusters.

The final evaluation of the quality of any clustering heuristics can only be
performed in combination with a VRPTWMS routing algorithm for the clusters.

.. codeauthor:: Gerald Senarclens de Grancy <oss@senarclens.eu>
.. codeauthor:: Dagmar Tschabrun <dagmar.tschabrun@edu.uni-graz.at>
.. copyright:: GNU General Public License version 3
"""

import sys

#if __name__ == "__main__" and __package__ is None:
    ## The following assumes the script is in the top level of the package
    ## directory.  We use dirname() to help get the parent directory to add to
    ## sys.path, so that we can import the current package.  This is necessary
    ## since when invoked directly, the 'current' package is not automatically
    ## imported.
    #from os.path import abspath, dirname
    #import sys
    #parent_dir = dirname(dirname(abspath(__file__)))
    #sys.path.insert(0, parent_dir)
    #import prototype
    #__package__ = str("prototype")

from . import config as cfg
from .cluster import Cluster, ClusterInfeasibleException, evaluate_new_cluster


def each_customer_in_own_cluster(problem):
    """
    Return a list of clusters, where each client is in a separate "cluster".

    Each customer is serviced via its closest feasible parking.
    """
    clusters = []
    for client in problem.clients:
        clusters.append(Cluster(problem, client.parkings[0], client))
    return clusters


def customer_near_distance_min_cluster(problem):
    """
    Return a list of clusters with clients clustered at their closest parking.

    Add client to an existing cluster in its closest parking. If there are no
    clusters using the closest parking or adding the client would result in an
    infeasible cluster, create a new cluster instead.
    """
    ## always add to closest parking
    #clusters = []
    #for client in problem.clients:
        #for cluster in client.parkings[0].clusters:
            #if cluster.evaluate_addition(client):
                #cluster.add(client)
                #break
        #else:
            #clusters.append(Cluster(problem, client.parkings[0], client))
    #return clusters
    ## alternative: add to closest available cluster
    #clusters = []
    #for client in problem.clients:
        #added = False
        #for parking in client.parkings:
            #for cluster in parking.clusters:
                #if cluster.evaluate_addition(client):
                    #cluster.add(client)
                    #added = True
        #if not added:
            #clusters.append(Cluster(problem, client.parkings[0], client))
    #return clusters

    ## What is Dagmar doing here? Likely not what's described in the
    ## docstring; strangely, it doesn't create really bad results
    clients = problem.clients
    parkings = problem.parkings

    d_min = [float('inf')] * len(clients)
    id_min = [float('inf')] * len(clients)
    d = problem.d  # distance

    counter = 0

    for ic, c in enumerate(clients):
        for ip, p in enumerate(parkings):
            if d[c.id][p.id] < d_min[counter]:  # find min value
                d_min[counter] = d[c.id][p.id]
                id_min[counter] = ip  # store id from list parking
        counter += 1

    parking_list = []

    for ip, p in enumerate(parkings):
        parking_temp = []
        for i, min_p in enumerate(id_min):
            if ip == min_p:
                parking_temp.append(i)
        parking_list.append(parking_temp)

    clusters = []

    for ip, p in enumerate(parking_list):
        for ic in p:

            makeNewCluster = 1
            for icluster, cluster in enumerate(clusters):
                len_old = len(cluster.clients)
                if cluster.evaluate_addition(clients[ic]):
                    cluster.add(clients[ic])
                len_new = len(cluster.clients)
                if len_old != len_new:  #if the new is longer, no new cluster required
                    makeNewCluster = 0
                    break
            if makeNewCluster == 1:
                clusters.append(Cluster(problem, parkings[ip], clients[ic]))
            else:
                clusters[icluster] = cluster;

    service_time_of_cluster(clusters)
    number_of_required_parkings(clusters)
    print("Clusters: ", len(clusters))
    return(clusters)


def customer_near_distance_sevice_time(problem):
    """
    Return a list of clusters with clients added to their nearest parking site.

    Add client to an existing cluster with the nearest parking site of the client 
    or create a new cluster with the nearest parking site if the result
    would be infeasible. The service time of each cluster should be minimized
    """

    clients = problem.clients
    parkings = problem.parkings

    d = problem.d  # distance

    clusters = []
    for c in clients:
        min_parking = problem.closest_parking(c).id
        #print(min_parking)
        if cfg.DEBUG_HEURISTICS:
            print('considering customer', c)
        attractiveness = 0

        decision = -1

        # attractiveness for new cluster:
        new_attractiveness = evaluate_new_cluster(problem, problem.closest_parking(c), c)
        if new_attractiveness > attractiveness:
            attractiveness = new_attractiveness
            newCluster = problem.closest_parking(c)
            decision = -1
        if cfg.DEBUG_HEURISTICS:
            print('attrac_new: ', attractiveness)

        # attractiveness for existing cluster:
        for icluster, cluster in enumerate(clusters):
            if cluster.parking.id == min_parking:
                new_attractiveness = cluster.evaluate_addition(c)
                if new_attractiveness > attractiveness:
                    attractiveness = new_attractiveness
                    newCluster = cluster
                    decision = icluster

        if cfg.DEBUG_HEURISTICS:
            print('attrac: ', attractiveness)

        if decision == -1:
            newCluster = Cluster(problem, newCluster, c)
            if cfg.DEBUG_HEURISTICS:
                print('creating new cluster:', newCluster.parking, newCluster.clients[0], attractiveness)
            clusters.append(newCluster)
        else:
            if cfg.DEBUG_HEURISTICS:
                print('adding: {} to {} ({})'.format(c.id, newCluster.id, attractiveness))
            newCluster.add(c)
    service_time_of_cluster(clusters)  
    number_of_required_parkings(clusters)
    print("Clusters: ", len(clusters))
    return(clusters)


def parallel_insertion(problem):
    """
    Return a list of clusters with all clients feasibly assigned to a cluster.

    This heuristic constructs clusters in parallel. Customers are inserted to
    clusters one after the other. For each customer, every feasible parking and
    cluster are evaluated by an attractiveness function before the customer is
    inserted.

    TODO: stochastic could be used in the order in which customers are inserted
    or via the attractiveness function (the latter is more sophisticated)

    Note: sorting customers by their attractiveness of creating a new cluster
    actually harms the solution quality (on average)
    #clients = sorted(problem.clients, reverse=True,
                    #key=lambda c: c.new_attractiveness)

    """
    clusters = []
    # TODO: would be faster and still good to only consider closest parking
    # TODO: do this when reading the problem and don't repeat it here
    # more efficient: try closest parking: if feasible, stop; otherwise go on
    # best: simply used improved problem.closest_parking(.)
    # the issue would be that for the unlikely case that a closest parking
    # is not feasible b/c the client is located between the depot and the
    # parking, the closest parking is not feasible (arrival from depot on
    # time or returning to depot on time)
    # improve this by improving problem.closest_parking(.)
    for client in problem.clients:
        client.new_attractiveness = 0
        for parking in client.parkings:
            attractiveness = evaluate_new_cluster(problem, parking, client)
            if attractiveness > client.new_attractiveness:
                client.new_attractiveness = attractiveness
                client.best_parking = parking
    for client in problem.clients:
        max_attractiveness = client.new_attractiveness
        create_cluster = True
        for cluster in clusters:
            attractiveness = cluster.evaluate_addition(client)
            if attractiveness > max_attractiveness:
                max_attractiveness = attractiveness
                destination = cluster
                create_cluster = False
                if cfg.DEBUG_HEURISTICS:
                    print('attrac: ', attractiveness)
        if create_cluster:
            clusters.append(Cluster(problem, client.best_parking, client))
        else:
            destination.add(client)
    return clusters


def sequential_insertion(problem):
    """
    Return a list of clusters with all clients feasibly assigned to a cluster.

    This heuristic constructs clusters sequentially.

    Options: sort customers to start selecting the ones having
    a) the longest total service times including the walking times
       => bad idea (obj. fun. value increases by ~50% over parallel ins.)
    b) the tightest time windows
       => bad idea (obj. fun. value increases by ~50% over parallel ins.)
    c) the highest "new_attractiveness"
       => works (but not as good as the parallel insertion heuristic)
    Not sure if this makes sense as there is no way to know when to stop
    filling a cluster.
    Another issue: if there is a client with a very low "new_attractiveness",
    this client will force all clusters to be really small until it is added
    to a cluster; test if this matters! alternative: use sorting c)
    """
    clusters = []
    for client in problem.clients:
        client.new_attractiveness = 0
        for parking in client.parkings:
            attractiveness = evaluate_new_cluster(problem, parking, client)
            if attractiveness > client.new_attractiveness:
                client.new_attractiveness = attractiveness
                client.new_parking = parking
    new_attractiveness = sorted(problem.clients,
                                key=lambda c: c.new_attractiveness)
    # sort by a) (longest total service time)
    #unassigned = list(reversed(new_attractiveness))
    # sort by b) (tightest time window at end of list)
    #unassigned = sorted(problem.clients, key=lambda c: c.lst - c.est,
                        #reverse=True)
    # sort by c) (clients with highest attractiveness to create new cluster first)
    unassigned = list(new_attractiveness)
    while unassigned:
        client = unassigned.pop()
        cluster = Cluster(problem, client.new_parking, client)
        new_attractiveness.remove(client)
        while unassigned:
            max_attractiveness = new_attractiveness[-1].new_attractiveness
            for client in unassigned:
                attractiveness = cluster.evaluate_addition(client)
                if attractiveness > max_attractiveness:
                    max_attractiveness = attractiveness
                    new_client = client
            if max_attractiveness > new_attractiveness[-1].new_attractiveness:
                cluster.add(new_client)
                new_attractiveness.remove(new_client)
                unassigned.remove(new_client)
            else:
                break
        clusters.append(cluster)
    return clusters


def combined_insertion(problem):
    """
    Return a list of clusters with all clients feasibly assigned to a cluster.

    This approach combines the sequential and parallel heuristics in that all
    customers are considered (sequential approach) for insertion in all
    clusters (parallel approach).
    """
    clusters = []
    for client in problem.clients:
        client.new_attractiveness = 0
        for parking in client.parkings:
            attractiveness = evaluate_new_cluster(problem, parking, client)
            if attractiveness > client.new_attractiveness:
                client.new_attractiveness = attractiveness
                client.best_parking = parking
    unassigned = sorted(problem.clients,
                        key=lambda c: c.new_attractiveness)

    while unassigned:
        max_attractiveness = unassigned[-1].new_attractiveness
        create_cluster = True
        for client in unassigned:
            for cluster in clusters:
                attractiveness = cluster.evaluate_addition(client)
                if attractiveness > max_attractiveness:
                    max_attractiveness = attractiveness
                    candidate = client
                    destination = cluster
                    create_cluster = False
        if create_cluster:
            client = unassigned.pop()
            clusters.append(Cluster(problem, client.best_parking, client))
        else:
            unassigned.remove(candidate)
            destination.add(candidate)
    return clusters


def customer_by_time_min_cluster(problem):
    """
    Return a list of clusters, where clients are added to a cluster or parking by time

    Evaluate every parking and cluster by attractiveness function
    """

    clients = problem.clients

    # set up clusters
    #################
    clusters = []
    for ic, c in enumerate(clients):
        if cfg.DEBUG_HEURISTICS:
            print('considering customer', c)
        attractiveness = 0

        decision = -1
        newCluster = problem.closest_parking(c)

        # attractiveness for existing clusters:
        for icluster, cluster in enumerate(clusters):
            new_attractiveness = cluster.evaluate_addition(c)
            if new_attractiveness > attractiveness:
                attractiveness = new_attractiveness
                newCluster = cluster
                decision = icluster

        if cfg.DEBUG_HEURISTICS:
            print('attrac: ', attractiveness)

        if decision == -1:
            newCluster = Cluster(problem, newCluster, c)
            if cfg.DEBUG_HEURISTICS:
                print('creating new cluster:', newCluster.parking,
                      newCluster.clients[0], attractiveness)
            clusters.append(newCluster)
        else:
            if cfg.DEBUG_HEURISTICS:
                print('adding: {} to {} ({})'.format(c.id, newCluster.id,
                                                     attractiveness))
            newCluster.add(c)

    service_time_of_cluster(clusters)
    number_of_required_parkings(clusters)
    print("Clusters: ", len(clusters))
    return clusters


def service_time_of_cluster(clusters):
    """
    Print the sum of the service times of each cluster
    """
    total_st_clusters = []
    for clu in clusters:
        total_st_clusters.append(clu.sts[cfg.MAX_WORKERS])
    if cfg.DEBUG_HEURISTICS:
        print("Total ST clusters: ", sum(total_st_clusters))


def number_of_required_parkings(clusters):
    """
    Print the number of required parkings for all clusters
    """

    all_parkings = []
    for c in clusters:
        p = c.parking.id
        is_new = 1
        for ap in all_parkings:
            if ap == p:
                is_new = 0
        if is_new == 1:
            all_parkings.append(p)
    if cfg.DEBUG_HEURISTICS:
        print("Parkings:", len(all_parkings))


heuristic = {
    "trivial": each_customer_in_own_cluster,
    "by_distance": customer_near_distance_min_cluster,
    "by_distance2": customer_near_distance_sevice_time,
    "combined": combined_insertion,
    "parallel": parallel_insertion,
    "sequential": sequential_insertion,
    "smart2": customer_by_time_min_cluster,
}
