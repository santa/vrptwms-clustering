/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <gtest/gtest.h>

#include "matrix.hpp"
#include "parking.hpp"
#include "problem.hpp"

namespace fs = boost::filesystem;


namespace VRPTWMS {
const std::string test_instance("unit_tests.json");
const std::string alternate_test_instance("unit_tests_clustering.json");

// A new one of these is created for each test
class TestProblem : public testing::Test {
public:
  Problem pb_;
  Problem pb2_;

  TestProblem()
    : pb_(get_instance_path(test_instance)),
      pb2_(get_instance_path(alternate_test_instance)) {}

  virtual void SetUp() {
  }

  virtual void TearDown()
  {
  }

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }

};


TEST_F(TestProblem, test_num_clients) {
  ASSERT_EQ(12, pb_.clients().size());
}


TEST_F(TestProblem, test_num_parkings) {
  ASSERT_EQ(4 + 1, pb_.parkings().size());  // includes the depot parking
}


TEST_F(TestProblem, test_depot) {
  ASSERT_EQ(0, pb_.depot().id());
  ASSERT_EQ(0.0, pb_.depot().x());
  ASSERT_EQ(0.0, pb_.depot().y());
  ASSERT_EQ(0.0, pb_.depot().est());
  ASSERT_EQ(480.0, pb_.depot().lst());
}


TEST_F(TestProblem, test_depot2) {
  ASSERT_EQ(0, pb2_.depot().id());
  ASSERT_EQ(-3.0, pb2_.depot().x());
  ASSERT_EQ(-1.0, pb2_.depot().y());
  ASSERT_EQ(0.0, pb2_.depot().est());
  ASSERT_EQ(480.0, pb2_.depot().lst());
}


TEST_F(TestProblem, test_distance_matrix) {
  ASSERT_DOUBLE_EQ(0.0, pb_.d()[0][0]);
  ASSERT_DOUBLE_EQ(3.1622776601683795, pb_.d()[0][11]);  // depot -> parking
  ASSERT_DOUBLE_EQ(pb_.d()[0][11], pb_.d()[11][0]);  // depot <- parking
  ASSERT_DOUBLE_EQ(3.1622776601683795, pb_.d()[0][5]);  // depot -> client
  ASSERT_DOUBLE_EQ(pb_.d()[0][5], pb_.d()[5][0]);  // depot <- client
  ASSERT_DOUBLE_EQ(1.0, pb_.d()[1][6]);  // parking -> parking
  ASSERT_DOUBLE_EQ(pb_.d()[1][6], pb_.d()[6][1]);  // parking <- parking
  ASSERT_DOUBLE_EQ(0.0, pb_.d()[10][14]);  // client -> client
  ASSERT_DOUBLE_EQ(pb_.d()[10][14], pb_.d()[14][10]);  // client <- client
  ASSERT_DOUBLE_EQ(2.0, pb_.d()[1][3]);  // parking -> client
  ASSERT_DOUBLE_EQ(pb_.d()[1][3], pb_.d()[3][1]);  // parking <- client
}


TEST_F(TestProblem, test_distance_matrix2) {
  ASSERT_DOUBLE_EQ(0.0, pb2_.d()[0][0]);
  ASSERT_DOUBLE_EQ(63.89053137985315, pb2_.d()[0][27]);  // depot -> cluster
  ASSERT_DOUBLE_EQ(pb2_.d()[0][27], pb2_.d()[27][0]);  // depot <- cluster
  ASSERT_DOUBLE_EQ(85.47514258543241, pb2_.d()[27][29]);  // cluster -> cluster
  ASSERT_DOUBLE_EQ(pb2_.d()[29][27], pb2_.d()[27][29]);  // cluster <- cluster
}


TEST_F(TestProblem, test_parking) {
  const Parking& p = pb_.parking(11);
  ASSERT_EQ(11, p.id());
  ASSERT_EQ(-3.0, p.x());
  ASSERT_EQ(1.0, p.y());
}
// TEST_F(TestProblem, test_calc_distance) {
//   Route* r = new Route(pb.nodes[2]);  // dist from depot: 18
//   pb.routes.push_back(r);
//   ASSERT_EQ(36, pb.calc_distance());
//   Route* r2 = new Route(pb.nodes[24]);  // dist from depot: 30
//   pb.routes.push_back(r2);
//   ASSERT_EQ(96, pb.calc_distance());
// }
// 
// 
// // forward declaration of calc_savings (not in algorithm.cpp's public interface)
// void calc_savings(std::vector<Saving>& v, Problem& pb);
// 
// // A new one of these is created for each test
// class TestSaving : public testing::Test {
// public:
//   Problem pb;
//   std::vector<Saving> savings;
// 
//   TestSaving(): pb("../data/R101_25.txt") { }
// 
//   virtual void SetUp()
//   {
//     Route::pb = &pb;
//     pb.init_routes();
//     calc_savings(savings, pb);
//   }
// 
//   virtual void TearDown()
//   {
//   }
// };
// 
// 
// TEST_F(TestSaving, test_calc_savings) {
//   // calc_savings must not change the routes
//   for (std::vector<Route>::size_type i = 0; i < pb.routes.size(); ++i) {
//     pb.routes[i]->nodes[1]->id = pb.nodes[i + 1].id;
//   }
// }
// 
// 
// TEST_F(TestSaving, test_saving_is_feasible) {
//   Saving s = savings[savings.size() - 1];
//   ASSERT_TRUE(s.is_feasible());
//   // test capacity constraint
//   s.first->route->load = pb.capacity + 1;
//   ASSERT_FALSE(s.is_feasible());
// }
// 
// 
// // A new one of these is created for each test
// class TestAlgorithm : public testing::Test {
// public:
//   Problem pb;
//   std::vector<Saving> savings;
// 
//     TestAlgorithm(): pb("../data/test.txt") { }
// 
//   virtual void SetUp()
//   {
//     Route::pb = &pb;
//     solve_savings_heuristic(pb);
//   }
// 
//   virtual void TearDown()
//   {
//   }
// };
// 
// 
// TEST_F(TestAlgorithm, test_savings_heuristic) {
//   // the hand calculated result is
//   // [0,2,1,10,9,0], [0,3,4,6,7,0] and [0,5,8,0] w/ distance ~43.0491
//   ASSERT_NEAR(43.0491, pb.calc_distance(), 0.001);
// }
}
