/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <gtest/gtest.h>

#include "matrix.hpp"
#include "problem.hpp"
#include "route.hpp"

#include "solution.hpp"

namespace fs = boost::filesystem;


namespace VRPTWMS {
const std::string clustering_instance("unit_tests_clustering.json");
const std::string routing_instance("unit_tests_routing.json");


// A new one of these is created for each test
class TestSolution : public testing::Test {
public:
  Problem pb_;
  Solution sol_;
  Route r1_;
  Route r2_;

  TestSolution()
    : pb_(get_instance_path(clustering_instance)),
      sol_{pb_, get_instance_path(routing_instance)},
      r1_{pb_, sol_.clusters()[0]},
      r2_{pb_, sol_.clusters()[1]} {}

  virtual void SetUp() {
  }

  virtual void TearDown()
  {
  }

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }

};


TEST_F(TestSolution, test_num_clusters) {
  ASSERT_EQ(61, sol_.clusters().size());
}

TEST_F(TestSolution, test_distance) {
  ASSERT_DOUBLE_EQ(0.0, sol_.distance()) << "solution w/out routes has 0 distance";
  sol_.routes_.push_back(Route(pb_, sol_.clusters()[0]));
  sol_.routes_.push_back(Route(pb_, sol_.clusters()[1]));
  ASSERT_DOUBLE_EQ(63.89053137985315 * 2 + 34.92849839314596 * 2,
                   sol_.distance()) <<
    "distance of a solution must be the sum of the route's distances";
}

TEST_F(TestSolution, test_workers) {
  ASSERT_EQ(0, sol_.workers()) << "solution w/out routes requires 0 workers";
  sol_.routes_.push_back(r1_);
  r2_.workers_ = 2;
  sol_.routes_.push_back(r2_);
  ASSERT_EQ(5, sol_.workers()) << "# of workers must be sum of workers per route";
}
}
