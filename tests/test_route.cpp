/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <gtest/gtest.h>

#include "cluster.hpp"
#include "insertion.hpp"
#include "problem.hpp"
#include "solution.hpp"

#include "route.hpp"

namespace fs = boost::filesystem;


namespace VRPTWMS {

const std::string clustering_instance("unit_tests_clustering.json");
const std::string routing_instance("unit_tests_routing.json");

// A new one of these is created for each test
class TestRoute : public testing::Test {
public:
  Problem pb_;
  Solution sol_;

  TestRoute()
    : pb_(get_instance_path(clustering_instance)),
      sol_{pb_, get_instance_path(routing_instance)} {}

  virtual void SetUp() {
  }

  virtual void TearDown()
  {
  }

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }

};


TEST_F(TestRoute, test_new_route) {
  Cluster& seed = sol_.clusters()[0];
  Route r(pb_, seed);
  ASSERT_EQ(1, r.num_stops());
  ASSERT_EQ(seed.demand(), r.demand());
  ASSERT_EQ(276.7157287525381, seed.aest()) << "cluster aest must be updated";
  ASSERT_EQ(294.85786437626905, seed.alst()) << "cluster lest must be updated";
}

TEST_F(TestRoute, test_length) {
  Cluster& seed = sol_.clusters()[0];
  Route r(pb_, seed);
  ASSERT_EQ(63.89053137985315 * 2, r.length()) << "length must be dep->c->dep";
  r.insert(r.stops_.begin(), sol_.clusters()[1]);
  ASSERT_EQ(34.92849839314596 + 85.47514258543241 + 63.89053137985315, r.length());
}

TEST_F(TestRoute, test_insert) {
  // a) insert at beginning
  Cluster& seed = sol_.clusters()[0];
  Route r(pb_, seed);
  seed.aest_ = 0.0;  // check if AEST is updated correctly on insert before seed
  r.insert(r.stops_.begin(), sol_.clusters()[1]);
  ASSERT_EQ(2, r.num_stops()) << "new client must be added to stops";
  ASSERT_EQ(seed.demand() + sol_.clusters()[1].demand(), r.demand());
  ASSERT_DOUBLE_EQ(276.7157287525381, seed.aest()) << "seed AEST must be updated";
  ASSERT_DOUBLE_EQ(55.949283631322345, sol_.clusters()[1].aest()) << "cluster aest must be updated";
  ASSERT_DOUBLE_EQ(65.3844718719117, sol_.clusters()[1].alst()) << "cluster lest must be updated";

  // b) insert at end
  Route r2(pb_, seed);
  seed.alst_ = 0.0;  // check if ALST is updated correctly on insert after seed
  r2.insert(r2.stops_.end(), sol_.clusters()[16]);
  ASSERT_EQ(2, r2.num_stops()) << "new client must be added to stops";
  ASSERT_EQ(seed.demand() + sol_.clusters()[16].demand(), r2.demand());
  ASSERT_DOUBLE_EQ(384.8902042976863 - 55.036351623268054 - 43.284271247461902,
                   seed.alst()) << "seed ALST must be correctly updated";
  ASSERT_DOUBLE_EQ(276.7157287525381 + 43.284271247461902 + 55.036351623268054,
                   sol_.clusters()[16].aest()) << "cluster aest must be updated";
  ASSERT_DOUBLE_EQ(384.8902042976863,
                   sol_.clusters()[16].alst()) << "cluster lest must be updated";
}

TEST_F(TestRoute, test_is_feasible) {
  Cluster& seed = sol_.clusters()[0];
  // a) test capacity
  Route r(pb_, seed);
  EXPECT_TRUE(r.is_feasible()) << "feasible route not correctly identified";
  r.demand_ = pb_.capacity() + 1;
  EXPECT_FALSE(r.is_feasible()) << "route with excess demand must not be feasible";

  // b) not enough service workers
  Route r2(pb_, sol_.clusters()[2]);
  r2.workers_ = 2;
  EXPECT_FALSE(r2.is_feasible()) << "route with missing workers must not be feasible";

  // c) time windows feasible with >= 2 workers
  Route r3(pb_, sol_.clusters()[41]);
  r3.insert(r3.stops_.end(), sol_.clusters()[47]);
  EXPECT_TRUE(r3.is_feasible()) << "time windows are feasible with 3 workers";
  r3.workers_ = 2;
  EXPECT_TRUE(r3.is_feasible()) << "time windows are feasible with 2 workers";
  r3.workers_ = 1;
  EXPECT_FALSE(r3.is_feasible()) << "time windows are infeasible with 1 worker";
}

TEST_F(TestRoute, test_is_feasible_with) {
  std::vector<Time> aests;
  // a) cluster requires three workers
  Route r(pb_, sol_.clusters()[2]);
  EXPECT_TRUE(r.is_feasible_with(3, aests)) << "all clusters feasible";
  EXPECT_EQ(1, aests.size()) << "aests must be populated";
  EXPECT_FALSE(r.is_feasible_with(2, aests)) << "cluster requires 3 workers";
  EXPECT_EQ(1, aests.size()) << "aests must be flushed before being populated";
  // b) time windows feasible with >= 2 workers
  Route r2(pb_, sol_.clusters()[41]);
  r2.insert(r2.stops_.end(), sol_.clusters()[47]);
  EXPECT_TRUE(r2.is_feasible_with(3, aests)) << "time windows are feasible with 3 workers";
  EXPECT_EQ(2, aests.size()) << "expecting exactly 2 elements in aests";
  EXPECT_TRUE(r2.is_feasible_with(2, aests)) << "time windows are feasible with 2 workers";
  EXPECT_EQ(2, aests.size()) << "after flushing, aests must contain exactly 2 elements";
  EXPECT_FALSE(r2.is_feasible_with(1, aests)) << "time windows are infeasible with 1 worker";
  EXPECT_EQ(2, aests.size());
}

TEST_F(TestRoute, test_can_insert_one_cluster) {
  Cluster& seed = sol_.clusters()[0];
  Route r(pb_, seed);
  // insertion impossible b/c of time window overlap
  EXPECT_FALSE(r.can_insert(r.stops_.begin(), sol_.clusters()[11]));
  EXPECT_FALSE(r.can_insert(r.stops_.end(), sol_.clusters()[11]));
  EXPECT_FALSE(r.can_insert(r.stops_.end(), sol_.clusters()[1]));
  // insertion possible
  EXPECT_TRUE(r.can_insert(r.stops_.begin(), sol_.clusters()[1]));
  // insertion impossible b/c of quantity restriction
  r.demand_ = r.pb_.capacity();
  EXPECT_TRUE(r.can_insert(r.stops_.begin(), sol_.clusters()[1]));  // capacity is not checked
  EXPECT_FALSE(r.can_insert(r.stops_.begin(), sol_.clusters()[1], true));
}

TEST_F(TestRoute, test_reduce_service_workers) {
  // a) no reduction possible
  Route r(pb_, sol_.clusters()[2]);
  EXPECT_FALSE(r.reduce_service_workers()) << "cluster doesn't allow reduction";
  EXPECT_EQ(3, r.workers()) << "still 3 workers on route";
  // b) reduction to 2 possible
  Route r2(pb_, sol_.clusters()[41]);
  r2.insert(r2.stops_.end(), sol_.clusters()[47]);
  EXPECT_TRUE(r2.reduce_service_workers()) << "reduction possible";
  EXPECT_EQ(2, r2.workers()) << "route updated to 2 workers";
  EXPECT_FALSE(r2.reduce_service_workers()) << "no further reduction possible";
  EXPECT_EQ(2, r2.workers());
}

TEST_F(TestRoute, test_update_insertion) {
  Cluster& seed = sol_.clusters()[0];
  Route r(pb_, seed);
  Insertion i;
  // no update b/c insertion not feasible
  EXPECT_FALSE(r.update_insertion(sol_.clusters()[11], i));
  ASSERT_EQ(nullptr, i.target) << "target must not be changed if no update happend";
  ASSERT_EQ(nullptr, i.c) << "cluster must not be changed if no update happend";
  std::list<Cluster*>::iterator no_iterator;
  ASSERT_EQ(no_iterator, i.pos) << "position must not be changed if no update happend";
  ASSERT_EQ(std::numeric_limits<double>::infinity(), i.cost) << "cost of infeasible insertion must be infinity";
  ASSERT_EQ(-std::numeric_limits<double>::infinity(), i.attr) << "attractivity of infeasible insertion must be -infinity";
  // update ok
  EXPECT_TRUE(r.update_insertion(sol_.clusters()[1], i)) << "feasible update w/ higher attractivity must be performed";
  ASSERT_EQ(&r, i.target) << "target must be updated correctly";
  ASSERT_EQ(&sol_.clusters()[1], i.c) << "cluster must be updated correctly";
  ASSERT_EQ(r.stops_.begin(), i.pos) << "position must be updated correctly";
  // no update b/c better insertion was present
  i.cost = -std::numeric_limits<double>::infinity();
  i.attr = std::numeric_limits<double>::infinity();
  EXPECT_FALSE(r.update_insertion(sol_.clusters()[9], i));  // 9 would be better than 1
  ASSERT_EQ(&sol_.clusters()[1], i.c) << "only update cluster if attractivity increases";
}
}
