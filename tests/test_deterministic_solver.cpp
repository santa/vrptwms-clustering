/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <gtest/gtest.h>

#include "common.hpp"
#include "cluster.hpp"
#include "insertion.hpp"
#include "problem.hpp"
#include "solution.hpp"
#include "route.hpp"

#include "deterministic_solver.hpp"

namespace fs = boost::filesystem;

// TODO: make get_instance_path a shared public function (eg in common)


namespace VRPTWMS {
const std::string clustering_instance("unit_tests_clustering.json");
const std::string routing_instance("unit_tests_routing.json");

// A new one of these is created for each test
class TestDeterministicRoutingSolver : public testing::Test {
public:
  Problem pb_;
  Solution sol_;

  TestDeterministicRoutingSolver()
    : pb_(get_instance_path(clustering_instance)),
      sol_{pb_, get_instance_path(routing_instance)} {}

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }
};


TEST_F(TestDeterministicRoutingSolver, test_get_furthest_seed) {
  DeterministicRoutingSolver solver(sol_);

  Cluster* c = solver.get_furthest_seed();
  ASSERT_EQ(9, c->id());
  ASSERT_EQ(60, solver.unrouted_.size());
}


class TestDeterministicClusteringSolver : public testing::Test {
public:
  Problem pb_;

  TestDeterministicClusteringSolver()
    : pb_(get_instance_path(clustering_instance)){}

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }
};


/**
 * Test if the Constructor correctly initializes the vector of trivial clusters.
 *
 * This is a regression test as the clusters were messed up by sorting.
 */
TEST_F(TestDeterministicClusteringSolver, test_DeterministicClusteringSolver) {
  Cluster::next_cid_ = 0;
  Solution sol(pb_);
  DeterministicClusteringSolver solver(sol);

  for (Cluster& cluster: solver.trivial_clusters_) {
    if (cluster.cid() == 0) {
      ASSERT_EQ(51, cluster.clients()[0]->id()) <<
        "clusters client got messed up during sorting";
      ASSERT_EQ(27, cluster.id()) <<
        "cluster parking got messed up during sorting";
    } else if (cluster.cid() == 199) {
      ASSERT_EQ(250, cluster.clients()[0]->id()) <<
        "clusters client got messed up during sorting";
      ASSERT_EQ(48, cluster.id()) <<
        "cluster parking got messed up during sorting";
    }
  }

//   Cluster* c = solver.get_furthest_seed();
//   ASSERT_EQ(9, c->id());
//   ASSERT_EQ(60, solver.unrouted_.size());
}
}
