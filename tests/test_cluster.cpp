/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <gtest/gtest.h>

#include "client.hpp"
#include "common.hpp"
#include "exceptions.hpp"
#include "parking.hpp"
#include "problem.hpp"
#include "solution.hpp"

#include "cluster.hpp"

namespace fs = boost::filesystem;
using boost::property_tree::ptree;


namespace VRPTWMS {
const std::string test_instance("unit_tests.json");
const std::string clustering_instance("unit_tests_clustering.json");
const std::string routing_instance("unit_tests_routing.json");

const std::string presentation_instance("presentation_scheduling_one_truck.json");

// A new one of these is created for each test
class TestCluster : public testing::Test {
public:
  Problem pb_;
  Problem pb2_;
  Problem presentation_pb_;  // for testing issue #4
  Solution sol2_;

  TestCluster()
    : pb_(get_instance_path(test_instance)),
      pb2_(get_instance_path(clustering_instance)),
      presentation_pb_(get_instance_path(presentation_instance)),
      sol2_(pb2_, get_instance_path(routing_instance)) {}

  virtual void SetUp() {
  }

  virtual void TearDown()
  {
  }

private:
  static std::string get_instance_path(std::string test_instance) {
    fs::path p(__FILE__);
    fs::path filepath = p.parent_path().parent_path() / "data" / "tests";
    filepath /= test_instance;
    return filepath.string();
  }

};


/*
 * Test that clusters are correctly initialized from ptrees.
 */
TEST_F(TestCluster, test_Cluster_ptree) {
  ptree data;
  data.put("demand", 4.0);
  data.put("id", 1);
  data.put("parking_id", 27);
  data.put("min_workers", 2);
  ptree ests, lsts, sts, est2, est3, lst2, lst3, st2, st3, client_ids;
  est2.put("", 242.85786437626905);
  est3.put("", 276.7157287525381);
  lst2.put("", 294.85786437626905);
  lst3.put("", 294.85786437626905);
  st2.put("", 77.284271247461902);
  st3.put("", 43.284271247461902);
  ests.push_back(std::make_pair("", est2));
  ests.push_back(std::make_pair("", est3));
  lsts.push_back(std::make_pair("", lst2));
  lsts.push_back(std::make_pair("", lst3));
  sts.push_back(std::make_pair("", st2));
  sts.push_back(std::make_pair("", st3));
  data.add_child("ests", ests);
  data.add_child("lsts", lsts);
  data.add_child("sts", sts);
  data.add_child("client_ids", client_ids);
  Parking p = pb_.parking(11);
  Cluster c(pb_, p, data);
  ASSERT_EQ(-3, c.x());
  ASSERT_EQ(1, c.y());
  ASSERT_EQ(2, c.min_workers());
  ASSERT_DOUBLE_EQ(4.0, c.demand());
  ASSERT_DOUBLE_EQ(242.85786437626905, c.est(2));
  ASSERT_DOUBLE_EQ(276.7157287525381, c.est(3));
  ASSERT_DOUBLE_EQ(294.85786437626905, c.lst(2));
  ASSERT_DOUBLE_EQ(294.85786437626905, c.lst(3));
  ASSERT_DOUBLE_EQ(77.284271247461902, c.st(2));
  ASSERT_DOUBLE_EQ(43.284271247461902, c.st(3));
}


// Test if the client-only constructor correctly assigns the closest
// feasible parking.
TEST_F(TestCluster, test_Cluster_closest_parking) {
  Client client2(2, 0, 0, 0, 10, 10, 10);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Client client9(9, 0, 1, 0, 10, 10, 10);
  Cluster c1(pb_, client2);  // Parkings 0 and 1 are closest
  Cluster c2(pb_, client3);  // Parking 6 is closest
  Cluster c3(pb_, client9);  // Parkings 0 and 1 are closest
  if (c1.id() != 0 && c1.id() != 1) {
    ASSERT_TRUE(false) << "Client 2 is closest to parkings 0 and 1";
  }
  ASSERT_EQ(6, c2.id());
  if (c3.id() != 0 && c3.id() != 1) {
    ASSERT_TRUE(false) << "Client 2 is closest to parkings 0 and 1";
  }
}

// TODO: there is no client w/ id 6, only a parking; that works, but is
// confusing; => adjust tests using client 6
/*
 * Test if the total service time is calculated correctly.
 */
TEST_F(TestCluster, test_calc_total_st) {
  Parking parking(1, 0, 0);
  Client client6(6, 1, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client6);
  ASSERT_DOUBLE_EQ((2 * 5 + 10) * 3, c.calc_total_st());
  Client client9(9, 0, 1, 0, 10, 10, 10);
  c.add(client9);
  ASSERT_DOUBLE_EQ((2 * 5 + 10) * 2, c.calc_total_st());
}

/*
 * Test that clusters are correctly initialized from routing input files.
 */
TEST_F(TestCluster, test_Cluster_clients) {
  Cluster& c = sol2_.clusters()[0];
  const Client& client_51 = *c.clients_[0];
  const Client& client_211 = *c.clients_[1];
  ASSERT_EQ(51, client_51.id());
  ASSERT_DOUBLE_EQ(309.0, client_51.lst());
  ASSERT_DOUBLE_EQ(2.0, client_51.demand());
  ASSERT_EQ(211, client_211.id());
  ASSERT_DOUBLE_EQ(352.0, client_211.lst());
  ASSERT_DOUBLE_EQ(14.0, client_211.st());
}


/*
 * The service time must be calculated correctly in one-customer clusters.
 *
 * In this case the client and the parking overlap.
 */
TEST_F(TestCluster, test_update_st_single_overlapping) {
  Parking parking(1, 0, 0);
  Client client2(2, 0, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client2);
  ASSERT_DOUBLE_EQ(10.0, c.st(1)) <<
    "The st of the cluster must equal the node's if no walking occurs";
  ASSERT_DOUBLE_EQ(10.0, c.st(2)) <<
    "A second worker cannot speed up the work at a single client";
}


/*
 * The service time must be calculated correctly in one-customer clusters.
 *
 * In this case the client and the parking are at different locations.
 */
TEST_F(TestCluster, test_update_st_single) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  Time st_single = 10 + 2 * pb_.walking_time() * sqrt(4);
  ASSERT_DOUBLE_EQ(st_single, c.st(1)) <<
    "The st of the cluster must equal the node's if no walking occurs";
  ASSERT_DOUBLE_EQ(st_single, c.st(2)) <<
    "A second worker cannot speed up the work at a single client";
}


/*
 * Test service time for two nodes with one, two and three workers.
 */
TEST_F(TestCluster, test_update_times_st_two_clients) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  Client client4(4, 0, 1, 0, 50, 10, 10);
  c.add(client4);
  // c.update_times();  // not needed as it happens in "add"
  Time st_single = (10 + sqrt(4) * pb_.walking_time() * 2 +
    10 + sqrt(1) * pb_.walking_time() * 2);
  Time st_double = std::max(10 + sqrt(4) * pb_.walking_time() * 2,
                            10 + sqrt(1) * pb_.walking_time() * 2);
  Time st_triple = st_double;  // the third worker cannot help here
  ASSERT_DOUBLE_EQ(st_single, c.st(1)) <<
    "Incorrect service time for a single worker";
  ASSERT_DOUBLE_EQ(st_double, c.st(2)) <<
    "Incorrect service time with two workers";
  ASSERT_DOUBLE_EQ(st_triple, c.st(3)) <<
    "A third worker cannot speed up the work if there are two nodes";
}


/*
 * Test service time for three nodes with up to three workers.
 */
TEST_F(TestCluster, test_update_times_st_three_clients) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  Client client4(4, 0, 1, 0, 50, 10, 10);
  c.clients_.push_back(&client4);
  Client client5(5, -3, 1, 0, 75, 10, 10);
  c.clients_.push_back(&client5);
  c.update_times();
  Time st_single = (10 + sqrt(4) * pb_.walking_time() * 2 +
    10 + sqrt(1) * pb_.walking_time() * 2 +
    10 + sqrt(10) * pb_.walking_time() * 2);
  Time st_two(std::max(10 + sqrt(4) * pb_.walking_time() * 2 +
    10 + sqrt(1) * pb_.walking_time() * 2,
                       10 + sqrt(10) * pb_.walking_time() * 2));
  Time st_two_alt(std::max(10 + sqrt(4) * pb_.walking_time() * 2,
                           10 + sqrt(1) * pb_.walking_time() * 2 +
    10 + sqrt(10) * pb_.walking_time() * 2));
  Time st_three = std::max({10 + sqrt(4) * pb_.walking_time() * 2,
                            10 + sqrt(1) * pb_.walking_time() * 2,
                            10 + sqrt(10) * pb_.walking_time() * 2});
  ASSERT_DOUBLE_EQ(st_single, c.st(1)) << "Incorrect st for one worker";
  // don't test the ordering (depends on heuristic)
  Time actual(c.st(2));
  if (almost_equal<Time>(st_two, actual) or almost_equal<Time>(st_two_alt, actual)) {
    ASSERT_DOUBLE_EQ(st_three, c.st(3)) <<
      "A third worker speeds up the work.";
  } else {
    ASSERT_TRUE(false) << "A second worker speeds up the work";
  }
}

/*
 * Test if required waiting is done correctly.
 */
TEST_F(TestCluster, test_update_times_st_waiting) {
  Parking parking(1, 0, 0);
  Client client6(6, 1, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client6);
  Client client2(7, 0, 1, 40, 50, 10, 10);
  c.clients_.push_back(&client2);
  c.update_times();
  Time st_single = 5 + 10 + 5 + 5 + 10 + 10 + 5;  // includes 10 for waiting
  Time st_double = st_single;  // the second worker just waits
  ASSERT_DOUBLE_EQ(st_single, c.st(1)) << "The st must include waiting times";
  ASSERT_DOUBLE_EQ(st_double, c.st(2)) << "The second worker just waits";
}


/*
 * Test if an infeasible cluster is recognized.
 */
TEST_F(TestCluster, test_update_times_infeasible) {
  Parking parking(1, 0, 0);
  Client client6(6, 1, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client6);
  Client client8(8, 0, 3, 0, 10, 10, 10);  // impossible to arrive on time
  ASSERT_FALSE(c.add(client8)) << "walking to client is too slow";
}


/*
 * Test a cluster that requires at least two workers.
 */
TEST_F(TestCluster, test_add_min_two_workers) {
  Parking parking(1, 0, 0);
  Client client6(6, 1, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client6);
  Client client9(9, 0, 1, 0, 10, 10, 10);
  c.add(client9);
  ASSERT_EQ(2, c.min_workers()) << "Cluster requires at least two workers";
  ASSERT_THROW(c.st(1), InfeasibilityException) <<
    "Not feasible with one worker.";
  ASSERT_FALSE(almost_equal<Time>(Constants::NoTime, c.st(2))) <<
    "Feasible with two workers.";
  ASSERT_FALSE(almost_equal<Time>(Constants::NoTime, c.st(3))) <<
    "If feasible with two workers also feasible with more.";
}


/*
 * Test if the aggregate demand is correct.
 */
TEST_F(TestCluster, test_add_client_demand) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  Client client4(4, 0, 1, 0, 50, 10, 10);  // impossible to arrive on time
  c.add(client4);
  ASSERT_DOUBLE_EQ(20, c.demand()) << "Demand must be aggregated correctly";
}


/*
 * Test if the service times are updated correctly.
 */
TEST_F(TestCluster, test_add_client_sts) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  Client client4(4, 0, 1, 0, 50, 10, 10);  // impossible to arrive on time
  c.add(client4);
  ASSERT_DOUBLE_EQ(50, c.st(1)) << "Single worker requires 50 time units";
  ASSERT_DOUBLE_EQ(30, c.st(2)) << "Two workers require 30 time units";
  ASSERT_DOUBLE_EQ(30, c.st(3)) << "Three workers require 30 time units";
  ASSERT_EQ(1, c.min_workers()) << "Cluster requires one worker";
}


/*
 * Test if cloning a cluster sets all values correctly.
 *
 * Using mock cluster (didn't use add to avoid depending on add and
 * update_times).
 */
TEST_F(TestCluster, test_clone_correct_values) {
  Cluster& c(sol2_.clusters()[0]);  // regular constructor would rely on add(.)
  c.min_workers_ = 2;
  c.earliest_start_ = 23;
  c.sts_.at(0) = 5.5;
  c.sts_.at(1) = 4.5;
  c.sts_.at(2) = 4.5;
  c.ests_.at(0) = 123.5;
  c.ests_.at(1) = 124.5;
  c.ests_.at(2) = 124.5;
  c.lsts_.at(0) = 133.5;
  c.lsts_.at(1) = 134.5;
  c.lsts_.at(2) = 134.5;
  c.demand_ += 3.5;
  Cluster c2(c);
  ASSERT_EQ(c.id(), c2.id());
  ASSERT_EQ(c.cid(), c2.cid());
  ASSERT_EQ(c.min_workers(), c2.min_workers());
  ASSERT_DOUBLE_EQ(c.earliest_start_, c2.earliest_start_);
  ASSERT_DOUBLE_EQ(c.st(2), c2.st(2)) << "st2 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.st(3), c2.st(3)) << "st3 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.est(1), c2.est(1)) << "est1 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.est(2), c2.est(2)) << "est2 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.est(3), c2.est(3)) << "est3 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.lst(1), c2.lst(1)) << "lst1 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.lst(2), c2.lst(2)) << "lst2 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.lst(3), c2.lst(3)) << "lst3 must be cloned correctly";
  ASSERT_DOUBLE_EQ(c.demand(), c2.demand());
  ASSERT_DOUBLE_EQ(c.x(), c2.x());
  ASSERT_DOUBLE_EQ(c.y(), c2.y());
  for (unsigned int i = 0; i < c.clients().size(); ++i) {
    ASSERT_EQ(c.clients().at(i), c2.clients().at(i)) <<
      "Clients must be in the same order";
  }
}


/*
 * Test if cloning a cluster makes the clone independent.
 *
 * Using mock cluster (didn't use add to avoid depending on add and
 * update_times).
 */
TEST_F(TestCluster, test_clone_independent) {
  Cluster& c(sol2_.clusters()[0]);  // regular constructor would rely on add(.)
  Cluster c2(c);
  Client client9(9, 0, 1, 0, 10, 10, 10);  // impossible to arrive on time
  c.clients_.push_back(&client9);
  c.min_workers_ = 2;
  c.sts_.at(0) = 5.5;
  c.sts_.at(1) = 4.5;
  c.demand_ += client9.demand();
  ASSERT_NE(c.min_workers(), c2.min_workers());
  ASSERT_DOUBLE_EQ(77.284271247461902, c2.st(1));  // value from input file
  ASSERT_FALSE(almost_equal<Amount>(c.demand(), c2.demand()));
}


/*
 * Test if the demand of a new cluster is calculated correctly.
 *
 * This test ensures that the demand is independent of prior clusters to avoid
 * a regression.
 */
TEST_F(TestCluster, test_demand) {
  Parking parking(1, 0, 0);
  Client client3(3, 2, 0, 0, 10, 10, 10);
  Cluster c(pb_, parking, client3);
  ASSERT_DOUBLE_EQ(10, c.demand());
  Client client4(4, 0, 1, 0, 50, 10, 10);  // impossible to arrive on time
  Cluster c2(pb_, parking, client4);
  ASSERT_DOUBLE_EQ(10, c2.demand()) << "cluster demand is incorrect";
}


/*
 * TC for Issue #4.
 * Discrepancy between lst calculation and latest finish time calculation.
 *
 * No assertion in the code must be triggered and all additions are feasible.
 */
TEST_F(TestCluster, test_issue_4) {
  Client c8(presentation_pb_.client(8));
  Client c11(presentation_pb_.client(11));
  Client c12(presentation_pb_.client(12));
  Client c14(presentation_pb_.client(14));
  Parking p3(presentation_pb_.parking(3));
  Cluster cluster(presentation_pb_, p3, c8);
  ASSERT_TRUE(cluster.add(c11)) << "Adding client 11 must be feasible";
  ASSERT_TRUE(cluster.add(c12)) << "Adding client 12 must be feasible";
  ASSERT_TRUE(cluster.add(c14)) << "Adding client 14 must be feasible";
}
}
