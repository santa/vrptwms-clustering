# VRPTWMS solver with on the fly customer clustering.
# Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

cmake_minimum_required(VERSION 2.8)

project(clustering)

# Compiler options
# set(CMAKE_CXX_COMPILER "clang++")
set(CMAKE_CXX_COMPILER "g++")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -Wconversion")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0 -std=c++11 -Wall -Wextra -Wconversion -DDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")

SET(BOOST_MIN_VERSION "1.53.0")
find_package(Boost ${BOOST_MIN_VERSION}
  COMPONENTS filesystem
             math_tr1
             program_options
             system
             timer REQUIRED)
# include_directories(${INCLUDE_DIRECTORIES} ${Boost_INCLUDE_DIRS})
# link_directories(${LINK_DIRECTORIES} ${Boost_LIBRARY_DIRS})

find_library(LIB_dl dl)
FIND_PACKAGE(FLTK REQUIRED)


set(LIBNAME ${PROJECT_NAME}_lib)
set(LIBS ${LIBS} ${LIBNAME}
  boost_filesystem boost_program_options boost_system boost_timer)

set(SOURCE_DIR src)
set(BIN_INSTALL_DIR bin)

set(PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/${SOURCE_DIR})
# include_directories(${PROJECT_SOURCE_DIR}/${SOURCE_DIR})

add_subdirectory(${SOURCE_DIR})
enable_testing()
add_subdirectory(tests)

# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
  configure_file(Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
  add_custom_target(doc
    ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
#     WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating API documentation with Doxygen" VERBATIM
  )
endif(DOXYGEN_FOUND)
