#!/bin/bash

# This script executes the routing solver for all available instances in data/.
# All parameters to this script are passed to the vrptwms call.

# Make sure you installed the most recent clustering_cli to a location in your
# path!

cluster_solution_dir=~/Ubuntu\ One/uni/results/clustering/
clustering_heuristic='parallel'  # trivial, by_distance, parallel, sequential
outfile=${clustering_heuristic}.out

if [ -f ${outfile} ]; then  # remove outfile if it already exists
  rm ${outfile}
fi

for f in data/?200*; do
  echo "processing ${f}"
  clusters=${f:5}
  clusters=${clusters%\.json}
  clusters=`ls "${cluster_solution_dir}${clustering_heuristic}/"${clusters}*`
  clustering_cli ${f} --clusters "${clusters}" >> $outfile
done
