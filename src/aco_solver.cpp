/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <iostream>
#include <utility>  // std::pair

#include <boost/timer/timer.hpp>
#include <boost/utility.hpp>  // boost::prior

#include "common.hpp"
#include "insertion.hpp"
#include "problem.hpp"
#include "solution.hpp"
#include "solver.hpp"
#include "weighted_roulette_wheel.hpp"

#include "aco_solver.hpp"

using boost::prior;
using boost::timer::cpu_timer;


namespace VRPTWMS {
/**
 * Constructor.
 */
ACOSolver::ACOSolver(Solution& sol,
                     std::function<void (const Matrix<double>& clustering_ph,
                                         const Matrix<double>& routing_ph,
                                         const Matrix<double>& opening_ph,
                                         const Matrix<double>& closing_ph)>
                                         gui_callback)
    : pb_(sol.pb()),
      best_sol_(sol),
      sol_(sol.pb()),
      solution_counter(0),
      gui_callback_(gui_callback),
      new_pheromone_(1.0 - sol.pb().cfg().rho()),
      clustering_ph_(static_cast<int>(sol.pb().num_locations()),
                     static_cast<int>(sol.pb().num_locations()),
                     sol.pb().cfg().initial_pheromone()),
      routing_ph_(static_cast<int>(sol.pb().num_locations()),
                  static_cast<int>(sol.pb().num_locations()),
                  sol.pb().cfg().initial_pheromone()),
      opening_ph_(static_cast<int>(sol.pb().clients().size()),
                  static_cast<int>(sol.pb().num_locations()),
                  sol.pb().cfg().initial_pheromone()),
      closing_ph_(static_cast<int>(sol.pb().clients().size()),
                  static_cast<int>(sol.pb().num_locations()),
                  sol.pb().cfg().initial_pheromone())
{
  for (const Client& client: pb_.clients()) {
    trivial_clusters_.push_back(Cluster(pb_, client));
  }
  std::sort(trivial_clusters_.begin(), trivial_clusters_.end(),
            [](const Cluster& a, const Cluster& b) {
              return a.attractiveness(true) < b.attractiveness(true);
            });
}


/**
 * Return required CPU time in nanoseconds.
 */
nanosecond_type ACOSolver::cpu_time() const
{
  return timer_.elapsed().user + timer_.elapsed().system;
}


void ACOSolver::solve()
{
  timer_.start();
  solve_aco();
  timer_.stop();
  Seconds seconds(to_seconds(cpu_time()));
  if (not seconds) {
    seconds++;
  }
  std::ofstream outfile;
  outfile.open(pb_.cfg().outfile(), std::ios::app);
  outfile << *this << "\n" << std::endl;
  outfile.close();
  if (pb_.cfg().verbosity() >= VerbosityLevel::BASIC) {
    std::cout << "calculated " << solution_counter << " solutions in ";
    std::cout << seconds << " seconds (";
    std::cout << solution_counter / seconds << " solutions per second)\n";
  }
}


/////////////////////
// private methods //
/////////////////////

/**
 * Log details about the obtained solution to given output stream.
 */
std::ostream& operator<<(std::ostream& os, const ACOSolver& s)
{
  Seconds runtime = s.best_sol_.milliseconds() / 1000;
  runtime = runtime ? runtime : 1;  // avoid 0 division
  os << s.pb_.name() << "\n";
  os << s.pb_.cfg() << "\n";
  os << "calculated " <<  s.solution_counter / runtime << " iterations/s\n";
  s.best_sol_.write_details(os);
  return os;
}


/**
 * Return true iif the configured runtime limit is not reached, otherwise false.
 */
bool ACOSolver::proceed()
{
  return to_seconds(cpu_time()) < pb_.cfg().max_runtime();
}


/**
 * Print status message to stdout iif verbose output is activated.
 */
void ACOSolver::report_progress()
{
  if (pb_.cfg().verbosity() >= VerbosityLevel::BASIC) {
    std::cout << best_sol_ << "\n";
  }
}

void ACOSolver::solve_aco()
{
  while (proceed()) {
    for (Index ant = 0; ant < pb_.cfg().ants(); ++ant) {
      sol_.reset();
      solve_clustering(pb_.cfg().weighted());
      solve_routing();
      sol_.improve();
      solution_counter++;
      if (sol_.total_cost() <  best_sol_.total_cost() or
          not best_sol_.trucks()) {  // no best solution yet
        best_sol_ = std::move(sol_);
        best_sol_.set_cpu_time(cpu_time());
        report_progress();
      }
    }
    update_pheromone();
  }
}


/**
 * Allows automatic switching between different clustering heuristics.
 *
 */
void ACOSolver::solve_clustering(bool weighted)
{
  unassigned_.assign(trivial_clusters_.begin(), trivial_clusters_.end());

  // Switch heuristic; currently, only the sequential heuristic is implemented.
  solve_sequential_heuristic(weighted);
}


/**
 * Solve a stochastic version of the sequential clustering heuristic.
 *
 * This function makes decisions based on the pheromone laid by the best prior
 * solution.
 */
void ACOSolver::solve_sequential_heuristic(bool weighted)
{
  while (!unassigned_.empty()) {
    WeightedRouletteWheel<const std::_List_iterator <Cluster> > seed_wheel(Constants::MinAttractiveness);
    for (auto it = unassigned_.begin(); it != unassigned_.end(); ++it) {
      double attr = std::max(it->attractiveness(), Constants::MinAttractiveness);
      seed_wheel.add(attr * trail(it->id(), *(it->clients()[0])), it);
    }
    auto& seed(seed_wheel.spin().second);
    Cluster cluster = std::move(*seed);  // move saves from destruction
    unassigned_.erase(seed);  // destroys the element at back()
    while (!unassigned_.empty()) {
      WeightedRouletteWheel<const Client*> roulette_wheel(Constants::MinAttractiveness);
      double attr = cluster.attractiveness(weighted);
      attr = (attr > 0) ? attr : Constants::MinAttractiveness;
      roulette_wheel.add(attr * trail(cluster), nullptr);
      for (const Client* client : pb_.clients(cluster.id())) {
        if (not is_unassigned(unassigned_, *client)) continue;
        roulette_wheel.add(cluster.evaluate_addition(*client, weighted) * trail(cluster, *client), client);
      }
      auto& pair(roulette_wheel.spin());

      if (pair.second) {  // a client is given (see nullptr assignment above)
        cluster.add(*pair.second);
        remove_unassigned(unassigned_, *pair.second);
      } else {
        break;
      }
    }
    sol_.clusters().push_back(cluster);
  }
}


void ACOSolver::solve_routing()
{
  unrouted_.clear();
  for (auto it = sol_.clusters().begin(); it != sol_.clusters().end(); ++it) {
    unrouted_.push_back(&(*it));
  }
  solve_solomon();
}


/**
 * Construct a guided stochastic solution for the VRPTWMS routing problem.
 *
 * This heuristic learns from the best prior solutions available.
 * Each route is initialized by default with the maximum number of workers.
 */
void ACOSolver::solve_solomon()
{
  while (!unrouted_.empty()) {
    Route r(pb_, *pick_routing_seed());
    while (!unrouted_.empty()) { // fill the current route
      Insertion ins(pick_insertion(r));
      if (not ins.c) {
        break;
      }
      r.insert(ins.pos, *ins.c);
      unrouted_.erase(ins.unrouted_it);
    }
    sol_.add_route(r);
  }
}


/**
 * Return a cluster pointer to be used to create the next route.
 *
 * The seed selection's attractiveness is directly proportional to a cluster's
 * distance from the depot. It further depends on whether the cluster's
 * locations (parking and clients) were part of a seed cluster in the best
 * known solution.
 */
Cluster* ACOSolver::pick_routing_seed()
{
  assert(!unrouted_.empty());
  WeightedRouletteWheel<const std::_List_iterator <Cluster*> > seed_wheel(Constants::MinAttractiveness);
  for (auto it = unrouted_.begin(); it != unrouted_.end(); ++it) {
    Attractiveness attr = pb_.d((*it)->id());
    attr = (attr > 0) ? attr : Constants::MinAttractiveness;
    attr *= trail(sol_.trucks(), **it, sol_.trucks());
    seed_wheel.add(attr, it);
  }
  auto& seed_it(seed_wheel.spin().second);
  Cluster* seed_ptr(*seed_it);
  unrouted_.erase(seed_it);
  return seed_ptr;
}


/**
 * Return an insertion chosen with a weighted random wheel.
 *
 * If no insertion is feasible, an insertion w/out a cluster is returned.
 * The pheromone trail for the ACO is used in Route::update_insertion. This is
 * necessary b/c update_insertion changes puts the given cluster to the best
 * position of a route which depends on the pheromone trail.
 */
Insertion ACOSolver::pick_insertion(Route& r)
{
  WeightedRouletteWheel<const Insertion> insertion_wheel(Constants::MinAttractiveness);
  for (auto it = unrouted_.begin(); it != unrouted_.end(); ++it) {
    Insertion ins;
    if (r.update_insertion(**it, ins, this)) {
      ins.unrouted_it = it;
      insertion_wheel.add(ins.attr, ins);
    }
  }
  if (insertion_wheel.empty()) {
    return Insertion();
  }
  return insertion_wheel.spin().second;
}


/**
 * Return the pheromone occurring between two clusters.
 *
 * Required for the routing trail (routing).
 * The pheromone consists of the sum of all links between the two given
 * clusters. Consequently (m+1)*(n+1) matrix lookups are required where m and n
 * are the number of clients in the first and second cluster. The '+1' denotes
 * each cluster's parking. This total pheromone is normalized to be neutral to
 * cluster size (this avoids a bias for large clusters which would otherwise be
 * preferred). The normalization is done by dividing the pheromone components
 * by the number of combinations occurring in the component: (m+1)*(n+1).
 */
double ACOSolver::pheromone(const Cluster& prior, const Cluster& cluster) const
{
  // parking -> parking
  double total_pheromone(routing_ph_[prior.id()][cluster.id()]);
  // parking -> clients
  for (const Client* client: cluster.clients()) {
    total_pheromone += routing_ph_[prior.id()][client->id()];
  }
  for (const Client* prior_client: prior.clients()) {
    // clients -> parking
    total_pheromone += routing_ph_[prior_client->id()][cluster.id()];
    // clients -> clients
    for (const Client* client: cluster.clients()) {
      total_pheromone += routing_ph_[prior_client->id()][client->id()];
    }
  }
  auto links = ((prior.clients().size() + 1) * (cluster.clients().size() + 1));
  return total_pheromone / static_cast<double>(links);
}


/**
 * Return the pheromone between a virtual depot and a cluster.
 *
 * Required by the routing heuristic to determine whether a cluster
 * succeeded the virtual depot.
 * The pheromone between a virtual depot and a cluster consists of k+1 look-ups
 * b/c a single virtual depot is mapped to k+1 locations (k clients and one
 * parking); to avoid a bias for large clusters, the pheromone is normalized by
 * dividing it through (k+1)
 */
double ACOSolver::pheromone(const Index vdepot_id, const Cluster& cluster) const
{
  double total_pheromone(opening_ph_[vdepot_id][cluster.id()]);  // parking
  for (const Client* client_ptr: cluster.clients()) {
    total_pheromone += opening_ph_[vdepot_id][client_ptr->id()];
  }
  return total_pheromone / static_cast<double>(cluster.clients().size() + 1);
}


/**
 * Return the pheromone between a cluster and a virtual depot.
 *
 * Required by the routing heuristic to determine whether a cluster preceded
 * the virtual depot.
 * The pheromone between a virtual depot and a cluster consists of k+1 look-ups
 * b/c a single virtual depot is mapped to k+1 locations (k clients and one
 * parking); to avoid a bias for large clusters, the pheromone is normalized by
 * dividing it through (k+1)
 */
double ACOSolver::pheromone(const Cluster& cluster, const Index vdepot_id) const
{
  double total_pheromone(closing_ph_[vdepot_id][cluster.id()]);  // parking
  for (const Client* client_ptr: cluster.clients()) {
    total_pheromone += closing_ph_[vdepot_id][client_ptr->id()];
  }
  return total_pheromone / static_cast<double>(cluster.clients().size() + 1);
}


/**
 * Return the pheromone between a client and a parking.
 *
 * Required for the seed of a new cluster (clustering).
 * Answers whether currently investigated client was associated w/ current
 * given parking in BKS?
 * A simple 1 value matrix lookup is enough.
 */
double ACOSolver::pheromone(Index parking_id, const Client& client) const
{
  return clustering_ph_[parking_id][client.id()];
}


/**
 * Return the pheromone between a cluster and a client.
 *
 * Required for adding a client to a cluster (clustering).
 * k + 1 lookups have to be performed where k is the number of clients in the
 * cluster prior to adding the given client. The '+ 1' stems from the parking
 * location.
 * To avoid a bias for large clusters, the pheromone is normalized by dividing
 * it by (k+1).
 */
double ACOSolver::pheromone(const Cluster& cluster, const Client& client) const
{
  double total_pheromone(clustering_ph_[cluster.id()][client.id()]);
  for (const Client* client_ptr: cluster.clients()) {
    total_pheromone += clustering_ph_[client_ptr->id()][client.id()];
  }
  return total_pheromone / static_cast<double>(cluster.clients().size() + 1);
}


/**
 * Return the pheromone of the given cluster.
 *
 * This is required to determine whether a cluster should be kept as is instead
 * of being extended with one or more new clients.
 *
 * TODO: remove this function and all calls to it;
 */
double ACOSolver::pheromone(const Cluster& cluster) const
{
  return 1.0;
//   double total_pheromone(0.0);
//   Index links(0);
//   const std::vector <const Client* >& clients = cluster.clients();
//   for (Index i = 0; i < clients.size(); ++i) {
//     total_pheromone += clustering_ph_[cluster.id()][clients[i]->id()];
//     links++;
//     for (Index j = i + 1; j < clients.size(); ++j) {
//       total_pheromone += clustering_ph_[clients[i]->id()][clients[j]->id()];
//       links++;
//     }
//   }
//   return total_pheromone / static_cast<double>(links);
}


/**
 * Return the trail of inserting cluster as seed (i.e. between two depots).
 */
double ACOSolver::trail(const Index odepot_id, const Cluster& cluster,
                        const Index cdepot_id) const
{

  return pheromone(odepot_id, cluster) + pheromone(cluster, cdepot_id);
}


/**
 * Return the trail of inserting cluster after the opening depot.
 */
double ACOSolver::trail(const Index odepot_id, const Cluster& cluster,
                        const Cluster& successor) const
{
  return (pheromone(odepot_id, cluster) + pheromone(cluster, successor)) /
    (2 * pheromone(odepot_id, successor));
}


/**
 * Return the trail of inserting cluster before the closing depot.
 */
double ACOSolver::trail(const Cluster& predecessor, const Cluster& cluster,
                        const Index cdepot_id) const
{
  return (pheromone(predecessor, cluster) + pheromone(cluster, cdepot_id)) /
    (2 * pheromone(predecessor, cdepot_id));
}


/**
 * Return the trail of inserting cluster between two other clusters.
 */
double ACOSolver::trail(const Cluster& predecessor, const Cluster& cluster,
                        const Cluster& successor) const
{
  return (pheromone(predecessor, cluster) + pheromone(cluster, successor)) /
    (2 * pheromone(predecessor, successor));
}


/**
 * Update the clustering pheromone matrix using the best known solution.
 *
 * The persistence is determined by the constant factor \rho and the
 * reinforcement is (1 - persistence).
 *
 * Updating the clustering pheromone means that
 * a) each position in the matrix has to be multiplied by rho while ensuring
 * that rho never gets too small (to avoid 0 divisions)
 * b) for each cluster in the best solution, the following procedure has to be
 * executed:
 * New pheromone has to be added between the parking and all clients (in both
 * directions so the matrix stays properly symmetric) as well as between each
 * pair of clients.
 */
void ACOSolver::update_clustering_pheromone()
{
  // evaporation
  for (Matrix<double>::row_size_t i = 0; i < clustering_ph_.num_rows(); ++i) {
    for (Matrix<double>::col_size_t j = 0; j < clustering_ph_.num_cols(); ++j) {
      clustering_ph_[i][j] = std::max(clustering_ph_[i][j] * pb_.cfg().rho(),
                                      pb_.cfg().min_pheromone());
    }
  }
  // adding new pheromone
  for (const Cluster& cluster: best_sol_.clusters()) {
    for (const Client* client_ptr: cluster.clients()) {
      // update link between parking and client
      clustering_ph_[cluster.id()][client_ptr->id()] += new_pheromone_;
      // matrix is symmetric
      clustering_ph_[client_ptr->id()][cluster.id()] += new_pheromone_;
      for (const Client* other_ptr: cluster.clients()) {
        if (client_ptr == other_ptr) continue;
        clustering_ph_[client_ptr->id()][other_ptr->id()] += new_pheromone_;
        clustering_ph_[other_ptr->id()][client_ptr->id()] += new_pheromone_;
      }
    }
  }
}


/**
 * Update the routing pheromone matrices using the best known solution.
 *
 * The routing pheromone consists of three matrices to be updated:
 * one for adjacency of clusters to the opening depot, one for adjacency to
 * the closing depot and a third for adjacency between clusters.
 *
 * For the depot adjacency, each route has a row in the adjacency matrix.
 * Indexing is done by the index of the route in the solution's vector of
 * routes.
 */
void ACOSolver::update_routing_pheromone()
{
  // evaporation (opening, closing in first loop, regular in second)
  for (Matrix<double>::row_size_t i = 0; i < opening_ph_.num_rows(); ++i) {
    for (Matrix<double>::col_size_t j = 0; j < opening_ph_.num_cols(); ++j) {
      opening_ph_[i][j] *= pb_.cfg().rho();
      closing_ph_[i][j] *= pb_.cfg().rho();
    }
  }
  for (Matrix<double>::row_size_t i = 0; i < routing_ph_.num_rows(); ++i) {
    for (Matrix<double>::col_size_t j = 0; j < routing_ph_.num_cols(); ++j) {
      routing_ph_[i][j] *= pb_.cfg().rho();
    }
  }
  // adding new pheromone
  for (Index i = 0; i < best_sol_.trucks(); ++i) {
    update_opening_depot_pheromone(i);
    const Route& route(best_sol_.routes()[i]);
    auto stop_iterator = route.stops().begin();
    ++stop_iterator;
    for (; stop_iterator != route.stops().end(); ++stop_iterator) {
      auto prior_iterator = prior(stop_iterator);
      // parking -> parking
      routing_ph_[(*prior_iterator)->id()][(*stop_iterator)->id()] += new_pheromone_;
      // parking -> clients
      for (const Client* client: (*stop_iterator)->clients()) {
        routing_ph_[(*prior_iterator)->id()][client->id()] += new_pheromone_;
      }
      for (const Client* prior_client: (*prior_iterator)->clients()) {
        // clients -> parking
        routing_ph_[prior_client->id()][(*stop_iterator)->id()] += new_pheromone_;
        // clients -> clients
        for (const Client* client: (*stop_iterator)->clients()) {
          routing_ph_[prior_client->id()][client->id()] += new_pheromone_;
        }
      }
    }
    update_closing_depot_pheromone(i);
  }
}


/**
 * Update a single route's closing depot pheromone.
 *
 * Simple helper function.
 */
void ACOSolver::update_closing_depot_pheromone(Index virtual_depot)
{
  const Route& r(best_sol_.routes()[virtual_depot]);
  const Cluster& last_cluster(*(r.stops().back()));
  closing_ph_[virtual_depot][last_cluster.id()] += new_pheromone_;  // parking
  for (const Client* client_ptr: last_cluster.clients()) {
    closing_ph_[virtual_depot][client_ptr->id()] += new_pheromone_;
  }
}


/**
 * Update a single route's opening depot pheromone.
 *
 * Simple helper function.
 */
void ACOSolver::update_opening_depot_pheromone(Index virtual_depot)
{
  const Route& r(best_sol_.routes()[virtual_depot]);
  const Cluster& first_cluster(*(r.stops().front()));
  opening_ph_[virtual_depot][first_cluster.id()] += new_pheromone_;  // parking
  for (const Client* client_ptr: first_cluster.clients()) {
    opening_ph_[virtual_depot][client_ptr->id()] += new_pheromone_;
  }
}
}


void VRPTWMS::ACOSolver::update_pheromone()
{
  update_clustering_pheromone();
  update_routing_pheromone();
  if (gui_callback_) {
    gui_callback_(clustering_ph_, routing_ph_, opening_ph_, closing_ph_);
  }
}
