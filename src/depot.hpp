/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DEPOT_H
#define DEPOT_H

#include <cfloat>

#include "common.hpp"
#include "parking.hpp"


namespace VRPTWMS {
class Depot : public Parking {
public:
  explicit Depot(Index id, Distance x, Distance y, Time opening_time,
                 Time closing_time)
      : Parking(id, x, y),
        opening_time_(opening_time),
        closing_time_(closing_time) {}
  Depot(const Depot& other)
      : Parking(other),
        opening_time_(other.est()),
        closing_time_(other.lst()) {}
  Time est() const {return opening_time_;}
  Time lst() const {return closing_time_;}
private:
  Time opening_time_;
  Time closing_time_;
};

std::ostream& operator<< (std::ostream& os, const Depot& depot);
}

#endif //DEPOT_H
