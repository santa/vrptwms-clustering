/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SOLVER_H
#define SOLVER_H

#include <boost/timer/timer.hpp>

class Client;
class Cluster;

using boost::timer::nanosecond_type;

/**
 * Abstract base class for all solvers.
 */


namespace VRPTWMS {


class Solver {
public:
  virtual nanosecond_type cpu_time() const = 0;
  virtual void solve () = 0;
  virtual ~Solver() {}  // virtual needed in presence of 1+ virtual functions
};

// common functions that may be used by all solvers
bool is_unassigned(std::list<Cluster>& unassigned, const Client& client);
void remove_unassigned(std::list<Cluster>& unassigned, const Client& client);
}

#endif // SOLVER_H
