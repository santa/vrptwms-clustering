/**
 * A tiny header-only implementation of a basic matrix for storing numbers.
 * Idea from Data Structures and Algorithm Analysis in C++ (3/e) by Mark Allen
 * Weiss (pg. 38).
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
using std::vector;

template <typename Object>
class Matrix {
public:
  typedef typename vector< vector<Object> >::size_type row_size_t;
  typedef typename vector<Object>::size_type col_size_t;
  explicit Matrix(): rows_(0) {}
  explicit Matrix(int rows, int cols): rows_(rows) {
    for (row_size_t i = 0; i < num_rows(); ++i)
      rows_[i].resize(cols);
  }
  explicit Matrix(int rows, int cols, Object default_value): Matrix(rows, cols) {
    for (row_size_t i = 0; i < num_rows(); ++i) {
      for (col_size_t j = 0; j < num_cols(); ++j) {
        rows_[i][j] = default_value;
      }
    }
  }
  explicit Matrix(const Matrix& other): rows_(other.num_rows()) {
    for (row_size_t i = 0; i < num_rows(); ++i) {
      rows_[i] = other.rows_[i];  // use vector's copy assignment
    }
  }
  Matrix& operator=(const Matrix& other) {
    rows_.resize(other.num_rows());
    for (row_size_t i = 0; i < num_rows(); ++i) {
      rows_[i] = other.rows_[i];  // use vector's copy assignment
    }
    return *this;
  }
  ~Matrix() {}

  const vector<Object>& operator[](row_size_t row) const {
    return rows_[row];
  }
  vector<Object>& operator[](row_size_t row) {
    return rows_[row];
  }

  row_size_t num_rows() const {return rows_.size();}
  col_size_t num_cols() const {return num_rows() ? rows_[0].size() : 0;}
  void resize(int rows, int cols) {
    rows_.resize(rows);
    for (auto it = rows_.begin(); it != rows_.end(); ++it)
      it->resize(cols);
  }
private:
  vector<vector<Object>> rows_;
};

#endif //  MATRIX_H
