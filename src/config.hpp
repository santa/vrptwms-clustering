/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <ctime>
#include <string>

#include <boost/timer/timer.hpp>

#include "common.hpp"

using boost::timer::nanosecond_type;


namespace VRPTWMS {
enum class ClusteringHeuristic : Index {
  COMBINED,
  PARALLEL,
  SEQUENTIAL,
  LAST,
  FIRST = COMBINED,
  DEFAULT = SEQUENTIAL
};

enum class VerbosityLevel : Index {
  MIN,
  BASIC,
  DETAILED,
  DEBUGGING,
  DEFAULT = MIN,
  MAX = DEBUGGING,
};

ClusteringHeuristic operator++(ClusteringHeuristic& h);
class Problem;


class Config {
public:
  explicit Config(std::string filename);
  /**
   * Default constructor uses hard-coded default values.
   */
  explicit Config()
    : ants_(0),
      attractiveness_penalty_(0.3),
      clustering_heuristic_(ClusteringHeuristic::DEFAULT),
      cost_truck_(1.0),
      cost_worker_(0.1),
      cost_distance_(0.0001),
      debug_(false),
      default_walking_speed_(0.2),
      deterministic_(false),
      digits_(Constants::Digits),
      dynamic_ants_(false),
      initial_pheromone_(1.0),
      max_attr_(0.5),
      max_runtime_(10),
      max_workers_(3),
      min_delta_(0.01),
      min_pheromone_(0.0001),
      outfile_("details.txt"),
      pheromone_gui_(false),
      random_seed_(time((time_t*) NULL)),
      rho_(0.9),
      verbosity_(VerbosityLevel::DEFAULT),
      weighted_(false),  // non-weighted attractiveness performs much better!
      write_back_(false) { if (!ants_) dynamic_ants_ = true;}
  /**
   * The number of ants (for ACO).
   */
  Index ants() const {return ants_;}
  void ants(Index number, bool disable_dynamic=false);
  /**
   * Constant factor decreasing the attractiveness of an insertion.
   *
   * The attractiveness is decreased if it requires additional workers.
   */
  double attractiveness_penalty() const {return attractiveness_penalty_;}
  /**
   * Set the attractiveness penalty to given value.
   */
  void attractiveness_penalty(double p) {attractiveness_penalty_ = p;}
  /**
   * Return which clustering heuristic should be used.
   */
  ClusteringHeuristic clustering_heuristic() const {return clustering_heuristic_;}
  /**
   * Select which clustering heuristic to use.
   *
   * Possible choices are listed in the enum class ClusteringHeuristic.
   */
  void clustering_heuristic(ClusteringHeuristic h) {clustering_heuristic_ = h;}
  void clustering_heuristic(std::string name);
  /**
   * Cost per truck used in a solution to the problem.
   */
  Cost cost_truck() const {return cost_truck_;}
  /**
   * Cost per worker used in a solution to the problem.
   */
  Cost cost_worker() const {return cost_worker_;}
  /**
   * Cost per distance unit used in a solution to the problem.
   */
  Cost cost_distance() const {return cost_distance_;}
  /**
   * Number of digits for rounding double precision values.
   */
  Index digits() const {return digits_;}
  /**
   * Return true if debug is enabled.
   */
  bool debug() const {return debug_;}
  /**
   * Set the debug flag.
   */
  void debug(bool enable) {debug_ = enable;}
  /**
   * Return the default walking speed.
   *
   * The walking speed is formulated as a fraction of the driving speed (i.e.
   * 0.2 means that a worker walks on average at 20% of the speed of a truck.
   * This speed is only used if it is missing in the input data (only true
   * in Solomon instances). The time required for walking a given distance
   * is that distance divided by the walking speed.
   */
  Speed default_walking_speed() const {return default_walking_speed_;}
  /**
   * Set the default walking speed for instances that lack this information.
   *
   * The only instances not including this information are the classic Solomon
   * instances.
   */
  void default_walking_speed(Speed speed) {default_walking_speed_ = speed;}
  /**
   * Return true if a deterministic heuristic should be used instead of a
   * metaheuristic.
   */
  bool deterministic() const {return deterministic_;}
  /**
   * Decide whether a deterministic heuristic should be used instead of a
   * metaheuristic.
   */
  void deterministic(bool value) {deterministic_ = value;}
  /**
   * The default value for initializing the pheromone matrices.
   */
  /**
   * Query whether dynamic number of ants is enabled.
   * Dynamic means that the number of ants depends on the number
   * of parkings in the current problem.
   * Dynamic ants are enabled by setting the number of ants to 0.
   */
  bool dynamic_ants() const {return dynamic_ants_;}
  double initial_pheromone() const {return initial_pheromone_;}
  /**
   * The maximum allowed attractiveness for adding a client to a cluster.
   */
  Attractiveness max_attr() const {return max_attr_;}
  /**
   * Maximum allowed runtime (CPU time) per instance (in nanoseconds).
   */
  Seconds max_runtime() const {return max_runtime_;}
  /**
   * Maximum allowed runtime (CPU time) per instance (in nanoseconds).
   */
  void max_runtime(Seconds value) {max_runtime_ = value;}
  /**
   * The maximum number of workers per vehicle.
   */
  Index max_workers() const {return max_workers_;}
  /**
   * The minimum difference between two double precision values.
   */
  double min_delta() const {return min_delta_;}
  /**
   * A minimum amount of pheromone ensures that no 0 divisions occur in any
   * trail calculation.
   */
  double min_pheromone() const {return min_pheromone_;}

  /**
   * Return the name of the file to contain detailed output.
   */
  std::string outfile() const {return outfile_;}
  /**
   * Return true if a small progress gui for the pheromone should be shown.
   */
  bool pheromone_gui() const {return pheromone_gui_;}
  /**
   * Decide whether a small progress gui for the pheromone should be shown.
   */
  void pheromone_gui(bool value) {pheromone_gui_ = value;}
  /**
   * Return the configured random seed. Defaults to current time.
   */
  long int random_seed() const {return random_seed_;}
  /**
   * Set the random seed to given value (for debugging).
   */
  void random_seed(long int value) {random_seed_ = value;}
  /**
   * Pheromone persistence.
   */
  double rho() const {return rho_;}
  /**
   * Set the pheromone persistence.
   * TODO: ensure rho <= 1.0.
   */
  void rho(double value) {rho_ = value;}
  void update(const Problem& pb);
  /**
   * Return the verbosity level.
   */
  VerbosityLevel verbosity() const {return verbosity_;}
  /**
   * Set the verbosity flag.
   */
  void verbosity(VerbosityLevel state) {verbosity_ = state;}
  /**
   * Return whether the attractiveness calculation should weight the number of
   * workers.
   *
   * Note: empirical results show that it is much better NOT to weight the
   * number of workers when adding to existing clusters.
   */
  bool weighted() const {return weighted_;}
  /**
   * True if new global best results should be written to the input file.
   */
  bool write_back() const {return write_back_;}

  static std::string short_name(ClusteringHeuristic h);

private:
  Index ants_;
  double attractiveness_penalty_;
  ClusteringHeuristic clustering_heuristic_;
  double cost_truck_;
  double cost_worker_;
  double cost_distance_;
  bool debug_;
  Speed default_walking_speed_;
  bool deterministic_;  /** if true, use deterministic heuristic instead of metaheuristic */
  Index digits_;  /** Number of digits for rounding double precision values */
  bool dynamic_ants_;  /** Number of ants depends on number of parkings */
  /** the maximum allowed attractiveness for adding a client to a cluster */
  double initial_pheromone_;
  Attractiveness max_attr_;
  Seconds max_runtime_;
  Index max_workers_;
  double min_delta_;
  double min_pheromone_;
  std::string outfile_;
  bool pheromone_gui_;
  long int random_seed_;
  double rho_;  /** Pheromone persistence */
  VerbosityLevel verbosity_;
  bool weighted_;
  bool write_back_;
};

std::ostream& operator<< (std::ostream& os, const Config& cfg);
}

#endif //CONFIG_H
