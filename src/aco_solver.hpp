/*
 * Copyright (C) 2014  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ACOSOLVER_H
#define ACOSOLVER_H

#include <functional>

#include <boost/timer/timer.hpp>

#include "insertion.hpp"
#include "matrix.hpp"
#include "problem.hpp"
#include "solution.hpp"
#include "solver.hpp"

using boost::timer::cpu_timer;
using boost::timer::nanosecond_type;


namespace VRPTWMS {

class Cluster;


class ACOSolver : public Solver {
public:
  explicit ACOSolver(Solution& sol,
                     std::function<void (const Matrix<double>& clustering_ph,
                                         const Matrix<double>& routing_ph,
                                         const Matrix<double>& opening_ph,
                                         const Matrix<double>& closing_ph)>
                                         gui_callback=nullptr);
  nanosecond_type cpu_time() const;
  void solve();
  ~ACOSolver() {}

private:
  const Problem& pb_;
  Solution& best_sol_;
  Solution sol_;
  Index solution_counter;  /** The number of solutions calculated */
  /** gui_callback_ allows updating a tiny gui showing the pheromone progress */
  std::function<void (const Matrix<double>& clustering_ph,
                      const Matrix<double>& routing_ph,
                      const Matrix<double>& opening_ph,
                      const Matrix<double>& closing_ph)> gui_callback_;

  cpu_timer timer_;

  bool proceed();
  void report_progress();
  void solve_aco();
  void update_pheromone();


  // clustering
  /**
   * Assignment of 1 customer per parking; they have the purpose of checking
   * if the current instance is feasible and to provide a resource for selecting
   * the next cluster to be started.
   * The trivial clusters are sorted ascending by their attractiveness.
   */
  std::vector<Cluster> trivial_clusters_;
  /**
   * A list of unassigned clients, realized as members of their best trivial
   * assignments. Used by the clustering heuristic.
   */
  std::list<Cluster> unassigned_;
  /** The amount of pheromone to be added along the paths of the best solution. */
  double new_pheromone_;
  /** Clustering pheromone lxl matrix where l is the number of locations
   * seed: was currently investigated client associated w/ current parking in BKS?
   * a simple 1 value matrix lookup is enough (only two times single location)
   *
   * all other insertions: k + 1 lookups where k is the number of clients in the
   * cluster. The + 1 stems from the parking location.
   */
  Matrix<double> clustering_ph_;  //

  double pheromone(Index parking_id, const Client& client) const;
  double pheromone(const Cluster& cluster, const Client& client) const;
  double pheromone(const Cluster& cluster) const;
  void solve_clustering(bool weighted=false);
  void solve_sequential_heuristic(bool weighted=false);
  /** A clustering trail is simply the calculated cluster pheromone. */
  double trail(Index parking_id, const Client& client) const {return pheromone(parking_id, client);}
  double trail(const Cluster& cluster, const Client& client) const {return pheromone(cluster, client);}
  double trail(const Cluster& cluster) const {return pheromone(cluster);}
  void update_clustering_pheromone();


  // routing
  /**
   * A list of unassigned clusters that still need to be routed.
   * Used by the routing heuristic.
   */
  std::list<Cluster*> unrouted_;

  /**
   * The routing pheromone matrix has dimension #locations x #locations
   * (although the depot is ignored b/c it requires separate treatment it is
   * needed as it may serve as the parking for a cluster in the middle of a
   * route)
   */
  Matrix<double> routing_ph_;  // pure routing pheromone matrix
  /**
   * Two virtual depot matrices are required: one for preceding the depot and
   * one for succeeding it. Each has the dimension #routes x #locations (as the
   * number of routes is not known, we use #customers x #locations instead)
   * (#customers is the maximum number of routes, hence we have one line per
   * potential route / virtual depot; #locations is the number of parkings
   * including the depot + the number of customers b/c it is possible that the
   * first / last cluster to be served on a route has its parking at the depot)
   */
  Matrix<double> opening_ph_;  // pheromone matrix for succeeding virtual depots
  Matrix<double> closing_ph_;  // pheromone matrix for preceding virtual depots

  // routing
  double pheromone(const Index vdepot_id, const Cluster& cluster) const;
  double pheromone(const Cluster& cluster, const Index vdepot_id) const;
  double pheromone(const Cluster& cluster1, const Cluster& cluster2) const;
  double trail(const Index odepot_id, const Cluster& cluster,
               const Index cdepot_id) const;
public:  // to be used from within Route's update_insertion
  double trail(const Index odepot_id, const Cluster& cluster,
               const Cluster& successor) const;
  double trail(const Cluster& predecessor, const Cluster& cluster,
               const Index cdepot_id) const;
  double trail(const Cluster& predecessor, const Cluster& cluster,
               const Cluster& successor) const;
private:
  Insertion pick_insertion(Route& r);
  Cluster* pick_routing_seed();
  void solve_routing();
  void solve_solomon();
  void update_routing_pheromone();
  void update_opening_depot_pheromone(Index virtual_depot);
  void update_closing_depot_pheromone(Index virtual_depot);

  friend std::ostream& operator<< (std::ostream& os, const ACOSolver& s);
};

std::ostream& operator<< (std::ostream& os, const ACOSolver& s);
}
#endif  // ACOSOLVER_H
