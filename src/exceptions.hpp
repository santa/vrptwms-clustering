 /*
  * This file is part of a program allowing to solve VRPTWMS instances.
  * It defines exceptions used throughout the program.
  *
  * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <stdexcept>
#include <string>


/**
 * This exception is thrown if a lookup-method for parkings demands a
 * non-existing parking (eg. an id which isn't a parking id).
 */
class NonexistingParking : public std::runtime_error {
  public:
    explicit NonexistingParking(const std::string& message)
        : std::runtime_error(message) { };
};


/**
 * This exception is thrown if a lookup-method for clients demands a
 * non-existing client (eg. an id which isn't a client id).
 */
class NonexistingClient : public std::runtime_error {
  public:
    explicit NonexistingClient(const std::string& message)
        : std::runtime_error(message) { };
};


/**
 * This exception is thrown if during the creation of a cluster or route
 * assignment an infeasibility is detected that can only be fixed by
 * propagating the problem to the caller.
 *
 * This exception is not to be used in places an infeasibility should never
 * happen. In those cases, an assertion should be used.
 */
class InfeasibilityException : public std::exception {
  public:
    explicit InfeasibilityException(const std::string& message)
      : std::exception(), m_message(message) { };
    virtual const char* what() const throw() { return m_message.c_str(); }
private:
    std::string m_message;
};


#endif // EXCEPTIONS_H
