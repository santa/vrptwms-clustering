/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INSERTION_H
#define INSERTION_H

#include <limits>
#include <list>


namespace VRPTWMS {
class Cluster;
class Route;


struct Insertion {
  explicit Insertion()
      : target(nullptr), c(nullptr), pos(),
        cost(std::numeric_limits<double>::infinity()),
        attr(-std::numeric_limits<double>::infinity()), is_feasible(false) {}
  explicit Insertion(Route* r, Cluster* cl, std::list<Cluster*>::iterator it,
                     double cost_, double attractiveness, bool is_feasible_)
      : target(r),
        c(cl),
        pos(it),
        cost(cost_),
        attr(attractiveness),
        is_feasible(is_feasible_) {}
  Route* target;
  Cluster* c;  //!< Cluster to be inserted.
  std::list<Cluster*>::iterator pos;  //!< Add cluster at this position.
  double cost;
  double attr;
  bool is_feasible;
  /** Iterator to list of unrouted allows to remove candidate if insertion
   is selected*/
  std::list<Cluster*>::iterator unrouted_it;
};
}

#endif  // INSERTION_H
