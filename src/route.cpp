/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <assert.h>
#include <iterator>
#include <list>
#include <vector>

#include <boost/utility.hpp>  // boost::prior

#include "aco_solver.hpp"
#include "common.hpp"
#include "cluster.hpp"
#include "insertion.hpp"
#include "problem.hpp"

#include "route.hpp"

using boost::prior;


namespace VRPTWMS {
/**
 * To be used by solvers to consistently identify routes and their depots.
 * This value is used to initialize each route's id before its incremented.
 * It is public as solvers might have to reset it to a common base for
 * multiple iterations generating new solutions with new routes.
 */
Index Route::next_id_ = 0;


/**
 * Constructor.
 */
Route::Route(const Problem& pb, Cluster& seed)
    : pb_(pb), id_(next_id_), demand_(seed.demand()), workers_(pb.max_workers())
{
  // TODO: properly assign id for ACO (current approach is useless, as the
  // route id will be different for each ant; current workaround: using the
  // route's position in the vector of routes in any given solution)
  next_id_++;
  stops_.push_back(&seed);
  update_ests(stops_.begin());
  update_lsts(stops_.rbegin());
}


/**
 * Copy constructor.
 */
Route::Route(const Route& other)
    : pb_(other.pb_),
      id_(other.id_),
      stops_(other.stops_),
      demand_(other.demand_),
      workers_(other.workers_)
{
}


/**
 * Copy assignment.
 *
 * The problem reference is not updated as it is the same for all objects.
 * Maybe copying the id might be adjusted in the future.
 */
// Route& Route::operator=(const Route& other)
// {
//   id_ = other.id_;
//   stops_ = other.stops_;
//   demand_ = other.demand_;
//   workers_ = other.workers_;
// }


/**
 * Return True if the suggested insertion is feasible.
 *
 * An insertion is feasible if there is no collision in the earliest and latest
 * start times. The approach used in this function is faster than the more
 * straight-forward approach of using max(est, pred->aest + c_m[...]).
 * By default, the capacity restriction is not checked by this function.
 */
bool Route::can_insert(std::list< Cluster* >::const_iterator pos,
                       const Cluster& c, bool check_capacity) const
{
  Time earliest_arrival(0), latest_arrival(0);
  if (check_capacity and (demand() + c.demand() > pb_.capacity())) return false;
  if (pos == stops_.begin()) {
    earliest_arrival = pb_.depot().est() + pb_.d(pb_.depot().id(), c.id());
  } else {
    earliest_arrival = (*prior(pos))->aest() + (*prior(pos))->st(workers()) +
      pb_.d((*prior(pos))->id(), c.id());
  }
  if (pos == stops_.end()) {
    latest_arrival = pb_.depot().lst() - pb_.d(c.id(), pb_.depot().id()) - c.st(workers());
  } else {
    latest_arrival = (*pos)->alst() - pb_.d(c.id(), (*pos)->id()) - c.st(workers());
  }
  return (earliest_arrival <= c.lst(workers_)) and
    (latest_arrival >= c.est(workers()) and (earliest_arrival <= latest_arrival));
}


/**
 * Return vector containing the ids of all clusters on this route.
 *
 * Only required for checking if solutions are feasible (each cluster must be
 * on exactly one route).
 */
vector<Index> Route::cluster_ids() const
{
  vector<Index> ids;
  for (Cluster* c: stops_) {
    ids.push_back(c->cid());
  }
  return ids;
}


// THIS MIGHT NEVER BE NEEDED
// void Route::insert(std::list< const Cluster* const>::iterator before,
//                    std::list< const Cluster* const>::iterator clusters) {
//
// }

/**
 * Insert given cluster in this route.
 *
 * All route data including all relevant cluster start times are updated.
 */
void Route::insert(std::list< Cluster* >::iterator pos, Cluster& cluster)
{
  stops_.insert(pos, &cluster);
    pos--;  // also update the added cluster (it points after it)
  update_ests(pos);
  std::list<Cluster*>::reverse_iterator rit(boost::next(pos));
  update_lsts(rit);
  demand_ += cluster.demand();
}


/**
 * Return true if the route is feasible, otherwise false.
 *
 * The purpose of this function is to ensure that no infeasible
 * routes are part of a final solution.
 * A route is feasible if it has enough workers for each of the clusters and
 * the total demand does not exceed the truck capacity and all time windows
 * are feasible.
 */
bool Route::is_feasible() const
{
  for (auto it = stops_.begin(); it != stops_.end(); ++it) {
    if ((*it)->min_workers() > workers_) {
      if (pb_.cfg().debug()) {
        std::cerr << "ERROR: " << *this << "\n";
        std::cerr << "has not enough workers to serve\n" << *(*it) << "\n";
      }
      return false;
    }
  }
  if (demand_ > pb_.capacity()) {
    if (pb_.cfg().debug()) {
      std::cerr << "ERROR: " << *this << "\n";
      std::cerr << "total demand exceeds truck capacity (" << pb_.capacity() << ")\n";
    }
    return false;
  }
  std::vector<Time> aests_ignored;
  return is_feasible_with(workers_, aests_ignored);
}


/**
 * Return True if the route is feasible in terms of all time windows.
 *
 * The check is performed for the given numbers of workers in order
 * to see if that number can be used instead of the current number.
 * \param aests Reference to a vector of actual earliest start times. This
 * vector is filled with the values calculated by this function and avoids
 * re-calculation by the caller.
 */
bool Route::is_feasible_with(Index workers, std::vector< Time >& aests) const
{
  assert(workers > 0);  // 0 workers makes no sense
  aests.clear();
  Time t(pb_.depot().est());
  auto it = stops_.begin();
  t = std::max(t + pb_.d(pb_.depot().id(), (*it)->id()), (*it)->est(workers));
  aests.push_back(t);
  if ((*it)->min_workers() > workers) {
    return false;
  }
  if (t > (*it)->lst(workers)) {
    return false;
  }  //
  ++it;
  for (; it != stops_.end(); ++it) {
    t += (*prior(it))->st(workers) + pb_.d((*prior(it))->id(), (*it)->id());
    t = std::max(t, (*it)->est(workers));
    aests.push_back(t);
    if ((*it)->min_workers() > workers) {
      return false;
    }
    if (t > (*it)->lst(workers)) {
      return false;
    }
  }
  t += (*prior(it))->st(workers) + pb_.d((*prior(it))->id(), pb_.depot().id());
  if (t > pb_.depot().lst()) {
    return false;
  }
  return true;
}


/**
 * Return the total distance driven serve this route.
 *
 * The total distance includes the distance between the opening depot, all stops
 * on the route and from the last stop to the closing depot.
 */
Distance Route::length() const
{
  assert(stops_.size());  // a route must never be empty
  auto it = stops_.cbegin();
  Distance total = pb_.d(pb_.depot().id(), (*it)->id());
  it++;
  while (it != stops_.cend()) {
    total += pb_.d((*prior(it))->id(), (*it)->id());
    it++;
  }
  return total + pb_.d((*prior(it))->id(), pb_.depot().id());
}


/**
 * Remove unnecessary service workers from the given route.
 * \return true if workers were removed, otherwise false
 */
bool Route::reduce_service_workers()
{
  bool reduced(false);
  Index workers = workers_ - 1;
  std::vector<Time> aest_cache;
  while (workers >= 1 && is_feasible_with(workers, aest_cache)) {
    assert(aest_cache.size() == stops_.size());
    workers_ = workers;
    auto it = stops_.begin();
    for (auto aest: aest_cache) {
      (*it)->aest_ = aest;
      ++it;
    }
    workers--;
    reduced = true;
  }
  if (reduced) {
    update_lsts(stops_.rbegin());
  }
  return reduced;
}


/**
 * Calculate and set the actual earliest start times.
 * \param c first cluster that needs to be updated (update towards tail)
 *
 * Update the cluster given by the iterator and all clusters after it on this
 * route. Actual earliest start times are either the earliest start of the
 * cluster for the given number of workers, or the earliest time the cluster
 * can be reached - whichever happens later.
 */
void Route::update_ests(std::list<Cluster*>::iterator c)
{
  if (c == stops_.begin()) {  // if c is the first stop on this route
    (*c)->aest_ = std::max((*c)->est(workers_), pb_.depot().est() +
                           pb_.d(pb_.depot().id(), (*c)->id()));
    c++;
  }
  while (c != stops_.end()) {  // aest is not relevant for the closing depot
                               //  as it is not used by insert*feasible
    (*c)->aest_ = std::max((*c)->est(workers_),
                           (*prior(c))->aest_ + (*prior(c))->st(workers_) +
                           pb_.d((*prior(c))->id(), (*c)->id()));
    c++;
  }
}


/**
 * Calculate and set the latest start times.
 * \param c last cluster that needs to be updated (update towards head)
 */
void Route::update_lsts(std::list<Cluster*>::reverse_iterator c)
{
  if (c == stops_.rbegin()) {
    (*c)->alst_ = std::min((*c)->lst(workers_),
                           pb_.depot().lst() - (*c)->st(workers_) -
                           pb_.d((*c)->id(), pb_.depot().id()));
    c++;
  }
  while (c != stops_.rend()) {  // alst not relevant for the opening depot
    (*c)->alst_ = std::min((*c)->lst(workers_),
                           (*prior(c))->alst_ - (*c)->st(workers_) -
                           pb_.d((*c)->id(), (*prior(c))->id()));
    c++;
  }
}


/**
 * Update the given insertion to the best position of c on this route.
 *
 * The insertion is only updated if there is a position for c that is better
 * than the already defined insertion.
 *
 * \return True if the insertion was updated.
 */
bool Route::update_insertion(Cluster& c, Insertion& ins, const ACOSolver* solver)
{
  bool updated(false);
  Distance cost_dist(0.0);
  // using constants instead of config (for now) => cost_time is ignored for now
  // this simplifies the implementation and slightly increases the performance
  // consider cvrptwms->calc_best_insertion for a full implementation
  //   const double alpha = 1.0, alpha2 = 1.0 - alpha;
  const double mu = 1.0, lambda = 2.0;

  if (demand() + c.demand() > pb_.capacity()) {
    return false;
  }
  for (auto it = stops_.begin(); it != stops_.end(); ++it) {
    if (not can_insert(it, c)) {
      continue;
    }
    if (it == stops_.begin()) {
      cost_dist = pb_.d(c.id()) + pb_.d(c.id(), (*it)->id()) -
        mu * pb_.d((*it)->id());
    } else {
      cost_dist = pb_.d((*prior(it))->id(), c.id()) + pb_.d(c.id(), (*it)->id()) -
        mu * pb_.d((*prior(it))->id(), (*it)->id());
    }
    Attractiveness attr(lambda * pb_.d(c.id()) - cost_dist);
    attr = (attr > 0) ? attr : Constants::MinAttractiveness;
    if (solver) {  // only use trail when calculating routes for ACO solver
      if (it == stops_.begin()) {  // insertion at beginning
        attr *= solver->trail(id(), c, **it);
      } else {  // insertion between two clusters
        attr *= solver->trail(**prior(it), c, **it);
      }
    }
    Distance cost(cost_dist - lambda * pb_.d(c.id()));
    if (attr > ins.attr) {
      ins.target = this;
      ins.c = &c;
      ins.cost = cost;
      ins.attr = attr;
      ins.pos = it;
      ins.is_feasible = true;
      updated = true;
    }
  }
  auto it = stops_.end();  // try insertion at end
  if (can_insert(it, c)) {
    cost_dist = pb_.d((*prior(it))->id(), c.id()) + pb_.d(c.id()) -
      mu * pb_.d((*prior(it))->id());
    Attractiveness attr(lambda * pb_.d(c.id()) - cost_dist);
    attr = (attr > 0) ? attr : Constants::MinAttractiveness;
    if (solver) {  // only use trail when calculating routes for ACO solver
      attr *= solver->trail(**prior(it), c, id());
    }
    Distance cost(cost_dist - lambda * pb_.d(c.id()));
    if (attr > ins.attr) {
      ins.target = this;
      ins.c = &c;
      ins.cost = cost;
      ins.attr = attr;
      ins.pos = it;
      ins.is_feasible = true;
      updated = true;
    }
  }
  return updated;
}


/**
 * Textual representation of route.
 *
 * If cluster ids are present (ie non-zero; this is the case for pure routing
 * problems) the sequence of these is printed. Otherwise, the internal cluster
 * details is printed instead of the cluster ids.
 *
 * In the first case, the cluster data is available in the input files and
 * does not need to be output to reproduce/ describe the result. For problems
 * including the clustering, the cluster internals are relevant as well.
 */
std::ostream& operator<<(std::ostream& os, const Route& r)
{
  os << "[" << r.pb_.depot().id() << ", ";
  for (auto it = r.stops_.begin(); it != r.stops_.end(); ++it) {
    os << (*it)->cid();
    os << ", ";
  }
  os << r.pb_.depot().id() << "] workers: " << r.workers();
  os << ", load: " << r.demand() << ", length: " << r.length();
  return os;
}
}
