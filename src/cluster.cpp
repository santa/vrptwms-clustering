/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <algorithm>
#include <iostream>
#include <cmath>
#include <vector>
#include <stdexcept>

#include <boost/property_tree/ptree.hpp>

#include "common.hpp"
#include "client.hpp"
#include "exceptions.hpp"
#include "parking.hpp"
#include "problem.hpp"

#include "cluster.hpp"

using boost::property_tree::ptree;


namespace VRPTWMS {
Index Cluster::next_cid_ = 0;


// Small helper function to avoid duplicating code.
static void add_values(ptree& data, std::vector<Time>& vec) {
  for (auto it = data.begin(); it != data.end(); ++it) {
    vec.push_back(it->second.get_value<Time>());
  }
}


/**
 * Regular constructor creating new cluster with a parking and one client.
 */
Cluster::Cluster(const Problem& pb, const Parking& parking,
                 const Client& client)
    : pb_(pb), parking_(&parking),
      demand_(0),
      min_workers_(1)  // A cluster with a single client only requires 1 worker
{
  earliest_start_ = pb_.depot().est() + pb_.d(id());
  cid_ = next_cid_;
  for (Index i = 0; i < pb_.max_workers(); ++i) {  // once for each # of workers
    ests_.push_back(Constants::NoTime);
    lsts_.push_back(Constants::NoTime);
    sts_.push_back(Constants::NoTime);
  }
  if (!add(client)) {  // cannot feasibly add initial client
    throw InfeasibilityException("Cannot construct infeasible cluster");
  }
  next_cid_++;
}


/**
 * Construct a cluster with the given client and using its closest feasible
 * parking.
 *
 * If more than one parking are equally close, the selection is done
 * arbitrarily.
 */
Cluster::Cluster(const Problem& pb, const Client& client)
    : Cluster(pb, find_closest_feasible_parking(pb, client), client) {}


/**
 * Constructor using json data that was parsed to boost ptree.
 *
 * The clients are not added to this cluster. The reason for this is that
 * the cluster was created by an external program and doesn't need to be
 * changed. Its only purpose is to be used by the routing algorithm.
 *
 * Also, the "cluster id" available in the input data ptree is ignored as
 * there is no dedicated distance matrix for the routing problem. Instead,
 * the cluster's parking id is used to index the same distance matrix that is
 * created for the clustering problem.
 */
Cluster::Cluster(const Problem& pb, const Parking& parking, const ptree& data)
    : pb_(pb), parking_(&parking)
{
  // clients are initialized to avoid code duplication for routing code
  // (combined routing should work the same way as routing only)
  ptree client_ids(data.get_child("client_ids"));
  for (auto it = client_ids.begin(); it != client_ids.end(); ++it) {
    clients_.push_back(&pb_.client(it->second.get_value<Index>()));
  }
  min_workers_ = data.get<Index>("min_workers");
  demand_ = data.get<Amount>("demand");
  cid_ = data.get<Index>("id");
  for (Index i = 1; i < min_workers_; ++i) {  // no time data in ptree
    ests_.push_back(Constants::NoTime);
    lsts_.push_back(Constants::NoTime);
    sts_.push_back(Constants::NoTime);
  }
  ptree ests(data.get_child("ests"));
  ptree lsts(data.get_child("lsts"));
  ptree sts(data.get_child("sts"));
  add_values(ests, ests_);
  add_values(lsts, lsts_);
  add_values(sts, sts_);
}


/**
 * Copy constructor.
 */
Cluster::Cluster(const Cluster& other)
: pb_(other.pb_)
{
  clone(other);
}


/**
 * Move constructor.
 */
Cluster::Cluster(const Cluster&& other)
: pb_(other.pb_)
{
  *this = std::move(other);
}


/**
 * Copy assignment.
 */
Cluster& Cluster::operator=(const Cluster& rhs) {
  if (this != &rhs) {
    clone(rhs);
  }
  return *this;
}


/**
 * Move assignment.
 */
Cluster& Cluster::operator=(const Cluster&& rhs)
{
  if (this != &rhs) {
    clients_ = std::move(rhs.clients_);
    cid_ = std::move(rhs.cid_);
    demand_ = std::move(rhs.demand_);
    earliest_start_ = std::move(rhs.earliest_start_);
    ests_ = std::move(rhs.ests_);
    lsts_ = std::move(rhs.lsts_);
    sts_ = std::move(rhs.sts_);
    aest_ = std::move(rhs.aest_);
    alst_ = std::move(rhs.alst_);
    min_workers_ = std::move(rhs.min_workers_);
    parking_ = std::move(rhs.parking_);
  }
  return *this;
}


/**
 * Add given client to cluster and update all relevant values.
 *
 * The addition is only performed if it results in a feasible cluster.
 *
 * \return true if the addition was successful, otherwise false
 */
bool Cluster::add(const Client& client)
{
  if (not pb_.is_feasible(*parking_, client)) return false;
//   if (not is_compatible(client)) return false;  // causes tests to fail!!
  Cluster clone(*this);
  clone.clients_.push_back(&client);
  clone.demand_ += client.demand();
  if (clone.demand_ > pb_.capacity()) {
    return false;
  } else if (!clone.update_times()) {
    return false;
  }
  *this = clone;
  return true;
}


const std::vector<Index> Cluster::client_ids() const {
  std::vector<Index> ids;
  for (const Client* client: clients_) {
    ids.push_back(client->id());
  }
  return ids;
}


/**
 * Set values of current cluster to the values of cloned cluster.
 *
 * This function is to be used by the copy constructor and does not only
 * allow to construct a copy of a cluster but also to set the current cluster
 * to the values of the clone.
 * The values have to be independent (i.e. changing `this` or `other` after
 * running this function does not affect both.
 *
 * Note: pb_ and parking_ never change.
 */
void Cluster::clone(const Cluster& other)
{
  clients_ = other.clients_;
  cid_ = other.cid_;
  demand_ = other.demand_;
  earliest_start_ = other.earliest_start_;
  ests_ = other.ests_;
  lsts_ = other.lsts_;
  sts_ = other.sts_;
  aest_ = other.aest_;
  alst_ = other.alst_;
  min_workers_ = other.min_workers_;
  parking_ = other.parking_;
}


/**
 * Return the closest parking to the given client that allows constructing a
 * feasible cluster.
 *
 * Note: the closest parking to the client is not feasible in all cases. In rare
 * circumstances a parking that is slightly further away from the client but
 * considerably closer to the depot will be the return value of this function.
 */
const Parking& Cluster::find_closest_feasible_parking(const Problem& pb,
                                                      const Client& client) {
  std::vector<const Parking*> parkings = pb.parking_ptrs();
  std::sort(parkings.begin(), parkings.end(),
            [&](const Parking* a, const Parking* b) {
              return pb.d(client.id(), a->id()) < pb.d(client.id(), b->id());
            });
  for (const Parking* p: parkings) {
    if (pb.is_feasible(*p, client)) {
      return *p;
    }
  }
  throw InfeasibilityException("Client cannot be served from any parking.");
}


/**
 * Return true iif the given client may be a possible addition to this cluster.
 *
 * This is a simple helper that checks if the client can be served from this
 * cluster's parking.
 */
bool Cluster::is_compatible(const Client& c) const
{
  return pb_.clients(id()).count(&c);
}



/**
 * Return attractiveness of adding client to cluster.
 *
 * If adding the client is not feasible, return 0. If adding the client does
 * not incur any cost, return a configurable maximum.
 *
 * The attractiveness is the inverse of the sum of additional st for 1 to
 * numerous workers. If the addition requires additional workers,
 * a) add the full service time of the original cluster w/ the now
 * impossible number of service workers multiplied w/ a penalty constant
 * to the delta (this works best!) or
 * b) add the service time of the newly required worker as delta; this
 * already incorporates a penalty, so we can drop the penalty constant;
 * this extra service time is the mean of the possible service times for
 * all feasible numbers of workers; however, since an additional worker
 * is required and the now impossible number of workers is still part of
 * the old total service time, delta_workers is incremented by one
 * => this works considerably worse then approach a)
 *
 * Note: I also tried to weight the number of workers in the deltas (a
 * delta for two workers would be counted twice, a delta for three workers
 * would be counted three times). This also worked considerably worse
 * than the current approach.s
 */
Attractiveness Cluster::evaluate_addition(const Client& c, bool weighted) const
{
  Cluster clone(*this);
  if (!clone.add(c)) {  // addition not successful
    return 0.0;
  }
  Time old_total(calc_total_st(weighted));
  Time new_total(clone.calc_total_st(weighted));
  Time delta(new_total - old_total);
  for (auto workers = min_workers(); workers < clone.min_workers(); ++workers) {
    if (weighted) {
      delta += st(workers) * pb_.cfg().attractiveness_penalty() *
        static_cast<Time>(workers);
    } else {
      delta += st(workers) * pb_.cfg().attractiveness_penalty();
    }
  }
  if (delta >= pb_.cfg().min_delta()) return 1.0 / delta;
  return pb_.cfg().max_attr();
}


/**
 * Return attractiveness of cluster.
 *
 * This method aims mainly for newly created clusters as it does not account
 * for requiring a minimum amount of workers.
 *
 * The weighted attractiveness is 1 / total service time where the total service
 * time is the time to serve the cluster with 1 worker + 2 times the time to
 * serve the cluster with two workers + 3 times the time to serve the cluster
 * with three workers etc.
 *
 * If `weighted` is set to false, the service time required by each number of
 * workers is simply added.
 *
 */
Attractiveness Cluster::attractiveness(bool weighted) const
{
  Time total_st(calc_total_st(weighted));
  if (total_st >= pb_.cfg().min_delta()) return 1.0 / total_st;
  return pb_.cfg().max_attr();
}


/**
 * Return true if the cluster is feasible internally.
 *
 * A cluster is feasible if all clients can be served in time and the truck
 * can return to the depot before it closes.
 *
 * The purpose of this method is solely for debugging and controlling all
 * results as a cluster can never become infeasible.
 */
bool Cluster::is_feasible()
{
  throw std::runtime_error("The method or operation is not implemented.");
}


/**
 * The required service time given the number of workers.
 *
 * Throws and InfeasibilityException if the service time for an infeasible
 * number of workers is demanded.
 */
Time Cluster::st(Index num_workers) const
{
  if (num_workers < min_workers_) {
    throw InfeasibilityException("Queried infeasible st(.).");
  }
  return sts_[num_workers - 1];
}


/**
 * Update the ests, lsts and sts for each possible number of workers.
 *
 * \return true if the cluster is feasible, otherwise false
 */
bool Cluster::update_times()
{
  for (Index workers = pb_.max_workers(); workers >= 1; --workers) {
    ests_[workers - 1] = Constants::NoTime;
    lsts_[workers - 1] = Constants::NoTime;
    sts_[workers - 1] = Constants::NoTime;
    try {
      Cluster::Assignment a(*this, workers);
      ests_[workers - 1] = a.earliest_start();
      lsts_[workers - 1] = a.latest_start();
      sts_[workers - 1] = a.service_time();
      min_workers_ = workers;
    } catch (InfeasibilityException exc) {
      if (workers == pb_.max_workers()) {
        return false;
      }
      break;
    }
  }
  return true;
}


/**
 * A Schedule represents the work (sorted clients) of a single worker.
 */
Cluster::Schedule::Schedule(const Cluster& c)
  : m_cluster(c)
{
  m_earliest_end = c.earliest_start_;
}


/**
 * Add a client to a worker's schedule.
 */
void Cluster::Schedule::add(const Client& c)
{
  m_clients.push_back(&c);
  Time start = std::max(c.est(),
    m_earliest_end + m_cluster.pb_.walking_time(*m_cluster.parking_, c));
  m_earliest_end = start + c.st() +
    m_cluster.pb_.walking_time(*(m_cluster.parking_), c);
}


/**
 * Return the earliest end of a schedule if starting at the earliest start.
 *
 * The earliest start is the starting time at the depot + the driving time to
 * the cluster.
 */
Time Cluster::Schedule::earliest_end() const
{
  Time start = m_cluster.pb_.depot().est() + m_cluster.pb_.d(m_cluster.id());
  return earliest_end(start);
}


/**
 * Return the earliest end of a schedule given a start time at a cluster.
 */
Time Cluster::Schedule::earliest_end(Time start) const
{
  const Problem& pb = m_cluster.pb_;
  const Parking& p = *m_cluster.parking_;
  for (const Client* c: m_clients) {
    start += pb.walking_time(p, *c);  // walk to client
    if (round(start, pb.cfg().digits()) > round(c->lst(), pb.cfg().digits())) {
      // TODO: rewrite w/out exceptions using Constants::NoTime as return value
      throw InfeasibilityException("must not arrive too late at client");
    }
    start = std::max(start, c->est());  // wait before starting if required
    start += c->st() + pb.walking_time(p, *c);  // work and return
  }
  return start;
}


/**
 * Return the latest starting time of the current schedule.
 *
 * If the worker has no clients, he doesn't ever need to start.
 * If the schedule is not feasible, an InfeasibilityException is thrown.
 * This is also the case if the schedule would require the truck to arrive
 * earlier than possible (earliest arrival is depot opening + driving time to
 * cluster).
 */
Time Cluster::Schedule::latest_start() const
{
  const Problem& pb = m_cluster.pb_;
  const Parking& p = *m_cluster.parking_;
  if (!m_clients.size()) {
    return pb.depot().lst();  // proxy for a high enough start time
  }
  const Client* c(m_clients[m_clients.size() - 1]);
  // start from when we're done
  Time lst = c->lst() + c->st() + pb.walking_time(p, *c);
  // make sure it's still possible to return to the depot
  lst = std::min(lst, pb.depot().lst() - pb.d(p.id()));
  for (auto it = m_clients.rbegin(); it != m_clients.rend(); ++it) {
    c = *it;
    lst -= pb.walking_time(p, *c);  // walk back
    lst -= c->st();
    if (lst < c->est()) {
      throw InfeasibilityException("Current schedule is not feasible");
    }
    lst = std::min(lst, c->lst());  // start to work
    lst -= pb.walking_time(p, *c);  // walk to customer
  }
  if (lst < m_cluster.earliest_start_) {
    throw InfeasibilityException("Truck arrives too late for schedule");
  }
  return lst;
}


/**
 * Calculate the latest time a worker can return from a customer.
 */
Time Cluster::calc_latest_return(const Client* c) const {
  return c->lst() + c->st() + pb_.walking_time(*parking_, *c);
}


/**
 * Calculate the latest time a worker has to leave the parking to a client.
 */
Time Cluster::calc_latest_start(const Client* c) const
{
  return c->lst() - pb_.walking_time(*parking_, *c);
}


/**
 * Return the sum of the feasible service times.
 *
 * I.e. if the cluster is feasible with 2 workers, the service time required
 * by 2 + the time required by three + the time up to the total max. number
 * of workers is returned.
 *
 * If the service times are weighted, then a cluster service time requiring
 * n workers is counted n times, otherwise once.
 */
Time Cluster::calc_total_st(bool weighted) const
{
  Time total(0.0);
  if (weighted) {
    for (auto num_workers = min_workers(); num_workers <= pb_.max_workers();
        ++num_workers) {
      total += st(num_workers) * static_cast<Time>(num_workers);
    }
  } else {
    for (auto num_workers = min_workers(); num_workers <= pb_.max_workers();
        ++num_workers) {
      total += st(num_workers);
    }
  }
  return total;
}


/**
 * An Assignment represents the schedule of each worker in a group of workers.
 *
 * For any possible number of workers, there is a separate assignment.
 * This constructor tries different client orderings until a feasible
 * assignment is found. If none can be found, an InfeasibilityException is
 * thrown.
 */
Cluster::Assignment::Assignment(const Cluster& c, Index workers)
  : m_clients(c.clients_),
    m_cluster(c),
    m_schedules(workers, Schedule(c))
{
  try {
    sort_clients_by_latest_start();
    assign();
  } catch (InfeasibilityException& exc) {
    sort_clients_by_latest_return();
    assign();  // if this fails, the exception is propagated to the caller
  }
}


/**
 * Return a reference to the schedule of the worker that will be available
 * soonest (i.e. the schedule that is finished earlier than all the others).
 */
Cluster::Schedule& Cluster::Assignment::next_schedule()
{
  return *std::min_element(m_schedules.begin(), m_schedules.end(),
                           [] (const Schedule& s1, const Schedule& s2)->bool {
                             return s1.earliest_end() < s2.earliest_end();
                           });
}


/**
 * Attempt to assign this assignment's clients to the available workers.
 *
 * The current order of the clients is maintained.
 * If no feasible assignment can be obtained, an `InfeasibilityException` is
 * thrown.
 */
void Cluster::Assignment::assign()
{
  for (const Client* c: m_clients) {
    Cluster::Schedule& s = next_schedule();
    s.add(*c);
  }
  m_latest_start = calc_latest_start();  // throws if infeasible
  // if this is reached, the current assignment is feasible
  Time earliest_end = calc_end(m_cluster.earliest_start_);
  Time latest_end = calc_end(m_latest_start);
  Time delta = latest_end - earliest_end;
  m_earliest_start = m_latest_start - delta;  // see prototype for rationale
  m_service_time = earliest_end - m_earliest_start;
}


/**
 * Return the assignment's earliest end given a start time.
 */
Time Cluster::Assignment::calc_end(Time start)
{
  std::vector<Time> times;
  for (Cluster::Schedule& s: m_schedules) {
    times.push_back(s.earliest_end(start));
  }
  return *std::max_element(times.begin(), times.end());
}


/**
 * Set the assignments latest start or throw an `InfeasibilityException` if
 * assigning the clients to worker schedules is not feasible in their
 * current order.
 */
Time Cluster::Assignment::calc_latest_start()
{
  std::vector<Time> lsts;
  for (Cluster::Schedule& s: m_schedules) {
    lsts.push_back(s.latest_start());
  }
  return *std::min_element(lsts.begin(), lsts.end());
}


/**
 * Sort the assignment's internal client* list in place by latest return time.
 */
void Cluster::Assignment::sort_clients_by_latest_return()
{
  std::sort(m_clients.begin(), m_clients.end(),
            [&](const Client* a, const Client* b) {
              return m_cluster.calc_latest_return(a) < m_cluster.calc_latest_return(b);
            });
}


/**
 * Sort the assignment's internal client* list in place by latest start time.
 */
void Cluster::Assignment::sort_clients_by_latest_start()
{
  std::sort(m_clients.begin(), m_clients.end(),
            [&](const Client* a, const Client* b) {
              return m_cluster.calc_latest_start(a) < m_cluster.calc_latest_start(b);
            });
}


/**
 * Textual representation of cluster.
 */
std::ostream& operator<<(std::ostream& os, const Cluster& c)
{
  os << "Cluster(";
  os << c.cid() << " ";
  os << "p" << c.id() << " " << c.x() << "/" << c.y() << " [";
  auto it = c.clients_.begin();
  os << (*it)->id();
  ++it;
  for (; it != c.clients_.end(); ++it) {
    os << "," << (*it)->id();
  }
  os << "])";
  return os;
}


/**
 * Return attractiveness of potential new cluster.
 *
 * The weighted attractiveness is 1 / total service time where the total service
 * time is the time to serve the cluster with 1 worker + 2 times the time to
 * serve the cluster with two workers + 3 times the time to serve the cluster
 * with three workers etc.
 * If the cluster would be infeasible for the given problem, 0 is returned.
 *
 * For performance reasons, the attractiveness does not instantiate a new
 * Cluster as this would require additional calculations.
 */
// TODO: maybe remove entirely
// Attractiveness evaluate_new_cluster(const Problem& pb,
//                                   const Parking& p, const Client& c)
// {
//   if (not pb.is_feasible(*parking_, client)) return 0.0;
//
//     d = problem.d
//     aest = d[problem.depot.id, parking.id]
//     aest += d[parking.id, client.id] * problem.walking_time
//     if aest > client.lst:  # client cannot be reached on time
//         return 0
//     time = max(aest, client.est)
//     time += client.st + d[client.id, parking.id] * problem.walking_time
//     time += d[parking.id, problem.depot.id]
//     if time > problem.depot.lst:  # cannot return to depot on time
//         return 0
//     st = d[client.id, parking.id] * problem.walking_time
//     st += client.st
//     st += d[parking.id, client.id] * problem.walking_time
//     #return 1 / (st * cfg.MAX_WORKERS)  # only one worker is needed
//     total_st = 0
//     # additional workers don't speed things up
//     for num_workers in range(1, cfg.MAX_WORKERS + 1):
//         total_st += st * num_workers
//     return 1 / total_st
// }
}
