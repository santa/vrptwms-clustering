/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CLUSTER_H
#define CLUSTER_H

#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/concept_check.hpp>
#include <gtest/gtest_prod.h>

#include "common.hpp"
#include "parking.hpp"
#include "route.hpp"

using boost::property_tree::ptree;


namespace VRPTWMS {
class Client;
class Problem;


/**
 * A cluster represents one or more clients and a single parking.
 *
 * The cluster's coordinates denote the location of its parking.
 *
 */
class Cluster {
public:
  explicit Cluster(const Problem& pb, const Parking& parking,
                   const Client& client);
  explicit Cluster(const Problem& pb, const Client& client);
  explicit Cluster(const Problem& pb, const Parking& parking,
                   const ptree& data);
  explicit Cluster(const Cluster& other);
  Cluster(const Cluster&& other);  // must not be explicit to allow std::sort
  Cluster& operator=(const Cluster& rhs);
  Cluster& operator=(const Cluster&& rhs);

  /**
   * The cluster's id is the cluster parking's id.
   *
   * This allows it to be used in the distance matrix.
   */
  Index id() const {return parking_->id();}
  /**
   * The cluster's x coordinate is the cluster parking's x coordinate.
   */
  double x() const {return parking_->x();}
  /**
   * The cluster's y coordinate is the cluster parking's y coordinate.
   */
  double y() const {return parking_->y();}
  /**
   * The minimum number of workers for which this cluster is feasible.
   */
  Index min_workers() const {return min_workers_;}
  /**
   * The cluster's total demand (sum of each of the client's demands).
   * Must not exceed the truck capacity.
   */
  /**
   * Cluster ID; only relevant for routing-only problems
   */
  Index cid() const {return cid_;}
  Amount demand() const {return demand_;}
  /**
   * The earliest start time given the number of workers.
   */
  Time est(Index num_workers) const {return ests_[num_workers - 1];}
  /**
   * The latest start time given the number of workers.
   */
  Time lst(Index num_workers) const {return lsts_[num_workers - 1];}
  /**
   * Actual earliest start time; depends on the routing.
   */
  Time aest() const {return aest_;}
  /**
   * Actual latest start time; depends on the routing.
   */
  Time alst() const {return alst_;}
  const std::vector<const Client*>& clients() const {return clients_;};
  const std::vector<Index> client_ids() const;
  bool is_feasible();
  Time st(Index num_workers) const;

  // Cluster construction logic
  bool add(const Client& c);
  Attractiveness evaluate_addition(const Client& c, bool weighted=false) const;
  Attractiveness attractiveness(bool weighted=false) const;

private:
  // TODO: add all functions required for algorithmically creating clusters
  const Problem& pb_;
  std::vector<const Client*> clients_;
  Time earliest_start_;  /** Earliest arrival of truck */
  const Parking* parking_;
  Index cid_;
  static Index next_cid_;
  Amount demand_;
  Index min_workers_;
  std::vector<Time> ests_;
  std::vector<Time> lsts_;
  std::vector<Time> sts_;
  Time aest_;  /** Actual earliest start time; depends on the routing */
  Time alst_;  /** Actual latest start time; depends on the routing */

  // Cluster construction logic
  void clone(const Cluster& other);
  bool update_times();

  // Helper functions
  bool is_compatible(const Client& c) const;
  Time calc_latest_return(const Client* c) const;
  Time calc_latest_start(const Client* c) const;
  Time calc_total_st(bool weighted=false) const;
  static const Parking& find_closest_feasible_parking(const Problem& pb,
                                                      const Client& client);


  friend std::ostream& operator<< (std::ostream& os, const Cluster& c);
  // Allow routes to update the aest_ and alst_ members.
  friend bool Route::reduce_service_workers();
  friend void Route::update_ests(std::list< Cluster* >::iterator it);
  friend void Route::update_lsts(std::list< Cluster* >::reverse_iterator it);

  FRIEND_TEST(TestCluster, test_Cluster_clients);
  FRIEND_TEST(TestCluster, test_calc_total_st);
  FRIEND_TEST(TestCluster, test_update_times_st_two_clients);
  FRIEND_TEST(TestCluster, test_update_times_st_three_clients);
  FRIEND_TEST(TestCluster, test_update_times_st_waiting);
  FRIEND_TEST(TestCluster, test_clone_correct_values);
  FRIEND_TEST(TestCluster, test_clone_independent);
  // for resetting the cluster ids.
  FRIEND_TEST(TestDeterministicClusteringSolver, test_DeterministicClusteringSolver);
  FRIEND_TEST(TestRoute, test_insert);

  class Schedule {
  public:
    Schedule(const Cluster& c);
    void add(const Client& c);
    Time earliest_end() const;
    Time earliest_end(Time start) const;
    Time latest_start() const;
  private:
    Time m_earliest_end;
    std::vector<const Client*> m_clients;
    const Cluster& m_cluster;
  };

  class Assignment {
  public:
    Assignment(const Cluster& c, Index workers);
    std::vector<const Client*> ordered_clients() const {return m_clients;};
    Time earliest_start() const {return m_earliest_start;};
    Time latest_start() const {return m_latest_start;};
    Time service_time() const {return m_service_time;};
  private:
    std::vector<const Client*> m_clients;
    const Cluster& m_cluster;
    Time m_earliest_start;
    Time m_latest_start;
    std::vector<Cluster::Schedule> m_schedules;
    Time m_service_time;

    void assign();
    Time calc_end(Time start);
    Time calc_latest_start();
    Cluster::Schedule& next_schedule();
    void sort_clients_by_latest_return();
    void sort_clients_by_latest_start();
  };
};


// Attractiveness evaluate_new_cluster(const Problem& pb,
//                                     const Parking& p, const Client& c);
std::ostream& operator<< (std::ostream& os, const Cluster& c);
}

#endif //CLUSTER_H
