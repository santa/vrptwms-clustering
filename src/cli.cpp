/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cstdlib>  // srand48
#include <iostream>
#include <string>
#include <thread> // std::thread
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <Fl/fl_draw.H>

#include "aco_solver.hpp"
#include "deterministic_solver.hpp"
#include "gui.hpp"
#include "problem.hpp"
#include "solution.hpp"

namespace fs = boost::filesystem;
namespace po = boost::program_options;


static std::string help_clustering_heuristic() {
  std::string s("Choose clustering heuristic; available heuristics are:");
  for (VRPTWMS::ClusteringHeuristic heuristic(VRPTWMS::ClusteringHeuristic::FIRST);
       heuristic != VRPTWMS::ClusteringHeuristic::LAST; ++heuristic) {
    s += "\n" + VRPTWMS::Config::short_name(heuristic);
  }
  return s;
}


/**
 * Print a usage message to standard out.
 */
void print_usage(char* executable) {
  fs::path executable_path(executable);
  std::cout << "Usage: " << executable_path.filename().string();
  std::cout << " [options] infile..." << std::endl;
}


int main(int argc, char **argv) {
  try {
    std::vector<std::string> cluster_solutions;
    VRPTWMS::Config cfg;

    po::options_description desc("Options");
    desc.add_options()
      ("ants", po::value<VRPTWMS::Index>()->default_value(cfg.ants()),
       "Set the number of ants; 0 sets ants to number of parkings in each problem")
      ("clustering-heuristic", po::value<std::string>()->default_value(
       VRPTWMS::Config::short_name(cfg.clustering_heuristic())),
       help_clustering_heuristic().c_str())
      ("clusters,c", po::value<std::vector <std::string> >(),
       "Clustering solutions (only routing will be solved)")
      ("debug", "show debugging output")
      ("default-walking-speed",
       po::value<VRPTWMS::Speed>()->default_value(cfg.default_walking_speed()),
       "Override the default walking speed for instances lacking this information")
      ("deterministic", "Use deterministic heuristic instead of metaheuristic")
      ("enable-pheromone-gui", "Show a small progress gui for the pheromone")
      ("help,h", "Print this help message")
      ("penalty,p",
       po::value<double>()->default_value(cfg.attractiveness_penalty()),
       "Penalty for cluster attractiveness when adding a customer requires more workers")
      ("runtime,r", po::value<VRPTWMS::Seconds>()->default_value(cfg.max_runtime()),
      "Runtime per instance (in seconds)\nset to 0 to disable this limit")
      ("seed", po::value<long int>()->default_value(cfg.random_seed()),
      "Select the seed for the pseudo random number generator (for debugging)")
      ("verbosity,v", po::value<VRPTWMS::Index>()->
       default_value(static_cast<VRPTWMS::Index>(cfg.verbosity())),
       "Set the verbosity level")
      ("version", "Output version information and exit")
    ;
    po::variables_map vm;
    po::parsed_options parsed =
      po::command_line_parser(argc, argv).options(desc).run();
    po::store(parsed, vm);
    po::notify(vm);
    if (vm.count("help")) {
      print_usage(argv[0]);
      std::cout << desc;
      return 0;
    }
    if (vm.count("version")) {
      std::cout << "vrptwms " << __DATE__ << " " << __TIME__ << std::endl;
      return 0;
    }
    std::vector<std::string> args = po::collect_unrecognized(parsed.options,
                                                             po::include_positional);
    if (!args.size()) {
      print_usage(argv[0]);
      std::cout << "ERROR: no input file given\n";
      return 0;
    }

    if (vm.count("clusters")) {
      cluster_solutions = vm["clusters"].as< std::vector<std::string> >();
      if (cluster_solutions.size() != args.size()) {
        std::cout << "ERROR: clustering must be provided once per input file\n";
        return 0;
      }
    }

    cfg.ants(vm["ants"].as<VRPTWMS::Index>(), true);
    cfg.attractiveness_penalty(vm["penalty"].as<double>());
    cfg.clustering_heuristic(vm["clustering-heuristic"].as<std::string>());
    if (vm.count("debug")) {
      cfg.debug(true);
    }
    if (vm.count("deterministic")) {
      cfg.deterministic(true);
    }
    cfg.default_walking_speed(vm["default-walking-speed"].as<VRPTWMS::Speed>());
    cfg.max_runtime(vm["runtime"].as<VRPTWMS::Seconds>());
    if (vm.count("enable-pheromone-gui")) {
      cfg.pheromone_gui(true);
    }
    cfg.random_seed(vm["seed"].as<long int>());
    srand48(cfg.random_seed());  // initialize randomizer
    cfg.verbosity(static_cast<VRPTWMS::VerbosityLevel>(vm["verbosity"].as<VRPTWMS::Index>()));

    std::cout << cfg << std::endl;

    // Solve problem and print output
    VRPTWMS::Cost total_cost(0);
    for (std::vector<std::string>::size_type i = 0; i < args.size(); ++i) {
      VRPTWMS::Problem pb(args[i], cfg);
      if (cluster_solutions.size()) {
        VRPTWMS::Solution sol(pb, cluster_solutions[i]);
        VRPTWMS::DeterministicRoutingSolver s(sol);
        s.solve();
        std::cout << sol << std::endl;
      } else {
        VRPTWMS::Solution sol(pb);
        if (cfg.deterministic()) {
          VRPTWMS::DeterministicClusteringSolver clustering_solver(sol);
          clustering_solver.solve();
          VRPTWMS::DeterministicRoutingSolver routing_solver(sol);
          routing_solver.solve();
        } else {
          if (cfg.pheromone_gui()) {
            VRPTWMS::Gui gui(sol, 7);
            VRPTWMS::ACOSolver aco_solver(sol, std::bind(&VRPTWMS::Gui::update,
                                                         &gui,
                                                         std::placeholders::_1,
                                                         std::placeholders::_2,
                                                         std::placeholders::_3,
                                                         std::placeholders::_4));
            gui.show();
            std::thread solver_thread(&VRPTWMS::ACOSolver::solve, &aco_solver);
            std::thread gui_thread(Fl::run);
            std::cout << "concurrent execution...\n";
            solver_thread.join();                // pauses until first finishes
            std::cout << "solver is done; close gui to finish\n";
            gui_thread.join();               // pauses until second finishes
            std::cout << "gui closed\n";
          } else {
            VRPTWMS::ACOSolver aco_solver(sol);
            aco_solver.solve();
          }
        }
        total_cost += sol.total_cost();
        std::cout << sol << std::endl;
      }
    }
    if (args.size() > 1) {
      std::cout << "average cost: " << total_cost / VRPTWMS::Cost(args.size()) << std::endl;
    }
    return 0;
  }
  catch(std::exception& e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  }
  catch(...) {
    std::cerr << "Exception of unknown type!\n";
  }

  return 0;
}
