/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It declares a class representing the problem to be solved.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PROBLEM_H
#define PROBLEM_H

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "client.hpp"
#include "common.hpp"
#include "config.hpp"
#include "depot.hpp"
#include "matrix.hpp"
#include "parking.hpp"


namespace VRPTWMS {
class Problem {
public:
  explicit Problem(const std::string& filename, Config config=Config());
//   explicit Problem(const std::string& filename,
//                    const std::string& cfg_filename);
  explicit Problem(const Problem& other);
  Problem& operator=(const Problem& other);
  ~Problem();

  /**
   * The problem instance's name.
   */
  const std::string name() const {return name_;}
  /**
   * The problem instance's input filename.
   */
  const std::string filename() const {return filename_;}
  /**
   * The maximum amount of goods a truck can transport.
   */
  Amount capacity() const {return capacity_;}
  const Parking& parking(Index id) const;
  /**
   * All available parking spaces.
   */
  const std::vector<Parking>& parkings() const {return parkings_;}
  /**
   * Pointers to all available parking spaces.
   */
  const std::vector<const Parking*>& parking_ptrs() const {return parking_ptrs_;}
  /**
   * Return a set of all parking pointers that can be used to serve `client`.
   */
  const std::set<const Parking*>& parkings(const Client& c) const {return client_parking_combinations_.at(c.id());}
  /**
   * All clients that have to be served.
   */
  const std::vector<Client>& clients() const {return clients_;}
  /**
   * Return a set of all client pointers that can be served from `parking`.
   */
  const std::set<const Client*>& clients(const Parking& p) const {return parking_client_combinations_.at(p.id());}
  const std::set<const Client*>& clients(Index p_id) const {return parking_client_combinations_.at(p_id);}
  const Client& client(Index id) const;
  /**
   * Return the number of locations.
   * The number of locations is the number of clients + the number of parkings
   * where the latter includes the depot.
   */
  Index num_locations() const {return d().num_rows();}
  /**
   * The central depot, from which trucks and workers serve the clients.
   */
  const Depot& depot() const {return depot_;}
  /**
   * The distance matrix.
   * A square lxl matrix where l is the number of locations (depot + number of
   * parkings + number of clients).
   */
  const Matrix<Distance>& d() const {return distances_;}
  /**
   * Return the distance between the locations with indices `from` and `to`.
   */
  Distance d(Index from, Index to) const {return distances_[from][to];}
  /**
   * Return the distance between the depot and the location with index `index`.
   */
  Distance d(Index index) const {return distances_[depot().id()][index];}
  /**
   * Return the distance between the depot and the given parking.
   */
  Distance d(const Parking& p) const {return d(p.id());}
// TODO
// times don't make sense here as they differ depending on the clusters that
// are created
// => need to evaluate if this would make sense for the performance of a
// routing-only solver
//   /**
//    * The time matrices.
//    *
//    * Each element represents the time to serve a cluster with the given number
//    * of workers and to drive to the next cluster.
//    */
//   const Matrix<Time> times(Index num_workers) const {return times_;}
  /**
   * Return the program's configuration.
   */
  const Config& cfg() const {return cfg_;}
  Index best_known_trucks() const {return best_known_trucks_;}
  Index best_known_workers() const {return best_known_workers_;}
  Distance best_known_distance() const {return best_known_distance_;}
  Cost best_known_cost() const {return best_known_cost_;}
  /**
   * The maximum number of workers per vehicle.
   */
  Index max_workers() const {return cfg_.max_workers();}
  /**
   * Walking time per distance unit.
   */
  Time walking_time() const {return walking_time_;}
  Time walking_time(const Parking& p, const Client& c) const;

  bool proceed() const;
  bool is_feasible(const Parking& parking, const Client& client) const;

private:
  static Depot read_depot(const std::string filename);
  void init(const std::string& filename);

  Config cfg_;
  Depot depot_;
  std::string name_;
  std::string filename_;
  std::vector<Client> clients_;
  std::vector<Parking> parkings_;
  std::vector< const Parking* > parking_ptrs_;
  Amount capacity_;
  Matrix<Distance> distances_;
  Index best_known_trucks_;
  Index best_known_workers_;
  Distance best_known_distance_;
  Cost best_known_cost_;
  Time walking_time_;  /** Walking time per distance unit. */
  Matrix<Time> walking_times_;  /** Matrix of walking times (avoids recalculation) */

  /** Possible parkings for a given client index **/
  std::map< Index, std::set<const Parking*> > client_parking_combinations_;
  /** Possible clients for a given parking index **/
  std::map< Index, std::set<const Client*> > parking_client_combinations_;
  void associate_clients_with_parkings();
};

std::ostream& operator<< (std::ostream& os, const Problem& p);
}

#endif // PROBLEM_H
