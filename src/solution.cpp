/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It defines a class for storing an manipulating solutions.
 *
 * Copyright (C) 2014  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <set>
#include <string>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "common.hpp"
#include "cluster.hpp"
#include "parking.hpp"
#include "problem.hpp"
#include "route.hpp"

#include "solution.hpp"

using boost::property_tree::ptree;


namespace VRPTWMS {
/**
 * Constructor reading clusters from input file.
 */
Solution::Solution(const Problem& pb, const std::string& cluster_filename)
    : cpu_time_(0), pb_(pb)
{
  ptree pt;
  boost::property_tree::json_parser::read_json(cluster_filename, pt);

  ptree clusters_tree = pt.get_child("clusters");
  for (auto it = clusters_tree.begin(); it != clusters_tree.end(); ++it) {
    Index parking_id = it->second.get<Index>("parking_id");
    clusters_.push_back(Cluster(pb_, pb_.parking(parking_id), it->second));
  }
}


Solution::Solution(const Solution& other) : pb_(other.pb_)
{
  // TODO: implement
  // careful: the new solution gets its own clusters and the new solution's
  //   routes must be updated to the new cluster pointers :(
}


/**
 * Set the current solution's value to the other solution's values.
 */
Solution& Solution::operator=(Solution&& other)
{
  if (pb().name() != other.pb().name()) {
    throw std::runtime_error("ERROR: incompatible solutions");
  }
  cpu_time_ = other.cpu_time_;
  clusters_ = std::move(other.clusters_);
  routes_ = std::move(other.routes_);
//   other.clusters_ = std::vector<Cluster>();  // should not neccessary by move semantic
//   other.routes_ = std::vector<Route> ();
  return *this;
}


/**
 * Return the total driving distance required by the solution.
 */
Distance Solution::distance() const
{
  double total_distance(0);
  for (auto it = routes_.begin(); it != routes_.end(); ++it) {
    total_distance += (*it).length();
  }
  return total_distance;
}


/**
 * Return the total number of workers required by the solution.
 */
Index Solution::workers() const
{
  Index workers(0);
  for (auto it = routes_.begin(); it != routes_.end(); ++it) {
    workers += (*it).workers();
  }
  return workers;
}

/**
 * Return true if the solution (all clusters and routes) are feasible.
 */
bool Solution::is_feasible() const
{
  return has_feasible_clusters() && has_feasible_routes();
}

/**
 * Return true if the clusters are feasible.
 *
 * The clusters are feasible if each cluster is feasible internally and if each
 * client is served by exactly one cluster.
 */
bool Solution::has_feasible_clusters() const
{
  // TODO: finish implementation; the only thing checked now is whether each
  // client is assigned to exactly one cluster
  std::set<Index> client_ids;
  for (const Client& client: pb().clients()) {
    client_ids.insert(client.id());
  }
  for (const Cluster& cluster: clusters_) {
    for (Index client_id: cluster.client_ids()) {
      auto it = client_ids.find(client_id);
      if (it == client_ids.end()) {
        std::cerr << "Client " << client_id << " served more than once\n";
        return false;
      }
      client_ids.erase(it);
    }
  }
  if (!client_ids.empty()) {
    std::cerr << "did not serve the following clients: ";
    for (Index i: client_ids) {
      std::cerr << i << " ";
    }
    std::cerr << std::endl;
    return false;
  }
  return true;
}


/**
 * Return true if the routes are feasible.
 *
 * The routes are feasible if each route is feasible internally and if each
 * cluster is on exactly one route.
 */
bool Solution::has_feasible_routes() const
{
  std::set<Index> served;
  for (const Cluster& cluster: clusters_) {
    assert(served.find(cluster.cid()) == served.end());  // c ids must be unique
    served.insert(cluster.cid());
  }
  for (const Route& r: routes_) {
    if (not r.is_feasible()) {
      std::cerr << r << " is not feasible" << std::endl;
      return false;
    }
    for (Index cid: r.cluster_ids()) {
      auto it = served.find(cid);
      if (it == served.end()) {
        std::cerr << "\nserved cid " << cid << "more than once" << std::endl;
        return false;
      } else {
        served.erase(it);
      }
    }
  }
  if (!served.empty()) {
    std::cerr << "did not serve the following clusters: ";
    for (Index i: served) {
      std::cerr << i << " ";
    }
    std::cerr << std::endl;
    return false;
  }
  return true;
}


/**
 * Improve current solution.
 *
 * This function offers an external interface for running the local search.
 */
void Solution::improve()
{
  for (Route& r: routes_) {
    r.reduce_service_workers();
  }
}


/**
 * Reset solution object to original, unsolved state.
 *
 * The timer is not touched to reflect the total solution time.
 * This is required by stochastic heuristics and construction based
 * metaheuristics.
 */
void Solution::reset()
{
  Route::next_id_ = 0;
  routes_.clear();
  clusters_.clear();
}


/**
 * Return the total cost of this solution.
 */
Cost Solution::total_cost() const
{
  return Cost(trucks()) * pb().cfg().cost_truck() +
    Cost(workers()) * pb().cfg().cost_worker() +
    distance() * pb().cfg().cost_distance();
}


/**
 * Write details about the current solution to the given output stream.
 */
std::ostream& Solution::write_details(std::ostream& os) const
{
  os << "Clusters:";
  for (const Cluster& cluster: clusters_) {
    os << "\n" << cluster;
  }
  os << "\nRoutes:";
  for (const Route& route: routes()) {
    os << "\n" << route;
  }
  os << "\n" << *this;
  return os;
}


std::ostream& operator<<(std::ostream& os, const Solution& s)
{
  assert(s.is_feasible());
  os << s.pb().name() << ",";
  os << s.clusters_.size() << ",";
  os << s.trucks() << ",";
  os << s.workers() << ",";
  os << s.distance() << ",";
  os << s.total_cost() << "," << s.milliseconds();
  return os;
}
}
