/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROUTE_H
#define ROUTE_H

#include <list>
#include <vector>

#include <gtest/gtest_prod.h>

#include "common.hpp"


namespace VRPTWMS {
class ACOSolver;
class Cluster;
struct Insertion;
class Problem;


class Route {
public:
  explicit Route(const Problem& pb, Cluster& seed);
//   Route (const Problem& pb, const Cluster& seed, Index id);
  explicit Route(const Route& other);
  Route& operator=(const Route& other);
  ~Route() {}

  /**
   * Return a constant reference to the list of clusters on the route.
   * Required for updating the routing pheromone.
   */
  const std::list<Cluster*>& stops() const {return stops_;}
  /**
   * To be used by solvers to identify routes and their virtual depots.
   */
  Index id() const {return id_;}
  /**
   * Return the number of stops (clusters) the route serves.
   */
  Index num_stops() const {return stops_.size();}
  /**
   * Return true if no stops are on the route.
   */
  bool empty() const {return stops_.empty();}
  /**
   * The total demand by all clusters on the route.
   */
  Amount demand() const {return demand_;}
  /**
   * The number of workers used on the route.
   */
  Index workers() const {return workers_;}


  bool can_insert(std::list<Cluster*>::const_iterator pos, const Cluster& c,
                  bool check_capacity = false) const;
  std::vector<Index> cluster_ids() const;
  bool is_feasible() const;
  bool is_feasible_with(Index workers, std::vector<Time>& aests) const;
  void insert(std::list<Cluster*>::iterator pos,
              Cluster& cluster);
  Distance length() const;
  bool reduce_service_workers();
  bool update_insertion(Cluster& c, Insertion& ins, const ACOSolver* solver=nullptr);
  /** Allows solvers to reset the id when generating multiple solutions. */
  static Index next_id_;

private:
  const Problem& pb_;
  Index id_;
  std::list<Cluster*> stops_;
  Amount demand_;
  Index workers_;

  friend std::ostream& operator<< (std::ostream& os, const Route& r);
  friend class Cluster;  // required so cluster can friend the update methods
  void update_ests(std::list<Cluster*>::iterator c);
  void update_lsts(std::list<Cluster*>::reverse_iterator c);

  FRIEND_TEST(TestRoute, test_can_insert_one_cluster);
  FRIEND_TEST(TestRoute, test_insert);
  FRIEND_TEST(TestRoute, test_is_feasible);
  FRIEND_TEST(TestRoute, test_is_feasible_with);
  FRIEND_TEST(TestRoute, test_length);
  FRIEND_TEST(TestRoute, test_reduce_service_workers);
  FRIEND_TEST(TestRoute, test_update_insertion);
  FRIEND_TEST(TestSolution, test_workers);
};

std::ostream& operator<< (std::ostream& os, const Route& r);
}

#endif //ROUTE_H
