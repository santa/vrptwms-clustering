/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It defines a class representing the problem to be solved.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "client.hpp"
#include "common.hpp"
#include "config.hpp"
#include "exceptions.hpp"
#include "depot.hpp"
#include "parking.hpp"

#include "problem.hpp"

using boost::property_tree::ptree;


namespace VRPTWMS {
Problem::Problem(const std::string& filename, Config config)
    : cfg_(config),
      depot_(read_depot(filename))
{
  init(filename);
}


void Problem::init(const std::string& filename)
{
  filename_ = filename;
  try {
    ptree pt;
    boost::property_tree::json_parser::read_json(filename, pt);
    capacity_ = pt.get<Amount>("truckCapacity");
    name_ = pt.get<std::string>("name");
    best_known_trucks_ = pt.get<Index>("bestKnownTrucks");
    best_known_workers_ = pt.get<Index>("bestKnownWorkers");
    best_known_distance_ = pt.get<Distance>("bestKnownDistance");
    best_known_cost_ = pt.get<Cost>("bestKnownCost");
    walking_time_ = 1 / pt.get<Speed>("walkingSpeed",
                                      cfg().default_walking_speed());
    parkings_.push_back(depot_);  // parkings include the depot
    ptree parkings_tree = pt.get_child("parkings");
    for (auto it = parkings_tree.begin(); it != parkings_tree.end(); ++it) {
      parkings_.push_back(Parking(it->second.get<Index>("id"),
                                  it->second.get<Distance>("x"),
                                  it->second.get<Distance>("y")));
    }
    ptree clients_tree = pt.get_child("clients");
    for (auto it = clients_tree.begin(); it != clients_tree.end(); ++it) {
      clients_.push_back(Client(it->second.get<Index>("id"),
                                it->second.get<Distance>("x"),
                                it->second.get<Distance>("y"),
                                it->second.get<Time>("est"),
                                it->second.get<Time>("lst"),
                                it->second.get<Time>("st"),
                                it->second.get<Amount>("demand")));
    }
  } catch (std::exception const& e) {
    std::cerr << e.what() << std::endl;
    throw;
  }

  std::vector<Location> locations;
  locations.reserve(parkings_.size() + clients_.size());  // preallocate memory
  locations.insert(locations.end(), parkings_.begin(), parkings_.end());
  locations.insert(locations.end(), clients_.begin(), clients_.end());
  distances_.resize(static_cast<int>(locations.size()),
                    static_cast<int>(locations.size()));
  walking_times_.resize(static_cast<int>(locations.size()),
                        static_cast<int>(locations.size()));
  for (auto it = locations.begin(); it != locations.end(); ++it) {
    for (auto it2 = locations.begin(); it2 != locations.end(); ++it2) {
      if (it->id() == it2->id()) {
        distances_[it->id()][it2->id()] = 0.0;
        continue;
      }
      Distance delta_x_square = (it->x() - it2->x()) * (it->x() - it2->x());
      Distance delta_y_square = (it->y() - it2->y()) * (it->y() - it2->y());
      distances_[it->id()][it2->id()] = sqrt(delta_x_square + delta_y_square);
      walking_times_[it->id()][it2->id()] = distances_[it->id()][it2->id()] *
        walking_time_;
    }
  }
  for (auto& p: parkings_) {
    parking_ptrs_.push_back(&p);
  }
  associate_clients_with_parkings();
  cfg_.update(*this);
}


// Problem::Problem(const std::string& filename, const std::string& cfg_filename)
//     : cfg_(cfg_filename) {
// }

// Problem::Problem(const Problem& other)
//     : depot_(other.depot()), distances_(other.d())
// {
// }

Problem::~Problem() {
//   timer_.elapsed();
// TODO: implement solution writeback
}

// Problem& Problem::operator=(const Problem& other) {
//   // TODO: implement
//   throw std::runtime_error("not implemented");
//   return *this;
// }


/**
 * Return true iif the the given client can be served by the given parking.
 *
 * This is only the case if the client can be reached from the depot in time
 * and it is also possible to return to the depot in time.
 */
bool Problem::is_feasible(const Parking& parking, const Client& client) const
{
  Time time(depot().est());
  time += d(parking);  // drive to parking
  time += walking_time(parking, client);  // walk to client
  if (time > client.lst()) return false;
  time = std::max(client.est(), time);
  time += client.st();
  time += walking_time(parking, client);
  time += d(parking);
  if (time > depot().lst()) return false;
  return true;
}


/**
 * Associate clients with parkings that can be feasibly combined.
 *
 * This allows a performance increase since algorithms don't have to
 * evaluate parkings or clusters with parkings that are too far to be feasible.
 */
void Problem::associate_clients_with_parkings()
{
  for (const Parking& parking: parkings_) {
    std::set<const Client*> possible_clients;
    parking_client_combinations_[parking.id()] = possible_clients;
  }
  for (const Client& client: clients_) {
    std::set<const Parking*> possible_parkings;
    for (const Parking* parking_ptr: parking_ptrs_) {
      if (is_feasible(*parking_ptr, client)) {
        possible_parkings.insert(parking_ptr);
        parking_client_combinations_[parking_ptr->id()].insert(&client);
      }
    }
    if (possible_parkings.empty()) {
      throw InfeasibilityException("Client cannot be served from any parking.");
    }
    client_parking_combinations_[client.id()] = possible_parkings;
  }
}


/**
 * Return the client with the given id.
 */
const Client& Problem::client(Index id) const {
  for (auto it = clients_.begin(); it != clients_.end(); ++it) {
    if (it->id() == id) {
      return *it;
    }
  }
  std::stringstream error;
  error << filename_ << " has no customer with id " << id;
  throw NonexistingClient(error.str());
}


/**
 * Return the parking with the given id.
 */
const Parking& Problem::parking(Index id) const {
  for (auto it = parkings_.begin(); it != parkings_.end(); ++it) {
    if (it->id() == id) {
      return *it;
    }
  }
  std::stringstream error;
  error << filename_ << " has no parking with id " << id;
  throw NonexistingParking(error.str());
}


/**
 * Return true if solver should proceed trying to find (better) solutions.
 *
 * When the maximum allowed runtime (in cpu nanoseconds) is exceeded, this
 * will return false.
 */
bool Problem::proceed() const {
  // TODO: implement
  return false;
}


/**
 * Walking time between the given locations.
 *
 * TODO: cache matrix of walking times to improve performance
 */
Time Problem::walking_time(const Parking& p, const Client& c) const
{
  return walking_times_[p.id()][c.id()];  // d(p.id(), c.id()) * walking_time();
}


// private static member functions
Depot Problem::read_depot(const std::string filename) {
  ptree pt;
  boost::property_tree::json_parser::read_json(filename, pt);
  ptree depot_tree = pt.get_child("depot");
  return Depot(depot_tree.get<Index>("id"),
               depot_tree.get<Distance>("x"),
               depot_tree.get<Distance>("y"),
               depot_tree.get<Time>("est"),
               depot_tree.get<Time>("lst"));
}


std::ostream& operator<<(std::ostream& os, const Problem& p) {
  os << "Problem " << p.name() << " (" << p.clients().size() << " clients, ";
  os << p.parkings().size() - 1 << " parkings)";
  return os;
}
}
