/**
 *
 * Implementation of a generic weighed roulette wheel to allow randomized
 * Element selection.
 *
 * Each element can be assigned a weight (attractiveness)
 * which linearly affects its likelihood to be selected. For performance
 * reasons it is best to store
 *
 * Copyright (C) 2014  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef WEIGHTED_ROULETTE_WHEEL_H
#define WEIGHTED_ROULETTE_WHEEL_H

#include <cstdlib>  // drand48
#include <utility>  // std::pair, std::make_pair
#include <vector>

#include <common.hpp>


namespace VRPTWMS {
template<class Elem> class WeightedRouletteWheel {
public:
  explicit WeightedRouletteWheel(Attractiveness min_attractiveness)
    : m_total_attractiveness(0.0), m_min_attractiveness(min_attractiveness) {}
  ~WeightedRouletteWheel() {}

  void add(Attractiveness attr, Elem e);
  bool empty() {return m_wheel.empty();}
  std::pair<Attractiveness, Elem>& spin();

private:
  std::vector< std::pair<Attractiveness, Elem> > m_wheel;
  Attractiveness m_total_attractiveness;
  Attractiveness m_min_attractiveness;
};

// template<class Elem>
// WeightedRouletteWheel<Elem>::WeightedRouletteWheel()
//   : m_total_attractiveness(0.0)
// {
//
// }


/**
 * Add an element with a given attractiveness to roulette wheel.
 *
 * Discards elements w/ 0 attractiveness for performance reasons and to avoid
 * adding invalid clients.
 */
template<class Elem>
void WeightedRouletteWheel<Elem>::add(Attractiveness attr, Elem e) {
  if (attr) {
    attr = attr > m_min_attractiveness ? attr : m_min_attractiveness;
    m_wheel.push_back(std::make_pair(attr, e));
    m_total_attractiveness += attr;
  }
}


/**
 * Return reference to randomly selected element.
 *
 * The selection is based on the elements' attractivenesses.
 */
template<class Elem>
std::pair<Attractiveness, Elem>& WeightedRouletteWheel<Elem>::spin() {
  if (m_wheel.empty()) {
    throw std::runtime_error("no elements in roulette wheel");
  }
  Attractiveness threshold = m_total_attractiveness * drand48();
  Attractiveness cum_attractiveness = 0.0;
  for (std::pair<Attractiveness, Elem>& p : m_wheel) {
    cum_attractiveness += p.first;
    if (cum_attractiveness > threshold) {
      return p;
    }
  }

//   // solely for debugging purposes (this should never be reached)
//   // only works for elements that can be converted to bool (like pointers)
//   for (std::pair<Attractiveness, Elem>& p : m_wheel) {
//     if (p.second) {
//       std::cout << *(p.second) << ": " << p.first << "\n";
//     } else {
//       std::cout << "cluster: " << p.first << "\n";
//     }
//   }
  throw std::runtime_error("no element selected by spinning roulette wheel");
}
}

#endif  // WEIGHTED_ROULETTE_WHEEL_H
