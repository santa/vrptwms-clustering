/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It defines a class for storing an manipulating solutions.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <list>

#include <boost/timer/timer.hpp>

#include "common.hpp"
#include "insertion.hpp"
#include "problem.hpp"
#include "solution.hpp"
#include "solver.hpp"

#include "deterministic_solver.hpp"

using boost::timer::cpu_timer;

namespace VRPTWMS {
/**
 * Constructor.
 */
DeterministicRoutingSolver::DeterministicRoutingSolver(Solution& sol)
    : pb_(sol.pb()), sol_(sol)
{
  for (auto it = sol_.clusters().begin(); it != sol_.clusters().end(); ++it) {
    unrouted_.push_back(&(*it));
  }
}


/**
 * Return required CPU time in nanoseconds.
 */
nanosecond_type DeterministicRoutingSolver::cpu_time() const
{
  return timer_.elapsed().user + timer_.elapsed().system;
}


/**
 * Return a pointer to the cluster furthest from the depot.
 * The cluster is also removed from the list of unrouted clusters.
 * Must never be called when there are no unrouted clusters left.
 */
Cluster* DeterministicRoutingSolver::get_furthest_seed()
{
  assert(!unrouted_.empty());
  auto furthest_it = unrouted_.begin();
  Distance max_dist = pb_.d((*furthest_it)->id());
  for (auto it = std::next(unrouted_.begin(), 1); it != unrouted_.end(); ++it) {
    if (pb_.d((*it)->id()) > max_dist) {
      max_dist = pb_.d((*it)->id());
      furthest_it = it;
    }
  }
  Cluster* furthest = *furthest_it;
  unrouted_.erase(furthest_it);
  return furthest;
}


/**
 * Populate the solution with a feasible VRPTWMS routing solution.
 */
void DeterministicRoutingSolver::solve()
{
  // allows implementing different VRPTWMS routing heuristics
  // the heuristic to be used can be stored as a member variable
  // or as a member of the problem's configuration in the future
  timer_.start();
  solve_solomon();
  sol_.improve();
  timer_.stop();
  sol_.increase_cpu_time(cpu_time());
}


/**
 * Construct a single initial solution.
 * Use the regular deterministic version of Solomon's I1 heuristic.
 * Each route is initialized by default with the maximum number of workers.
 */
void DeterministicRoutingSolver::solve_solomon()
{
  while (!unrouted_.empty()) {
    Route r(pb_, *get_furthest_seed());
    while (!unrouted_.empty()) { // fill the current route
      Insertion ins;
      auto best_it = unrouted_.begin();
      for (auto it = unrouted_.begin(); it != unrouted_.end(); ++it) {
        if (r.update_insertion(**it, ins)) best_it = it;
      }
      if (not ins.is_feasible) break;
      r.insert(ins.pos, *ins.c);
      unrouted_.erase(best_it);
    }
    sol_.add_route(r);
  }
}


/**
 * Constructor for a deterministic clustering solver.
 * 
 * Note: weighting the attractivenesses of clusters with only one client
 * effectively halves them (the attractiveness is
 * 1 / sum(total time per available number of workers)
 * this is the same regardless of the number of workers b/c only one client
 * has to be served, but if two and three workers are counted twice or thrice
 * the result is 1 / (6 * service time with one worker) instead of
 * 1 / (3 * service time with one worker)). This leads
 * to much better results.
 *
 * The same does not hold true for evaluating the
 * attractiveness of an insertion into an existing cluster. This makes sense
 * as the ratio of new attractivenesses to the attractivenesses of insertions
 * is one of the key factors determining the number of clusters and hence the
 * solution quality.
 */
DeterministicClusteringSolver::DeterministicClusteringSolver(Solution& sol)
: pb_(sol.pb()), sol_(sol)
{
  for (const Client& client: pb_.clients()) {
    trivial_clusters_.push_back(Cluster(pb_, client));
  }
  std::sort(trivial_clusters_.begin(), trivial_clusters_.end(),
            [](const Cluster& a, const Cluster& b) {
              return a.attractiveness(true) < b.attractiveness(true);
            });
}


/**
 * Return required CPU time in nanoseconds.
 */
nanosecond_type DeterministicClusteringSolver::cpu_time() const
{
  return timer_.elapsed().user + timer_.elapsed().system;
}


void DeterministicClusteringSolver::solve()
{
  timer_.start();
  solve(false);
  timer_.stop();
  sol_.increase_cpu_time(cpu_time());
}


/**
 * Populate the solution with a feasible VRPTWMS clustering solution.
 *
 * Resetting the solution is solely used for evaluating the runtime of the
 * heuristics (solving the same heuristic multiple times to get more reliable
 * runtime data). Otherwise, solving after the first time does nothing.
 */
void DeterministicClusteringSolver::solve(bool reset)
{
  if (reset || sol_.clusters().empty()) {
    sol_.clusters().clear();
    unassigned_.assign(trivial_clusters_.begin(), trivial_clusters_.end());

    switch (pb_.cfg().clustering_heuristic()) {
    case ClusteringHeuristic::COMBINED:
      solve_combined_heuristic(pb_.cfg().weighted());
      break;
    case ClusteringHeuristic::PARALLEL:
      solve_parallel_heuristic(pb_.cfg().weighted());
      break;
    case ClusteringHeuristic::SEQUENTIAL:
      solve_sequential_heuristic(pb_.cfg().weighted());
      break;
    default:
      throw std::runtime_error("selected clustering heuristic not implemented");
    }
  }
}


/**
 * Populate solution with feasible clusters containing all clients.
 *
 * This approach combines the sequential and parallel heuristics in that all
 * customers are considered (sequential approach) for insertion in all
 * clusters (parallel approach).
 *
 * The runtime is worse than with the other heuristics, but the solution
 * quality is better.
 */
void DeterministicClusteringSolver::solve_combined_heuristic(bool weighted)
{
  while (!unassigned_.empty()) {
    Attractiveness max_attr = unassigned_.back().attractiveness(weighted);
    std::list<Cluster>::iterator src(unassigned_.end());
    std::vector<Cluster>::iterator dest(sol_.clusters().end());
    bool create_cluster(true);
    for (auto src_it = unassigned_.begin(); src_it != unassigned_.end();
         ++src_it) {
      for (auto dest_it = sol_.clusters().begin();
           dest_it != sol_.clusters().end(); ++dest_it) {
        const Client& client(*(*src_it).clients()[0]);
        Cluster& cluster(*dest_it);
        Attractiveness attr = cluster.evaluate_addition(client, weighted);
        if (attr > max_attr) {
          max_attr = attr;
          src = src_it;
          dest = dest_it;
          create_cluster = false;
        }
      }
    }
    if (create_cluster) {
      sol_.clusters().push_back(std::move(unassigned_.back()));
      unassigned_.pop_back();
    } else {
      dest->add(*(src->clients()[0]));
      unassigned_.erase(src);
    }
  }
}


/**
 * Populate solution with feasible clusters containing all clients.
 *
 * This heuristic constructs clusters in parallel. Customers are inserted to
 * clusters one after the other. For the current customer, each cluster in the
 * current solution is evaluated by an attractiveness function before the client
 * is either inserted in an existing cluster or added as a new cluster.
 *
 * Note: the order in which the clients are evaluated matters for solution
 * quality. If sorting is done, clients with low attractiveness for creating
 * a new cluster should be taken first. It is important for the quality
 * of the routing solution that neither too many nor too few clusters exist.
 * 
 * TODO: would be faster and still good to only consider the clusters in a
 * configurable number of closest parkings.
 */
void DeterministicClusteringSolver::solve_parallel_heuristic(bool weighted)
{
  while (!unassigned_.empty()) {
    Cluster* destination(&unassigned_.front());
    Attractiveness max_attr = destination->attractiveness(weighted);
    const Client& client = *destination->clients()[0];
    bool create_cluster = true;
    for (Cluster& cluster: sol_.clusters()) {
      Attractiveness attr = cluster.evaluate_addition(client, weighted);
      if (attr > max_attr) {
        max_attr = attr;
        create_cluster = false;
        destination = &cluster;
      }
    }
    if (create_cluster) {
      sol_.clusters().push_back(std::move(*destination));
      unassigned_.pop_front();
    } else {
      destination->add(client);
      unassigned_.pop_front();
    }
  }
}


/**
 * Populate solution with feasible clusters containing all clients.
 *
 * This heuristic constructs clusters sequentially. For the current cluster, all
 * unassigned customers are evaluated and the best alternative is inserted one
 * by one until creating a new cluster has a higher attractiveness. In that
 * case, the new cluster is inserted into the solution and subsequently filled.
 * The above is repeated until all customers are assigned to exactly one
 * cluster.
 * Whenever a new cluster is created, the heuristic picks the new cluster with
 * the highest attractiveness.
 */
void DeterministicClusteringSolver::solve_sequential_heuristic(bool weighted)
{
  while (!unassigned_.empty()) {
    Cluster cluster = std::move(unassigned_.back());  // move saves from destruction
    unassigned_.pop_back();  // destroys the element at back()
    while (!unassigned_.empty()) {
      // setting max_attr to current cluster instead of next improves
      // solution quality (on average by 1/2 a truck)
      Attractiveness max_attr = cluster.attractiveness(weighted);

      const Client* candidate(nullptr);
      bool create_cluster(true);
      for (const Client* client : pb_.clients(cluster.id())) {
        if (not is_unassigned(unassigned_, *client)) continue;
        Attractiveness attr = cluster.evaluate_addition(*client, weighted);
        if (attr > max_attr) {
          max_attr = attr;
          candidate = client;
          create_cluster = false;
        }
      }
      if (!create_cluster) {
        cluster.add(*candidate);
        remove_unassigned(unassigned_, *candidate);
      } else {
        break;
      }
    }
    sol_.clusters().push_back(cluster);
  }
}
}

//       std::list<Cluster>::iterator candidate(unassigned_.end());
//       bool create_cluster(true);
//       for (auto it = unassigned_.begin(); it != unassigned_.end(); ++it) {
//         const Client& client = *(*it).clients()[0];
//         Attractiveness attr = cluster.evaluate_addition(client, weighted);
//         if (attr > max_attr) {
//           max_attr = attr;
//           candidate = it;
//           create_cluster = false;
//         }
//       }
//       if (!create_cluster) {
//         cluster.add(*(*candidate).clients()[0]);
//         unassigned_.erase(candidate);
//       } else {
//         break;
//       }
//     }
//     sol_.clusters().push_back(cluster);
//   }
// }
