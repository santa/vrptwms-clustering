/*
 * Copyright (C) 2014  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GUI_H
#define GUI_H

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <Fl/fl_draw.H>

#include "location.hpp"
#include "matrix.hpp"


namespace VRPTWMS {

class Solution;


class Gui : public Fl_Double_Window {
public:
  Gui(const Solution& sol, unsigned int scale=1);
  void update(const Matrix<double>& clustering_ph,
              const Matrix<double>& routing_ph,
              const Matrix<double>& opening_ph,
              const Matrix<double>& closing_ph);

private:
//   class Canvas : public Fl_Widget {
//   public:
//     Canvas(const Solution& sol, unsigned int scale=1);
//
//   private:
    void draw();
//     static void time_callback(void* userdata) {
//       Canvas* o = (Canvas*) userdata;
//       o->redraw();
//       Fl::repeat_timeout(0.25, time_callback, userdata);
//     }
//   }

//   Gui::Canvas canvas_;
  bool data_is_ready_;
  const Problem& pb_;
  unsigned int scale_;
  const Solution& sol_;

  const Matrix<double>* clustering_ph_;
  const Matrix<double>* routing_ph_;
  const Matrix<double>* opening_ph_;
  const Matrix<double>* closing_ph_;

  void draw_line(Location l1, Location l2, double pheromone);
  double max_pheromone(VRPTWMS::Index id, const Matrix< double >* matrix);
};


int x_range(const Problem& pb);
int y_range(const Problem& pb);
}

#endif  // GUI_H
