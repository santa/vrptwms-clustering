/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It declares a class for storing an manipulating solutions.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SOLUTION_H
#define SOLUTION_H

#include <iostream>
#include <string>
#include <vector>

#include <boost/timer/timer.hpp>

#include "common.hpp"
#include "cluster.hpp"
#include "route.hpp"


using boost::timer::nanosecond_type;


namespace VRPTWMS {
class Problem;


class Solution {
public:
//  explicit Solution(const Problem& pb) {}
  explicit Solution(const Problem& pb) : cpu_time_(0), pb_(pb) {}
  explicit Solution(const Problem& pb, const std::string& cluster_filename);

  explicit Solution(const Solution& other);
  Solution& operator=(const Solution& other);
  Solution& operator=(Solution&& other);
  ~Solution() {}

  /**
   * Add given route to the list of routes in the solution.
   */
  void add_route(Route& r) {routes_.push_back(r);}
  /**
   * Increase the required CPU time by the given number of nanoseconds.
   */
  void increase_cpu_time(nanosecond_type time) {cpu_time_ += time;}
  /**
   * Set the required CPU time to the given number of nanoseconds.
   */
  void set_cpu_time(nanosecond_type time) {cpu_time_ = time;}
  /**
   * Return the total CPU time required to obtain this solution (in ms).
   */
  Milliseconds milliseconds() const {return to_milliseconds(cpu_time_);}
  /**
   * Return the total CPU time required to obtain this solution (in ns).
   */
  nanosecond_type nanoseconds() const {return cpu_time_;}
  /**
   * The number of trucks (=routes) required by the solution.
   */
  Index trucks() const {return routes_.size();}
  Cost total_cost() const;
  /**
   * Return the clusters created/ used by the solution.
   *
   * The reference is not constant b/c the clusters' actual earliest and latest
   * start times need to be adjusted by solvers.
   */
  std::vector<Cluster>& clusters() {return clusters_;}
  /**
   * Return the vector of routes.
   */
  const std::vector<Route>& routes() const {return routes_;}
  /**
   * The problem which which is solved in this solution.
   */
  const Problem& pb() const {return pb_;}

  Distance distance() const;
  void improve();
  bool is_feasible() const;
  void reset();
  Index workers() const;
  std::ostream& write_details(std::ostream& os) const;
private:
  bool has_feasible_clusters() const;
  bool has_feasible_routes() const;

  nanosecond_type cpu_time_;
  const Problem& pb_;
  std::vector<Route> routes_;
  std::vector<Cluster> clusters_;

  friend std::ostream& operator<< (std::ostream& os, const Solution& s);

  FRIEND_TEST(TestSolution, test_distance);
  FRIEND_TEST(TestSolution, test_workers);
};

std::ostream& operator<< (std::ostream& os, const Solution& s);
}

#endif //SOLUTION_H

