/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <utility>  // std::pair

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <Fl/fl_draw.H>

#include "common.hpp"
#include "location.hpp"
#include "matrix.hpp"
#include "problem.hpp"
#include "solution.hpp"

#include "gui.hpp"


namespace VRPTWMS {

static const double padding = 0.03;  // in % of the range; used for each side

template <typename T>
static std::pair<Distance, Distance> x_limits(Distance min_x, Distance max_x,
                                              const std::vector<T>& locations);
template <typename T>
static std::pair<Distance, Distance> y_limits(Distance min_y, Distance max_y,
                                              const std::vector<T>& locations);

/* Constructor */
Gui::Gui(const Solution& sol, unsigned int scale)
    : Fl_Double_Window(x_range(sol.pb()) * scale,
                       y_range(sol.pb()) * scale),
      data_is_ready_(false),
      pb_(sol.pb()),
      scale_(scale),
      sol_(sol)
{}


void Gui::update(const Matrix<double>& clustering_ph,
                 const Matrix<double>& routing_ph,
                 const Matrix<double>& opening_ph,
                 const Matrix<double>& closing_ph)
{
  if (! data_is_ready_) {
    data_is_ready_ = true;
    clustering_ph_ = &clustering_ph;
    routing_ph_ = &routing_ph;
    opening_ph_ = &opening_ph;
    closing_ph_ = &closing_ph;
  }
  redraw();
}


/**
 * Draw routing pheromone progress.
 */
void Gui::draw()
{
  Fl_Double_Window::draw();
  fl_color(FL_WHITE);
  fl_rectf(0, 0, w(), h() );
  if (!data_is_ready_) {
    return;
  }
  for (const Parking& parking : pb_.parkings()) {
    // to / from depot
    double depot_pheromone(max_pheromone(parking.id(), opening_ph_));
    depot_pheromone = std::max(depot_pheromone,
                               max_pheromone(parking.id(), closing_ph_));
    draw_line(parking, pb_.depot(), depot_pheromone);
    for (const Parking& other : pb_.parkings()) {
      if (parking.id() == other.id()) {
        continue;
      }
      if (parking.id() == pb_.depot().id() || other.id() == pb_.depot().id()) {
        continue;
      }
      double pheromone = (*routing_ph_)[parking.id()][other.id()];
      draw_line(parking, other, pheromone);
    }
  }
  std::cout << "draw is called" << std::endl;
}


/**
 * Draw line showing the pheromone between two locations.
 */
void Gui::draw_line(Location l1, Location l2, double pheromone)
{
  if (pheromone < 0.1) {
    return;
  }
  fl_line_style(FL_SOLID, static_cast<int>(pheromone * scale_), 0);
  uchar color(static_cast<uchar>((1 - pheromone) * 255u));
  fl_color(fl_rgb_color(color));
  fl_line(static_cast<int>(l1.x()) * scale_ + w() / 2,
          static_cast<int>(l1.y()) * scale_ + h() / 2,
          static_cast<int>(l2.x()) * scale_ + w() / 2,
          static_cast<int>(l2.y()) * scale_ + h() / 2);
}


/**
 * Return the maximum pheromone for the given id.
 *
 * This is useful for the opening and closing pheromone lookups as these are
 * per route and the GUI does not know which route to select.
 */
double Gui::max_pheromone(Index id, const Matrix<double>* matrix)
{
  double pheromone(0);
  for (Matrix<double>::row_size_t row = 0; row < matrix->num_rows(); ++row) {
    pheromone = std::max((*matrix)[row][id], pheromone);
  }
  return pheromone;
}


/**
 * Return the minimum and maximum x coordinate in the given list of locations.
 *
 * The minimum and maximum are initialized with the provided values. If no
 * location has a value lower than the provided minimum resp. higher than the
 * provided maximum, the corresponding parameter value will be returned.
 */
template <typename T>
static std::pair<Distance, Distance> x_limits(Distance min_x, Distance max_x,
                                              const std::vector<T>& locations) {
  for (const Location& l: locations) {
    if (l.x() < min_x) {
      min_x = l.x();
    }
    if (l.x() > max_x) {
      max_x = l.x();
    }
  }
  return std::pair<Distance, Distance>(min_x, max_x);
}


/**
 * Return the minimum and maximum y coordinate in the given list of locations.
 *
 * The minimum and maximum are initialized with the provided values. If no
 * location has a value lower than the provided minimum resp. higher than the
 * provided maximum, the corresponding parameter value will be returned.
 */
template <typename T>
static std::pair<Distance, Distance> y_limits(Distance min_y, Distance max_y,
                                              const std::vector<T>& locations) {
  for (const Location& l: locations) {
    if (l.y() < min_y) {
      min_y = l.y();
    }
    if (l.y() > max_y) {
      max_y = l.y();
    }
  }
  return std::pair<Distance, Distance>(min_y, max_y);
}


/**
 * Return the range (span between minimum and maximum value) of all x
 * coordinates in the given problem.
 *
 * The range is then increased with a defined padding on both sides.
 */
int x_range(const Problem& pb)
{
  std::pair<Distance, Distance> x_limits_(x_limits(pb.depot().x(),
                                                   pb.depot().x(),
                                                   pb.clients()));
  x_limits_ = x_limits(x_limits_.first, x_limits_.second, pb.parkings());
  return static_cast<int>(ceil((x_limits_.second - x_limits_.first) *
    (1 + 2 * padding)));
}


/**
 * Return the range (span between minimum and maximum value) of all y
 * coordinates in the given problem.
 *
 * The range is then increased with a defined padding on both bottom and top.
 */
int y_range(const Problem& pb)
{
  std::pair<Distance, Distance> y_limits_(y_limits(pb.depot().y(),
                                                   pb.depot().y(),
                                                   pb.clients()));
  y_limits_ = y_limits(y_limits_.first, y_limits_.second, pb.parkings());
  return static_cast<int>(ceil((y_limits_.second - y_limits_.first) *
    (1 + 2 * padding)));
}

}  // namespace VRPTWMS
