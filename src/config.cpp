/*
 * Copyright (C) 2014  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdexcept>

#include "common.hpp"
#include "problem.hpp"

#include "config.hpp"


namespace VRPTWMS {
ClusteringHeuristic operator++(ClusteringHeuristic& h) {
  h = (ClusteringHeuristic)(std::underlying_type<ClusteringHeuristic>::type(h) + 1);
  return h;
}

/**
 * Parse config file and set config values accordingly.
 */
Config::Config(std::string filename) {
  std::cout << "ERROR: parsing custom config not implemented yet\n";
  std::cout << filename << "\n";
  throw std::runtime_error("Not implemented!");
}


/**
  * Set the number of ants (for ACO).
  * Also allows to disable the dynamic number of ants.
  */
void Config::ants(Index number, bool disable_dynamic)
{
  ants_ = number;
  if (disable_dynamic) dynamic_ants_ = false;
  if (!ants_) dynamic_ants_ = true;
}


/**
 * Set the clustering heuristic by name.
 */
void Config::clustering_heuristic(std::string name)
{
  if (name == "combined") {
    clustering_heuristic_ =  ClusteringHeuristic::COMBINED;
  } else if (name == "parallel") {
    clustering_heuristic_ =  ClusteringHeuristic::PARALLEL;
  } else if (name == "sequential") {
    clustering_heuristic_ =  ClusteringHeuristic::SEQUENTIAL;
  } else {
    throw std::runtime_error(
      "\"" + name + "\" is not a valid name for a clustering heuristic");
  }
}


/**
 * Return the short name of the given heuristic.
 */
std::string Config::short_name(ClusteringHeuristic h)
{
  switch (h) {
  case ClusteringHeuristic::COMBINED:
    return "combined";
  case ClusteringHeuristic::PARALLEL:
    return "parallel";
  case ClusteringHeuristic::SEQUENTIAL:
    return "sequential";
  default:
    return "unknown";
  }
}


/**
 * Update configuration according to topology of given problem.
 * This currently only affects the number of ants.
 */
void Config::update(const Problem& pb)
{
  if (dynamic_ants()) {
    ants(pb.parkings().size());
  }
}


std::ostream& operator<<(std::ostream& os, const Config& cfg)
{
  os << "attractiveness penalty: " << cfg.attractiveness_penalty() << "\n";
  if (cfg.deterministic()) {
    os << "deterministic heuristic for clustering and routing" << "\n";
    os << "clustering-heuristic: ";
    os << Config::short_name(cfg.clustering_heuristic());
  } else {
    os << "metaheuristic: ACO (clustering + routing)\nants: ";
    if (cfg.dynamic_ants()) {
      os << "dynamic\n";
    } else {
      os << cfg.ants() << "\n";
    }
    os << "persistance: " << cfg.rho() * 100 << "%\n";
    os << "runtime: " << cfg.max_runtime() << "\n";
    os << "seed: " << cfg.random_seed();
  }
  return os;
}
}
