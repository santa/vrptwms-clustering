/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include "client.hpp"


namespace VRPTWMS {
/**
 * Textual representation of client.
 */
std::ostream& operator<< (std::ostream& os, const Client& c) {
  os << "Client(" << c.id() << " " << c.x() << "/" << c.y() << ", ";
  os << c.est() << "-" << c.lst() << ": " << c.st() << ", ";
  os << c.demand() << ")";
  return os;
}
}
