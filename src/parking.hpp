/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PARKING_H
#define PARKING_H

#include "common.hpp"
#include "location.hpp"


namespace VRPTWMS {
class Parking : public Location {
public:
  explicit Parking(Index id, double x, double y) : Location(id, x, y) {}
  Parking(const Parking& other) : Location(other) {}
//   Parking& operator=(const Parking& other) {};
  ~Parking() {}
};

std::ostream& operator<< (std::ostream& os, const Parking& p);
}

#endif //PARKING_H
