/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DETERMINISTIC_SOLVER_H
#define DETERMINISTIC_SOLVER_H

#include <list>

#include <boost/timer/timer.hpp>

#include <gtest/gtest_prod.h>

#include "problem.hpp"
#include "solution.hpp"
#include "solver.hpp"


using boost::timer::cpu_timer;
using boost::timer::nanosecond_type;

class Cluster;


namespace VRPTWMS {
/**
 * A solver implementing deterministic VRPTWMS routing heuristics.
 */
class DeterministicRoutingSolver : public Solver {
public:
  explicit DeterministicRoutingSolver(Solution& sol);
  nanosecond_type cpu_time() const;
  void solve();

private:
  Cluster* get_furthest_seed();
  void solve_solomon();

  /**
   * A list of unassigned clusters that still need to be routed.
   */
  std::list<Cluster*> unrouted_;
  const Problem& pb_;
  Solution& sol_;

  cpu_timer timer_;

  FRIEND_TEST(TestDeterministicRoutingSolver, test_get_furthest_seed);
};


/**
 * A solver implementing deterministic VRPTWMS clustering heuristics.
 */
class DeterministicClusteringSolver : public Solver {
public:
  explicit DeterministicClusteringSolver(Solution& sol);
  nanosecond_type cpu_time() const;
  void solve();
  void solve(bool reset);  // cannot use default value to honor solver interface

private:
  /**
   * Assignment of 1 customer per parking; they have the purpose of checking
   * if the current instance is feasible and to provide a resource for selecting
   * the next cluster to be started.
   * The trivial clusters are sorted ascending by their attractiveness.
   */
  std::vector<Cluster> trivial_clusters_;
  /**
   * A list of unassigned clients, realized as members of their best trivial
   * assignments.
   */
  std::list<Cluster> unassigned_;
  const Problem& pb_;
  Solution& sol_;

  cpu_timer timer_;

  // heuristics
  void solve_combined_heuristic(bool weighted=false);
  void solve_parallel_heuristic(bool weighted=false);
  void solve_sequential_heuristic(bool weighted=false);

  FRIEND_TEST(TestDeterministicClusteringSolver, test_DeterministicClusteringSolver);
};
}

#endif  // DETERMINISTIC_SOLVER_H
