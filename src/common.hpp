/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It contains typedefs used through the whole program.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COMMON_H
#define COMMON_H

#include <cmath>

#include <boost/math/special_functions/next.hpp>
#include <boost/timer/timer.hpp>

using boost::math::float_distance;
using boost::timer::nanosecond_type;

namespace VRPTWMS {
typedef double Amount;
typedef double Attractiveness;
typedef double Cost;
typedef double Distance;
typedef long unsigned int Index;
typedef long int Milliseconds;
typedef long int Seconds;
typedef double Speed;
typedef double Time;

namespace Constants
{
  /** number of digits after decimal separator when rounding */
  const Index Digits = 5;
  /** default number of units of least precision; see almost_equal(.) */
  const int DefaultULP = 5;
  /** allows standardized way of ensuring that all attractivenesses are > 0 */
  const Attractiveness MinAttractiveness = 0.00000001;
  /** used whenever no legal time is present */
  const Time NoTime = -1.0;
}

inline Seconds to_seconds(nanosecond_type nanoseconds) {
  return nanoseconds / 1000000000;
}

inline Milliseconds to_milliseconds(nanosecond_type nanoseconds) {
  return nanoseconds / 1000000;
}

inline nanosecond_type to_nanoseconds(Seconds seconds) {
  return seconds * 1000000000;
}

/**
 * Return true if two floating point numbers are almost equal, otherwise false.
 *
 * The numbers must not differ by more than a defined number of ULPs. See
 * code/c++/examples/floating_point_calculation.cpp for an explanation and
 * tests.
 */
template <class FP>
FP almost_equal(FP a, FP b, int maxULPs=Constants::DefaultULP) {
  return std::abs(float_distance(a, b)) < maxULPs;
}

/**
 * Return `number` rounded to given number of `digits` after the decimal separator.
 */
template <class Decimal>
Decimal round(Decimal number, Index digits=Constants::Digits) {
  return std::round(number * std::pow(10, digits)) / std::pow(10, digits);
}
}

#endif  // COMMON_H
