/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CLIENT_H
#define CLIENT_H

#include "common.hpp"
#include "location.hpp"


namespace VRPTWMS {
class Client : public Location {
public:
  explicit Client(Index id, double x, double y,
                  Time earliest_start, Time latest_start, Time service_time,
                  Amount demand)
      : Location(id, x, y),
        earliest_start_(earliest_start),
        latest_start_(latest_start),
        service_time_(service_time),
        demand_(demand) {}
  // explicit copy ctor prohibits accidental copying
  explicit Client(const Client& other)
    : Client(other.id(), other.x(), other.y(), other.est(), other.lst(),
             other.st(), other.demand()) {}
  Time est() const {return earliest_start_;}
  Time lst() const {return latest_start_;}
  /**
   * Return the service time required by the client.
   */
  Time st() const {return service_time_;}
  Amount demand() const {return demand_;}

private:
  Time earliest_start_;
  Time latest_start_;
  Time service_time_;
  Amount demand_;
};

std::ostream& operator<< (std::ostream& os, const Client& c);
}

#endif //CLIENT_H
