/*
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOCATION_H
#define LOCATION_H

#include "common.hpp"


namespace VRPTWMS {
class Location {
public:
  explicit Location(Index id, double x, double y) : id_(id), x_(x), y_(y) {}
  Location(const Location& other)
      : id_(other.id()), x_(other.x()), y_(other.y()) {}
  ~Location() {};
//   Problem& operator=(const Problem& other);
public:
  Index id() const {return id_;}
  Distance x() const {return x_;}
  Distance y() const {return y_;}
private:
   Index id_;
   Distance x_;
   Distance y_;
};
}

#endif //LOCATION_H
