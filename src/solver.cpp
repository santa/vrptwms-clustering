/*
 * This file is part of a program allowing to solve VRPTWMS instances.
 * It defines a class representing the problem to be solved.
 *
 * Copyright (C) 2013  Gerald Senarclens de Grancy <oss@senarclens.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <list>

// #include "deterministic_solver.hpp"
// #include "problem.hpp"
// #include "solution.hpp"
#include "client.hpp"
#include "cluster.hpp"
#include "solver.hpp"


namespace VRPTWMS {
/**
 * Return true iif the given client is in the list of "unassigned clusters".
 */
bool is_unassigned(std::list< Cluster >& unassigned, const Client& client)
{
  for (auto it = unassigned.begin(); it != unassigned.end(); ++it) {
    if (it->clients()[0]->id() == client.id()) {
      return true;
    }
  }
  return false;
}


/**
 * Remove cluster of unassigned containing the given client.
 */
void remove_unassigned(std::list<Cluster>& unassigned, const Client& client)
{
  for (auto it = unassigned.begin(); it != unassigned.end(); ++it) {
    if (it->clients()[0]->id() == client.id()) {
      unassigned.erase(it);
      return;
    }
  }
}
}
