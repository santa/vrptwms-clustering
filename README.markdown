Clustering heuristics for the VRPTWMS
=====================================

Prerequisites
-------------
The prototype is written in Python 3 and the real Implementation is done in
C++. The used build-system is CMake. Make sure you have the required packages
installed w/

    sudo apt-get install python3 python3-numpy python3-scipy \
      python3-matplotlib g++ cmake

Then make sure you have the required boost libraries (Version 1.53 or newer)

    sudo aptitude install libboost-all-dev

The packages used include libboost-filesystem-dev libboost-math-dev
libboost-program-options-dev libboost-ptree-dev libboost-system-dev
libboost-timer-dev.

In order to allow visualizing the pheromone data structures, FLTK 1.3 needs to
be installed as well.

    sudo aptitude install libfltk1.3-dev libfontconfig1-dev libxinerama-dev \
      libxft-dev

Testing
-------
For testing, the googletest framework is used. It can be installed via

    :::bash
    sudo aptitude install libgtest-dev
    cd /usr/src/gtest
    sudo cmake CMakeLists.txt
    sudo make
    sudo cp *.a /usr/lib

If you feel uncomfortable running make and cmake as root, you'd have to copy /usr/src/gtest to a directory writable by your user.

Compiling
---------

    mkdir -p build
    cd build
    cmake ..
    make

