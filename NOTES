This file contains hints on our reasoning and things to implement. Once these
are implemented, they should either be moved to docstrings or get rephrased
properly and moved to the README.

Goals
------

Minimize service-time(ST), minimize earliest starting-time (EST),  maximize latest starting-time (LST) (last two: maximize flexibility when cluster can be served)

Calculation
------------

For each customer:
latest starting-time (LST), latest finish-time (LFT), earliest starting-time (EST), earliest finish-time (EFT) and service-time (ST)

LST: determine order of processing nodes
  rule: max (latest_closing + service_time)
  if not feasible for all nodes (excess of time window constraint):
    rule: max(latest_closing)
  pick rule that is feasible for more nodes
LFT: reverse order of LST
EFT: same direction as LFT
delta: LFT-EFT
EST: LST - delta
ST: EFT-EST; LFT-LST
run-time: O(n²) for each customer
each node has sorted list of distances O(n²*log(n))


Target:
--------
1) 100 percent guarantee: feasible/infeasible results from algorithm
2) correct results: LST, LFT, EFT, EST, ST

Search for park space
----------------------

Strategies:
1) coordinate of parking site = coordinate of first customer in cluster
2) define coordinate random
3) pay fine for parking at customer
4) if no parking site in cluster --> generate coordinate of parking site with the same coordinates of first customer in cluster. If two nodes have the same coordinate --> pay fine --> the rest of customers have to travel from parking site with euclidean distance
5) ....?


Cluster strategies
----------------
1) Each customer is a cluster
2) Cluster customers based on the euclidean distance (one by one with max distance)
3.1) Cluster customers based on the time windows objective function of minimizing the service time
3.2) Cluster customers based on the time windows objective function of minimizing the number of cluster
4.1) Cluster customers based on the distance to the parking site with objective function of minimizing service time
4.2) Cluster customers based on the distance to the parking site with objective function of minimizing the number of cluster
