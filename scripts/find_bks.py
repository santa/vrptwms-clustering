#!/usr/bin/env python3

"""
This script is downloads the known BKS from a live web source and compares
these against a series of files containing new solution data. If the given
files contain new BKS, these are properly reported to be updated in the common
web source.
"""

import csv
import requests
import sys

from io import StringIO

AUTHOR = 'Senarclens'
BKS_RESOURCE = 'http://j.mp/vrptwms_clustering_BKS-as_csv'
COST = 'Cost'
INSTANCE = 'Instance'
FIELD_NAMES = (INSTANCE, 'Clusters', 'Trucks', 'Workers', 'Distance', COST,
               'Time [ms]')


class Colors:
    '''Terminal color escape sequence definitions.'''
    EMPH = '\033[94m'
    STRONG = '\033[91m'
    ENDC = '\033[0m'


def fetch_bks(resource):
    """Download BKS data and return it as dictionary of dictionaries."""
    response = requests.get(resource)
    assert response.status_code == 200, 'Wrong status code'
    text = response.content.decode()
    lines = text.split('\n')
    header_lines = 0
    for line in lines:
        if len(line.split(',')[len(FIELD_NAMES)]):
            break
        header_lines += 1
    text = '\n'.join(lines[header_lines:])
    reader = csv.DictReader(StringIO(text))
    bks = {}
    for dict_ in reader:
        instance = dict_.pop('Instance')
        for key in dict_.keys():
            value = dict_.pop(key)
            dict_[key.strip()] = value
        bks[instance] = dict_
    return bks


def read_resultfile(filename):
    """Return a generator of dicts with result data from the given file."""
    with open(filename, 'r', encoding='utf-8') as infile:
        reader = csv.DictReader(infile, FIELD_NAMES)
        for dict_ in reader:
            if not dict_[COST]:
                continue
            yield dict_


def read_metadata(filename):
    '''
    Return the parameter setup for the results in filename.

    `return`: a string containing the parameter setup
    '''
    metadata = ''
    with open(filename, 'r', encoding='utf-8') as infile:
        metadata += infile.readline().strip()
        for line in infile:
            data = line.strip()
            if not data:
                break
            elif len(data.split(',')) >= len(FIELD_NAMES):
                break
            metadata += '; ' + data
    return metadata


def compare_bks(bks, filename):
    """Try to find new bks in given file."""
    for result in read_resultfile(filename):
        instance = result[INSTANCE]
        if result[COST] < bks[instance][COST]:
            for field in FIELD_NAMES:
                if field == COST:
                    print(Colors.EMPH + result[field] + Colors.ENDC, end=',')
                else:
                    print(result[field], end=',')
            print(AUTHOR, end=',')
            print(read_metadata(filename))

def main():
    """Entry point when starting the module."""
    if len(sys.argv) == 1:
        print("Usage:", sys.argv[0], "filename [filenames ...]")
        return 1
    bks = fetch_bks(BKS_RESOURCE)
    for filename in sys.argv[1:]:
        compare_bks(bks, filename)
    return 0


if __name__ == "__main__":
    sys.exit(main())
